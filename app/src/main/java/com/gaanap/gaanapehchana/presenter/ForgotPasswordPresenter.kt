package com.gaanap.gaanapehchana.presenter

import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.ForgotPasswordView
import com.gaanap.gaanapehchana.models.response.BaseResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class ForgotPasswordPresenter(val view: ForgotPasswordView) {

    fun forgotPassword(email: String) : Disposable {
        view.showProgress()
        return ApiClient.getClient().forgotPassword(email)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse,this::onError)
    }

    fun resetPassword(newPassword: String,retypePassword: String,code: String) : Disposable {
        view.showProgress()
        return ApiClient.getClient().resetPassword(code,newPassword,retypePassword)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse,this::onError)
    }

    private fun onResponse(response: BaseResponse) {
        view.hideProgress()
        view.onResponse(response)
    }

    private fun onError(throwable: Throwable) {
        view.hideProgress()
        view.onError(throwable)
    }
}