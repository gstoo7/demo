package com.gaanap.gaanapehchana.presenter

import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.GPRadioView
import com.gaanap.gaanapehchana.models.response.PlayList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

class GPRadioPresenter(val view: GPRadioView) {

    fun getRandomSongs(userId: String): Observable<ArrayList<PlayList>> {
        return Observable.concatArray(getRandomSongsFromDB(), getRandomSongsFromServer(userId))
                .filter { !it.isEmpty() }
                .take(1)
                .materialize()
                .filter { !it.isOnError }
                .dematerialize<ArrayList<PlayList>>()
    }

    private fun getRandomSongsFromDB() : Observable<List<PlayList>>{
        val realm = Realm.getDefaultInstance()
        val list  = realm.copyFromRealm(realm.where(PlayList::class.java).findAll())
        realm.close()
        return Observable.just(list)
    }

    fun getRandomSongsFromServer(userId: String) : Observable<List<PlayList>>{
        return ApiClient.getClient().getRandomSongs(userId)
                .doOnNext(this::storeAllSongsInDb)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun storeAllSongsInDb(list: List<PlayList>) {
        if (list.isEmpty()) return
        Realm.getDefaultInstance().use {
            it.executeTransaction {realm->
                val count = realm.where(PlayList::class.java).findAll().count()
                if(count < 40)
                    realm.insertOrUpdate(list)
                else {
                    realm.delete(PlayList::class.java)
                    realm.insertOrUpdate(list)
                }
            }
        }
    }
}