package com.gaanap.gaanapehchana.presenter

import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.CommonView
import com.gaanap.gaanapehchana.models.response.BaseResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ContactUsPresenter(val view: CommonView) {

    fun submitComments(name: String,email: String,subject: String,msg: String) : Disposable {
        view.showProgress()
        return ApiClient.getClient()
                .submitComments(name,email,subject,msg)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse,this::onError)
    }

    fun onResponse(response: BaseResponse) {
        view.hideProgress()
        view.onResponse(response)
    }

    fun onError(throwable: Throwable) {
        view.hideProgress()
        view.onError(throwable)
    }
}