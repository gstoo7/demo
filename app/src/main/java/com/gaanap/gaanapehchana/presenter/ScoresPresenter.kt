package com.gaanap.gaanapehchana.presenter

import android.util.Log
import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.firebasehelper.model.GameInfo
import com.gaanap.gaanapehchana.interfaces.ScoreView
import com.gaanap.gaanapehchana.models.User
import com.gaanap.gaanapehchana.models.response.ScoreResponse
import com.gaanap.gaanapehchana.rx.RxEvent
import com.gaanap.gaanapehchana.rx.RxFirebaseNode
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers

class ScoresPresenter(val view: ScoreView) {

    private var user: User? = null

    fun setGameInfo(user: User) {
        this.user = user
    }

    fun updateTMKScore(userId: String,score: Int,gameId: String) : Disposable {
        view.showProgress()
        RxEvent.updateOnLocalDB(score,gameId).subscribe ({ Log.d("score","success") },Throwable::printStackTrace)
        return ApiClient.getClient()
                .updateTMKScore(userId,score,gameId)
//                .doOnSuccess {
//                    updateScoreOnFirebase(it,userId,"TMK")
//                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse,this::onError)
    }

    fun updateBBGScore(userId: String,score: Int) : Disposable {
        view.showProgress()
        return ApiClient.getClient()
                .updateBBGScore(userId,score)
//                .doOnSuccess {
//                    updateScoreOnFirebase(it,userId,"BBG")
//                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse,this::onError)
    }

    fun updateASNScore(userId: String,score: Int) : Disposable {
        view.showProgress()
        return ApiClient.getClient()
                .updateASNScore(userId,score)
//                .doOnSuccess {
//                    updateScoreOnFirebase(it,userId,"ASN")
//                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse,this::onError)
    }

    private fun onResponse(response: ScoreResponse) {
        view.hideProgress()
        view.onResponse(response)
    }

    private fun onError(throwable: Throwable) {
        view.hideProgress()
        view.onError(throwable)
    }

    private fun updateScoreOnFirebase(response: ScoreResponse,userId: String,gameType: String) {
        val map = HashMap<String,Any>()
        map["weekly/$gameType/$userId/totalScore"]  = response.score.sevendays
        map["weekly/$gameType/$userId/totalGame"]   = response.count.sevendays
        map["weekly/$gameType/$userId/name"]        = user?.fullName ?: ""
        map["weekly/$gameType/$userId/profileIcon"] = user?.pic ?: ""
        map["weekly/$gameType/$userId/timestamp"]   = System.currentTimeMillis().toString()

        map["monthly/$gameType/$userId/totalScore"]  = response.score.monthly
        map["monthly/$gameType/$userId/totalGame"]   = response.count.monthly
        map["monthly/$gameType/$userId/name"]        = user?.fullName ?: ""
        map["monthly/$gameType/$userId/profileIcon"] = user?.pic ?: ""
        map["monthly/$gameType/$userId/timestamp"]   = System.currentTimeMillis().toString()

        map["lifetime/$gameType/$userId/totalScore"]  = response.score.lifetime
        map["lifetime/$gameType/$userId/totalGame"]   = response.count.lifetime
        map["lifetime/$gameType/$userId/name"]        = user?.fullName ?: ""
        map["lifetime/$gameType/$userId/profileIcon"] = user?.pic ?: ""


        FirebaseNodeRef.getRootRef().updateChildren(map).addOnCompleteListener {
            Single.zip(RxFirebaseNode.getWeeklyTMKScoreRef(userId),
                    RxFirebaseNode.getWeeklyBBGScoreRef(userId),
                    RxFirebaseNode.getWeeklyASNScoreRef(userId),
                    Function3<GameInfo,GameInfo,GameInfo,GameInfo>{t1, t2, t3 ->
                        val gameInfo = GameInfo()
                        gameInfo.totalGame  = t1.totalGame  + t2.totalGame  + t3.totalGame
                        gameInfo.totalScore = t1.totalScore + t2.totalScore + t3.totalScore
                        gameInfo

                    })
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.newThread())
                    .subscribe(this::updateWeeklyMasterNode,Throwable::printStackTrace)

            Single.zip(RxFirebaseNode.getMonthlyTMKScoreRef(userId),
                    RxFirebaseNode.getMonthlyBBGScoreRef(userId),
                    RxFirebaseNode.getMonthlyASNScoreRef(userId),
                    Function3<GameInfo,GameInfo,GameInfo,GameInfo>{t1, t2, t3 ->
                        val gameInfo = GameInfo()
                        gameInfo.totalGame  = t1.totalGame  + t2.totalGame  + t3.totalGame
                        gameInfo.totalScore = t1.totalScore + t2.totalScore + t3.totalScore
                        gameInfo

                    })
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.newThread())
                    .subscribe(this::updateMonthlyMasterNode,Throwable::printStackTrace)

            Single.zip(RxFirebaseNode.getLifetimeTMKScoreRef(userId),
                    RxFirebaseNode.getLifetimeBBGScoreRef(userId),
                    RxFirebaseNode.getLifetimeASNScoreRef(userId),
                    Function3<GameInfo,GameInfo,GameInfo,GameInfo>{t1, t2, t3 ->
                        val gameInfo = GameInfo()
                        gameInfo.totalGame  = t1.totalGame  + t2.totalGame  + t3.totalGame
                        gameInfo.totalScore = t1.totalScore + t2.totalScore + t3.totalScore
                        gameInfo

                    })
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.newThread())
                    .subscribe(this::updateLifetimeMasterNode,Throwable::printStackTrace)
        }
    }

    private fun updateWeeklyMasterNode(gameInfo: GameInfo) {
        val map = HashMap<String,Any>()
        map["weekly/Master/${user?.user_id ?: return}/totalScore"]  = gameInfo.totalScore
        map["weekly/Master/${user?.user_id ?: return}/totalGame"]   = gameInfo.totalGame
        map["weekly/Master/${user?.user_id ?: return}/name"]        = user?.fullName ?: ""
        map["weekly/Master/${user?.user_id ?: return}/profileIcon"] = user?.pic ?: ""
        map["weekly/Master/${user?.user_id ?: return}/timestamp"]   = System.currentTimeMillis().toString()

        FirebaseNodeRef.getRootRef().updateChildren(map)
    }

    private fun updateMonthlyMasterNode(gameInfo: GameInfo) {
        val map = HashMap<String,Any>()
        map["monthly/Master/${user?.user_id ?: return}/totalScore"]  = gameInfo.totalScore
        map["monthly/Master/${user?.user_id ?: return}/totalGame"]   = gameInfo.totalGame
        map["monthly/Master/${user?.user_id ?: return}/name"]        = user?.fullName ?: ""
        map["monthly/Master/${user?.user_id ?: return}/profileIcon"] = user?.pic ?: ""
        map["monthly/Master/${user?.user_id ?: return}/timestamp"]   = System.currentTimeMillis().toString()

        FirebaseNodeRef.getRootRef().updateChildren(map)
    }

    private fun updateLifetimeMasterNode(gameInfo: GameInfo) {
        val map = HashMap<String,Any>()
        map["lifetime/Master/${user?.user_id ?: return}/totalScore"]  = gameInfo.totalScore
        map["lifetime/Master/${user?.user_id ?: return}/totalGame"]   = gameInfo.totalGame
        map["lifetime/Master/${user?.user_id ?: return}/name"]        = user?.fullName ?: ""
        map["lifetime/Master/${user?.user_id ?: return}/profileIcon"] = user?.pic ?: ""

        FirebaseNodeRef.getRootRef().updateChildren(map)
    }
}