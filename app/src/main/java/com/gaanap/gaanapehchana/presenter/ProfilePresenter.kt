package com.gaanap.gaanapehchana.presenter

import android.content.Context
import android.widget.Toast
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.ProfileView
import com.gaanap.gaanapehchana.models.User
import com.gaanap.gaanapehchana.models.response.UpdateProfileResponse
import com.gaanap.gaanapehchana.utility.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ProfilePresenter(val context: Context?,val view: ProfileView?) {


    fun updateProfileInfo(user: User?,fullName: String,mobileNo: String,country: String) : Disposable {
        view?.showProgress()
        val map        = HashMap<String,String>()
        map[USER_ID]   = user?.user_id?:""
        map[TYPE]      = "updateProfile"
        map[FULL_NAME] = fullName

        if(mobileNo.isNotBlank())
            map[MOBILE] = mobileNo

        if(country.isNotBlank())
            map[COUNTRY] = country

        return ApiClient.getClient()
                .updateProfile(map)
                .doOnSuccess {
                    updateNameOnFireBase(it.user.user_id,it.user.fullName)
                    SessionManager.saveUser(context ?: return@doOnSuccess,it.user)
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse,this::onError)
    }

    fun updateNotificationSetting(user: User,settings: String) : Single<UpdateProfileResponse> {

        val map              = HashMap<String,String>()
        map[USER_ID]         = user.user_id
        map[TYPE]            = "updateProfile"
        map["notifications"] = settings

        return ApiClient.getClient()
                .updateProfile(map).subscribeOn(Schedulers.newThread())
    }

    fun updatePreference(user: User,decades: String,difficulty: String) : Single<UpdateProfileResponse> {
        val map        = hashMapOf<String,String>()
        map[USER_ID]   = user.user_id
        map[TYPE]      = "updateProfile"

        if(decades.isNotBlank())
            map["decades"] = decades

        if(difficulty.isNotBlank())
            map["difficulty"] = difficulty

        return ApiClient.getClient()
                .updateProfile(map).subscribeOn(Schedulers.newThread())
    }


    fun updatePassword(userId: String,oldPassword: String?,newPassword: String) : Disposable {
        view?.showProgress()
        return ApiClient.getClient()
                .updatePassword(userId,"change",oldPassword,newPassword)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onResponse() },this::onError)
    }

    fun changeProfilePic(userId: String,filePath: String) : Disposable {
        view?.showProgress()
        val file     = File(filePath)
        val id       = RequestBody.create(MediaType.parse("text/plain"),userId)
        val fileBody = RequestBody.create(MediaType.parse("image/jpeg"), file)
        val body     = MultipartBody.Part.createFormData("image", file.name, fileBody)
        return ApiClient.getClient()
                .uploadProfilePic(id,body)
                .doOnSuccess {
                    updatePicOnFireBase(it.user.user_id,it.user.pic)
                    SessionManager.saveUser(context ?: return@doOnSuccess,it.user)
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onResponse(it)
                    Toast.makeText(context, R.string.profile_pic_updated,Toast.LENGTH_SHORT).show()
                },this::onError)
    }

    private fun onResponse(response: UpdateProfileResponse) {
        view?.hideProgress()
        view?.onResponse(response)
    }

    private fun onResponse() {
        view?.hideProgress()
        view?.onResponse()
        DialogUtils.openDialog(context?: return,"Password has been changed successfully")
    }

    private fun onError(throwable: Throwable) {
        view?.hideProgress()
        view?.onError(throwable)
    }

    private fun updateNameOnFireBase(userId: String,name: String?) {
        if(userId.isBlank() || name.isNullOrBlank())
            return

        FirebaseNodeRef.getLifetimeMasterRef(userId).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if(dataSnapshot?.exists() == true && dataSnapshot.hasChildren()) { }
            }

        })
    }


    private fun updatePicOnFireBase(userId: String,pic: String?) {
        if(userId.isBlank() || pic.isNullOrBlank())
            return

        FirebaseNodeRef.getLifetimeMasterRef(userId).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if(dataSnapshot?.exists() == true && dataSnapshot.hasChildren()) { }
            }

        })
    }
}