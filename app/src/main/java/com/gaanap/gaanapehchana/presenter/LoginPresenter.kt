package com.gaanap.gaanapehchana.presenter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.LoginView
import com.gaanap.gaanapehchana.models.response.LoginResponse
import com.gaanap.gaanapehchana.models.request.RegisterRequest
import com.gaanap.gaanapehchana.utility.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import java.util.*
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.twitter.sdk.android.core.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginPresenter(private val activity:Activity, val view: LoginView) {

    private val tag  = "LoginActivity"
    private var type = ""

    fun loginWithFacebook(callbackManager: CallbackManager?) {
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, _ ->
                    type = "Facebook"
                    getSocialMediaInfo(`object`?.optString("name"),`object`?.optString("email",""),`object`?.optString("id"),`object`?.optJSONObject("picture")?.optJSONObject("data")?.optString("url"),"F")
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,picture.type(small)")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                Log.d(tag,"FbLoginError Cancel")
                EventUtils.logGroupClickEvent(SIGN_UP,FAILURE,"Facebook")
            }

            override fun onError(exception: FacebookException) {
                Log.d(tag, "FbLoginError:$exception")
                EventUtils.logGroupClickEvent(SIGN_UP,FAILURE,"Facebook")
            }
        })

        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"))
    }

    fun loginWithTwitter(authClient: TwitterAuthClient?) {
        authClient!!.authorize(activity, object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                requestForEmail(result.data)
            }

            override fun failure(e: TwitterException) {
                Log.e(tag, "TwitterException:$e")
                EventUtils.logGroupClickEvent(SIGN_UP,FAILURE,"Twitter")
            }
        })
    }

    private fun requestForEmail(session: TwitterSession) {
        TwitterAuthClient().requestEmail(session, object : Callback<String>() {
            override fun success(result: Result<String>) {
                type = "Twitter"
                getSocialMediaInfo(session.userName,result.data,session.id.toString(),"","T")
            }

            override fun failure(exception: TwitterException) {
                Log.e(tag, "TwitterException:$exception")
                EventUtils.logGroupClickEvent(SIGN_UP,FAILURE,"Twitter")
            }
        })
    }


    fun loginWithGoogle() {
        val googleSignInAccount = GoogleSignIn.getLastSignedInAccount(activity)
        if (googleSignInAccount != null) {
            type = "Google"
            getSocialMediaInfo(googleSignInAccount.displayName,googleSignInAccount.email,googleSignInAccount.id,googleSignInAccount.photoUrl.toString(),"G")
        } else {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
            activity.startActivityForResult(GoogleSignIn.getClient(activity,gso).signInIntent,RC_SIGN_IN)
        }
    }

    fun handleSignInResult(data: Intent?) {
        try {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val googleSignInAccount = task.getResult(ApiException::class.java)
            if(googleSignInAccount != null) {
                type = "Google"
                getSocialMediaInfo(googleSignInAccount.displayName, googleSignInAccount.email, googleSignInAccount.id, googleSignInAccount.photoUrl.toString(), "G")
            }
        } catch (e: ApiException) {
            Log.d(tag, "signInResult:failed code=${e.statusCode}")
            EventUtils.logGroupClickEvent(SIGN_UP,FAILURE,"Google")
        }

    }

    private fun getSocialMediaInfo(fullName: String?,email: String?,id: String?,profilePic: String?,source: String) {

        val request = RegisterRequest(fullName,email,null,source,id,profilePic)

        if(TextUtils.isEmpty(email))
            view.onEmailMissing(request)
        else
            register(request)
    }

    fun register(request: RegisterRequest) {
        ApiClient.getClient().register(request)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess {
                    MyPreference.onUpdateDecades(activity,it.data.user.myDecades)
                    MyPreference.onUpdateDifficulty(activity,it.data.user.myDifficulty)
                }
                .subscribe(this::onResponse,this::onError)
    }

    private fun onResponse(response: LoginResponse) {
        if(type.isNotBlank()) EventUtils.logGroupClickEvent(SIGN_UP,SUCCESS,type)
        view.hideProgress()
        view.onResponse(response)
    }

    private fun onError(throwable: Throwable) {
        if(type.isNotBlank()) EventUtils.logGroupClickEvent(SIGN_UP,FAILURE,type)
        view.hideProgress()
        view.onError(throwable)
    }
}