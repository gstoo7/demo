package com.gaanap.gaanapehchana.presenter;

import android.text.TextUtils;

import com.gaanap.gaanapehchana.api.ApiClient;
import com.gaanap.gaanapehchana.interfaces.RadioView;
import com.gaanap.gaanapehchana.models.response.ChannelsResponse;
import com.gaanap.gaanapehchana.models.response.PlayList;
import com.gaanap.gaanapehchana.models.response.RadioSearchResponse;
import com.gaanap.gaanapehchana.models.response.SongCategory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.realm.Realm;

public class RadioPresenter {

    private RadioView view;
    //private AllSongsRepository repository;

    public RadioPresenter(RadioView view) {
        this.view  = view;
        //repository = new AllSongsRepository();
    }

    public Disposable getAllSongs(int page,int limit) {
        view.showProgress();
        return ApiClient.getClient().getChannels(page,limit)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse, this::onError);
    }


    public Disposable getMyPlayList(String userId) {
        view.showProgress();
        return ApiClient.getClient().getMyPlayList(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,this::onError);
    }

    public Disposable getPublicPlayList(String userId) {
        view.showProgress();
        return ApiClient.getClient().getPublicPlayList(userId)
                .flatMap(Observable::fromIterable)
                .filter(res -> res.getTotalSongs() > 0)
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> view.onResponse(new ArrayList<>(list)),this::onError);
    }

    private List<PlayList> getPlayListSongsFromDB(String playListId) {
        List<PlayList> list = null;
        Realm realm = Realm.getDefaultInstance();
        SongCategory result = realm.where(SongCategory.class).equalTo("id",playListId).findFirst();
        if(result != null)
            list = realm.copyFromRealm(result).getPlayList();
        realm.close();
        return list;
    }

    public void getPlayListSongs(String playListId,String userId) {
        view.showProgress();
        List<PlayList> list = getPlayListSongsFromDB(playListId);
        if(list != null && list.size() > 0)
            onResponse(list);
        else {
            ApiClient.getClient().getPlaylist(playListId,userId)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onResponse, this::onError);
        }
    }

    public Disposable createPlayList(String title,int privacy,String userId) {
        view.showProgress();
        return ApiClient.getClient()
                .createPlayList(title,privacy,userId," ")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,this::onError);
    }

    public Disposable addToPlayList(String songId,String playlistId) {
        view.showProgress();
        return ApiClient.getClient()
                .addToPlayList(songId,playlistId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,this::onError);
    }

    public Disposable deletePlayList(String pid) {
        view.showProgress();
        return ApiClient.getClient()
                .deletePlayList(pid)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,this::onError);
    }

    public Disposable removeSongFromPlayList(String playListId,String songId) {
        view.showProgress();
        return ApiClient.getClient()
                .removeSongFromPlayList(playListId,songId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,this::onError);
    }

    public Observable<RadioSearchResponse> radioSearch(String query, String userId) {
        view.showProgress();
        return ApiClient.getClient()
                .radioSearch("all",query,userId)
                .debounce(200,TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Disposable searchQuery(PublishSubject<String> subject,String userId) {
        return subject
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter(searchString -> !TextUtils.isEmpty(searchString))
                .distinctUntilChanged()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.io())
                .switchMap(query -> ApiClient.getClient().radioSearch("all",query,userId)).onErrorResumeNext(Observable.create(errorSubject -> errorSubject.onNext(new RadioSearchResponse(1,new ArrayList<>()))))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,view::onError);
    }

    private void onResponse(ChannelsResponse response) {
        view.hideProgress();
        view.onResponse(response);
    }

    private void onResponse(List<PlayList> playLists) {
        view.hideProgress();
        view.onResponse(playLists);
    }

    private void onError(Throwable throwable) {
        view.hideProgress();
        view.onError(throwable);
    }
}
