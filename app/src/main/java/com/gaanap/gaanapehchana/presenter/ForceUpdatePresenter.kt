package com.gaanap.gaanapehchana.presenter

import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.ForceUpdateView
import com.gaanap.gaanapehchana.models.response.ForceUpdateResponse
import com.gaanap.gaanapehchana.models.response.TMKGameCategory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

class ForceUpdatePresenter(private val view: ForceUpdateView) {

    fun checkForForceUpdate() : Disposable {
        view.showProgress()
        return ApiClient.getClient()
                .forceUpdate("android")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(this::onUpdateTMKGameCategory)
                .subscribe(this::onResponse,this::onError)
    }

    private fun onUpdateTMKGameCategory(response: ForceUpdateResponse?) {
        val categories = response?.categories ?: return
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                realm.delete(TMKGameCategory::class.java)
                realm.insertOrUpdate(categories)
            }
        }
    }

//    fun onUpdateDecades(response: ForceUpdateResponse) {
//        val realm   = Realm.getDefaultInstance()
//        val results = realm.where(Preference::class.java).equalTo("isDifficulty",false).findAll()
//        if(results.size <= 0) {
//            response.decades.forEach {
//                realm.beginTransaction()
//                val decade = realm.createObject(Preference::class.java,it.id)
//                decade.labelName    = it.label_name
//                decade.isSelected   = true
//                decade.isDifficulty = false
//                realm.commitTransaction()
//            }
//        } else {
//            response.decades.forEach {
//                val decade = realm.where(Preference::class.java).equalTo("id",it.id)
//                if(decade == null) {
//                    realm.beginTransaction()
//                    val newDecade = realm.createObject(Preference::class.java,it.id)
//                    newDecade.labelName    = it.label_name
//                    newDecade.isSelected   = true
//                    newDecade.isDifficulty = false
//                    realm.commitTransaction()
//                }
//            }
//        }
//
//        realm.close()
//    }

    fun onResponse(response: ForceUpdateResponse) {
        view.hideProgress()
        view.onResponse(response)
    }

    fun onError(throwable: Throwable) {
        view.hideProgress()
        view.onError(throwable)
    }
}