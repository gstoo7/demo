package com.gaanap.gaanapehchana.presenter

import android.content.Context
import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.LoginView
import com.gaanap.gaanapehchana.models.request.LoginRequest
import com.gaanap.gaanapehchana.models.response.LoginResponse
import com.gaanap.gaanapehchana.utility.MyPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SignInPresenter(val context: Context,val view: LoginView) {

    fun login(email:String,password:String) : Disposable {
        view.showProgress()
       return ApiClient.getClient()
               .login(LoginRequest(email,password))
               .subscribeOn(Schedulers.newThread())
               .observeOn(AndroidSchedulers.mainThread())
               .doOnSuccess {
                   MyPreference.onUpdateDecades(context,it.data.user.myDecades)
                   MyPreference.onUpdateDifficulty(context,it.data.user.myDifficulty)
               }
               .subscribe(this::onResponse,this::onError)
    }

    private fun onResponse(loginResponse: LoginResponse) {
        view.hideProgress()
        view.onResponse(loginResponse)
    }

    private fun onError(throwable: Throwable) {
        view.hideProgress()
        view.onError(throwable)
    }
}