package com.gaanap.gaanapehchana.presenter

import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.TMKGameView
import com.gaanap.gaanapehchana.models.response.TMKGameCategory
import com.gaanap.gaanapehchana.models.response.TMKListResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

class TMKPresenter(val view:TMKGameView) {

    fun getGameCategories() : List<TMKGameCategory> {
        Realm.getDefaultInstance().use {
            val results = it.where(TMKGameCategory::class.java).findAll()
            return it.copyFromRealm(results)
        }
    }

    fun getGameList(userId: String,page:Int,limit: Int,categorySlug: String = "") : Disposable {
        view.showProgress()
        return ApiClient.getClient().getGamesList(userId,page,categorySlug,limit)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,view::onError)
    }
}