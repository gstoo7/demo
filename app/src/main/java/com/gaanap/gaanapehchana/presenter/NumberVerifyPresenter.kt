package com.gaanap.gaanapehchana.presenter

import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.NumberVerifyView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class NumberVerifyPresenter(private val view: NumberVerifyView){

    fun verifyNumber(userId: String,mobile: String,countryCode: String) : Disposable {
        view.showProgress()
        return ApiClient.getClient()
                .verifyNumber(userId,mobile,countryCode)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,view::onError)
    }

    fun verifyOTP(userId: String,mobile: String,enteredOTP: String,sentOTP: String) : Disposable {
        view.showProgress()
        return ApiClient.getClient()
                .verifyOTP(userId,mobile,enteredOTP,sentOTP)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,view::onError)
    }
}