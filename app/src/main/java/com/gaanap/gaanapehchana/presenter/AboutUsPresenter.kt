package com.gaanap.gaanapehchana.presenter

import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.AboutUsView
import com.gaanap.gaanapehchana.models.response.AboutUsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class AboutUsPresenter(private val view: AboutUsView) {

    fun aboutUs() : Disposable {
        view.showProgress()
        return ApiClient.getClient()
                .aboutUs()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse,this::onError)
    }

    fun onResponse(response: AboutUsResponse) {
        view.hideProgress()
        view.onResponse(response)
    }

    fun onError(throwable: Throwable) {
        view.hideProgress()
        view.onError(throwable)
    }
}