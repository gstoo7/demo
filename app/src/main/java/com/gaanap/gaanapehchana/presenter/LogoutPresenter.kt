package com.gaanap.gaanapehchana.presenter

import com.gaanap.gaanapehchana.api.ApiClient
import com.gaanap.gaanapehchana.interfaces.CommonView
import com.gaanap.gaanapehchana.interfaces.ProfileView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class LogoutPresenter(val view: ProfileView) {

    fun logout(userId: String,deviceId: String) : Disposable {
        return ApiClient.getClient()
                .logout(userId,deviceId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onResponse,view::onError)
    }
}