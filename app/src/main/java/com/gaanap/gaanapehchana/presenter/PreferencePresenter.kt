package com.gaanap.gaanapehchana.presenter

import android.content.Context
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.Preference
import io.realm.Realm
import io.realm.RealmResults

class PreferencePresenter {

    fun getDecadesFromDb(context: Context,realm: Realm): RealmResults<Preference> {
        val results = realm.where(Preference::class.java).equalTo("isDifficulty",false).findAll().sort("id")
        if(results.size <= 0) {
            val decadeIds    = context.resources.getIntArray(R.array.decades)
            val decadeLevels = context.resources.getStringArray(R.array.decades_name)
            for(i in 0 until decadeIds.size) {
                realm.beginTransaction()
                val decade = realm.createObject(Preference::class.java,decadeIds[i])
                decade.labelName    = decadeLevels[i]
                decade.isSelected   = true
                decade.isDifficulty = false
                realm.commitTransaction()
            }
        }
        return results
    }

    fun getDifficultyFromDb(context: Context,realm: Realm): RealmResults<Preference> {
        val results = realm.where(Preference::class.java).equalTo("isDifficulty",true).findAll()
        if(results.size <= 0) {
            val diffLevels = context.resources.getStringArray(R.array.difficultLevels)
            for(i in 0 until diffLevels.size) {
                realm.beginTransaction()
                val diff = realm.createObject(Preference::class.java,i)
                diff.labelName    = diffLevels[i]
                diff.isSelected   = true
                diff.isDifficulty = true
                realm.commitTransaction()
            }
        }
        return results
    }


    fun getSelectedDecades(): List<Preference> {
        val realm   = Realm.getDefaultInstance()
        val results = realm.where(Preference::class.java).equalTo("isDifficulty",false).equalTo("isSelected",true).findAll()
        val decades = realm.copyFromRealm(results)
        realm.close()
        return decades
    }

    fun getSelectedDifficulty(): List<Preference> {
        val realm      = Realm.getDefaultInstance()
        val results    = realm.where(Preference::class.java).equalTo("isDifficulty",true).equalTo("isSelected",true).findAll()
        val difficulty = realm.copyFromRealm(results)
        realm.close()
        return difficulty
    }

    fun getDecadesAndDifficulty() : HashMap<String,String> {
        val map = HashMap<String,String>()
        val difficulty = getSelectedDifficulty()
        for (i in 0 until difficulty.size)
            map["difficulty_level[$i]"] = difficulty[i].labelName

        val decades = getSelectedDecades()
        for (i in 0 until decades.size)
            map["decade_ids[$i]"] = decades[i].id.toString()

        return map
    }
}