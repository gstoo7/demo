package com.gaanap.gaanapehchana.services.jobs

import android.content.Context
import android.os.Bundle
import com.firebase.jobdispatcher.*
import com.gaanap.gaanapehchana.presenter.ProfilePresenter
import com.gaanap.gaanapehchana.utility.USER_ID
import io.reactivex.disposables.CompositeDisposable

class SettingsJob : JobService() {

    val presenter  = ProfilePresenter(this,null)
    val disposable = CompositeDisposable()

    override fun onStartJob(job: JobParameters): Boolean {
        val param  = job.extras?.getString(PARAM1)  ?: ""
        val userId = job.extras?.getString(USER_ID) ?: ""
        val user   = SessionManager.getUser(this)

        if(user == null || user.user_id.isNullOrBlank() || param.isBlank() || userId.isBlank() || user.user_id != userId)
            return false

        presenter.updateNotificationSetting(user,param)
                .subscribe({
                    SessionManager.saveUser(this,it.user)
                    jobFinished(job,false)
                },{ jobFinished(job,true) })

        return true
    }

    override fun onStopJob(job: JobParameters): Boolean {
        disposable.clear()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    companion object {

        private const val PARAM1 = "param1"

        fun scheduleSettingsTask(context: Context,userId: String,request: String) {
            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context.applicationContext))
            val bundle = Bundle().apply {
                putString(PARAM1,request)
                putString(USER_ID,userId)
            }

            val myJob = dispatcher.newJobBuilder()
                    .setService(SettingsJob::class.java)
                    .setTag("SettingsJob")
                    .setReplaceCurrent(true)
                    .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                    .setTrigger(Trigger.executionWindow(10,10))
                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                    .setConstraints(Constraint.ON_ANY_NETWORK)
                    .setExtras(bundle)
                    .build()
            dispatcher.mustSchedule(myJob)
        }
    }
}



