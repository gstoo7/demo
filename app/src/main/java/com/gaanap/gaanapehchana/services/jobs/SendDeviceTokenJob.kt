package com.gaanap.gaanapehchana.services.jobs

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import android.util.Log
import com.firebase.jobdispatcher.Constraint
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService
import com.firebase.jobdispatcher.Lifetime
import com.firebase.jobdispatcher.RetryStrategy
import com.firebase.jobdispatcher.Trigger
import com.gaanap.gaanapehchana.api.ApiClient
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.schedulers.Schedulers

class SendDeviceTokenJob : JobService() {

    @SuppressLint("HardwareIds")
    override fun onStartJob(job: JobParameters): Boolean {
        Log.d(TAG, "onStartJob=" + job.tag)
        val userId = SessionManager.getUser(this)?.user_id
        if(userId.isNullOrBlank())
            return false

        val deviceToken = FirebaseInstanceId.getInstance().token
        if(deviceToken.isNullOrBlank())
            return false

        val deviceId = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)

        ApiClient.getClient()
                .updateDeviceToken(userId,deviceId,deviceToken,"ANDROID")
                .subscribeOn(Schedulers.newThread())
                .subscribe({
                    SessionManager.isDeviceTokenSend(this,true)
                    jobFinished(job,false)
                },{ jobFinished(job,true) })

        return true
    }

    override fun onStopJob(job: JobParameters): Boolean {
        Log.d(TAG, "onStopJob=" + job.tag)
        return true
    }


    companion object {
        private const val TAG = "SendDevice"

        fun scheduleTaskForDeviceToken(context: Context) {
            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context.applicationContext))
            val myJob = dispatcher.newJobBuilder()
                    .setService(SendDeviceTokenJob::class.java)
                    .setTag(TAG)
                    .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                    .setTrigger(Trigger.executionWindow(0, 0))
                    .setReplaceCurrent(true)
                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                    .setConstraints(Constraint.ON_ANY_NETWORK)
                    .build()

            dispatcher.mustSchedule(myJob)
        }
    }
}