package com.gaanap.gaanapehchana.services.jobs

import android.content.Context
import com.firebase.jobdispatcher.*
import com.gaanap.gaanapehchana.presenter.PreferencePresenter
import com.gaanap.gaanapehchana.presenter.ProfilePresenter
import io.reactivex.disposables.CompositeDisposable

class UpdatePreferenceJob : JobService() {

    private val presenter     = ProfilePresenter(this,null)
    private val prefPresenter = PreferencePresenter()
    private val disposable    = CompositeDisposable()

    override fun onStartJob(job: JobParameters): Boolean {

        val user = SessionManager.getUser(this) ?: return false

        val decadesBuilder    = StringBuilder()
        val difficultyBuilder = StringBuilder()

        val selectedDecades    = prefPresenter.getSelectedDecades()
        val selectedDifficulty = prefPresenter.getSelectedDifficulty()

        selectedDecades    .forEach { decadesBuilder.append(it.id).append(",")    }
        selectedDifficulty .forEach { difficultyBuilder.append(it.labelName).append(",") }

        val decades    = if(decadesBuilder.isNotEmpty())     decadesBuilder.dropLast(1).toString()    else ""
        val difficulty = if(selectedDifficulty.isNotEmpty()) difficultyBuilder.dropLast(1).toString() else ""

        if(decades.isBlank() && difficulty.isBlank())
            return false

        presenter.updatePreference(user,decades,difficulty)
                .subscribe({
                    SessionManager.saveUser(this,it.user)
                    jobFinished(job,false)
                },{ jobFinished(job,true) })

        return true
    }

    override fun onStopJob(job: JobParameters): Boolean {
        disposable.clear()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    companion object {

        fun schedulePreferenceTask(context: Context) {
            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context.applicationContext))

            val myJob = dispatcher.newJobBuilder()
                    .setService(UpdatePreferenceJob::class.java)
                    .setTag("PreferenceJob")
                    .setReplaceCurrent(true)
                    .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                    .setTrigger(Trigger.executionWindow(10,20))
                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                    .setConstraints(Constraint.ON_ANY_NETWORK)
                    .build()
            dispatcher.mustSchedule(myJob)
        }
    }
}