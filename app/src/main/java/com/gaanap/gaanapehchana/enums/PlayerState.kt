package com.gaanap.gaanapehchana.enums

enum class PlayerState {
    INIT,PREPARING,START,STOP,PAUSE,AUTO_PAUSE
}