package com.gaanap.gaanapehchana.enums

enum class TimerState {
    INIT, START, PAUSE, STOP
}