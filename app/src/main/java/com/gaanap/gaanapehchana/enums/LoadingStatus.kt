package com.gaanap.gaanapehchana.enums

enum class LoadingStatus {
   SHOW_PROGRESS,HIDE_PROGRESS,ERROR,DATA,PROGRESS,HALF_PROGRESS
}