package com.gaanap.gaanapehchana.enums

enum class ScoreType {
    WEEKLY,MONTHLY,LIFE_TIME
}