package com.gaanap.gaanapehchana.enums

enum class DownloadingStatus {
    INIT,STARTING,DONE,ERROR
}