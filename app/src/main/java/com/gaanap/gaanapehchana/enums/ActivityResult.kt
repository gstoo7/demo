package com.gaanap.gaanapehchana.enums

enum class ActivityResult(val value: Int) {
    NEW_GAME(0),REPLAY(1)
}