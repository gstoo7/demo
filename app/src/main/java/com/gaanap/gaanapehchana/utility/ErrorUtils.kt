package com.gaanap.gaanapehchana.utility

import com.gaanap.gaanapehchana.models.response.ErrorResponse
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import java.io.IOException
import retrofit2.HttpException
import retrofit2.Response

object ErrorUtils {

    fun parseErrorResponse(throwable: Throwable): ErrorResponse? {
        if (throwable is HttpException) {
            val response = throwable.response()
            if (response?.errorBody() != null) {
                try {
                    return Gson().fromJson(response.errorBody()!!.string(), ErrorResponse::class.java)
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
        return null
    }
}
