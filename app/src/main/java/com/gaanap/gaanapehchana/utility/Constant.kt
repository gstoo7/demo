package com.gaanap.gaanapehchana.utility

val LAVEL           = arrayOf("Sa", "Re", "Ga", "Ma", "Pa")
val levelColors     = hashMapOf("sa" to "#000000", "re" to "#48007b", "ga" to "#b50000", "ma" to "#07077c", "pa" to "#ff7d26")
//val playIconArray   = intArrayOf(R.drawable.tmk_play_icon, R.drawable.bbg_play_icon, R.drawable.aursunao_play_icon)
//val playBgIconArray = intArrayOf(R.drawable.circle_btn_tmk, R.drawable.circle_bbg, R.drawable.circle_asn)


const val ITEM    = 1
const val LOADING = 0

const val RC_SIGN_IN     = 101
const val backActionCode = 1001

const val DIALOG  = "dialog"
const val GAME    = "game"
const val GAME_ID = "gameId"
const val SCORE   = "score"
const val THEME   = "theme"
const val RESULT  = "result"
const val IS_WIN  = "is_win"

const val PREVIOUS_SCORE       = "previousScore"
const val SELECTED_OPTION_LIST = "selected_option_list"
const val PLAY_LIST            = "playList"
const val PLAY_LIST_ID         = "playListId"
const val PLAY_LIST_NAME       = "playListName"
const val IS_MY_PLAY_LIST      = "isMyPlayList"

const val POSITION  = "position"
const val SONG_LIST = "songList"
const val USER_ID   = "user_id"
const val TYPE      = "type"
const val FULL_NAME = "full_name"
const val MOBILE    = "mobile"
const val COUNTRY   = "country"

const val GAME_PLAYED_INFO = "info"
const val SELECTED_COLOR   = "selectedColor"


const val MILLI_SECONDS = 1000
const val SECONDS       = 60


// for permission request code
const val EXTERNAL_STORAGE_PC     = 201
const val CAMERA_PC               = 205


//Fragment TAG Name
const val HOME_FRAGMENT         = "HomeFragment"
const val TMK_LIST_FRAGMENT     = "TMKListFragment"
const val GAME_WIN_FRAGMENT     = "GameWinFragment"
const val MY_MUSIC_FRAGMENT     = "MyMusicFragment"
const val PLAYLIST_FRAGMENT     = "PlayListSongs"
const val PLAYER_FRAGMENT       = "PlayerFragment"
const val RADIO_SEARCH_FRAGMENT = "RadioSearchFragment"
const val GP_RADIO_FRAGMENT     = "GPRadioFragment"
const val BLOG_FRAGMENT         = "blogsFragment"
const val PROFILE_FRAGMENT      = "ProfileFragment"
const val ABOUT_US_FRAGMENT     = "AboutUsFragment"
const val SETTINGS_FRAGMENT     = "SettingsFragment"
const val CONTACT_US_FRAGMENT   = "ContactUsFragment"


//Mix Panel Event names
const val TMKGameBoard         = "Tmk_Game_Board"
const val LEADERBOARD_BUTTON   = "Leaderboard_button"
const val HOW_TO_PLAY          = "How_To_Play"
const val MY_MUSIC_SEARCH      = "My_Music_Search"
const val MY_MUSIC_PL_CREATION = "My_Music_playlist_creation"
const val TMK_SEARCH_BUTTON    = "TMK_Search_button"
const val SONG_LISTEN_COUNT    = "Song_listen_Count"
const val LOGOUT_COUNT         = "Logout_count"


const val SIGN_UP  = "Signup"
const val SUCCESS  = "Success"
const val FAILURE  = "Failure"

const val TMK                  = "TMK"
const val BBG                  = "BBG"
const val ASN                  = "ASN"
const val GAME_CHOICE          = "Game choice"
const val MOST_SELECTED        = "Most_selected"
const val GAME_COMPLETION      = "Game_Completion"
const val GAME_IN_BETWEEN_LEFT = "Game_inbetween_left"

const val DECADES              = "Decades"
const val LEVEL                = "Level"
const val CHANNEL              = "Channel"
const val TAB                  = "Tab"
const val MY_MUSIC             = "My_Music"
const val MOST_TIME_SPENT      = "Most_time_spent"

const val MOST_TIME_SPENT_IN_TAB       = "Most_time_spent_in_Tab"
const val TIME_OF_GAME_IN_BETWEEN_LEFT = "Time_of_Game_inbetween_left"


