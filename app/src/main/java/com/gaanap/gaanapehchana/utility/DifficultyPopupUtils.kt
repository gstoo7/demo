package com.gaanap.gaanapehchana.utility

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.Preference
import com.gaanap.gaanapehchana.services.jobs.UpdatePreferenceJob
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.view_difficulty_level_popup.view.*

class DifficultyPopupUtils(val context: Context, val parent: ViewGroup, val realm: Realm) : PopupWindow(context) {

    private val views          = arrayListOf<TextView>()
    private var selectedDiffBg = R.drawable.rect_gradient_radius_20dp_bbg

    private var selectedDiffCount = 0

    init {
        contentView = parent.inflate(R.layout.view_difficulty_level_popup)
        width       = ViewGroup.LayoutParams.WRAP_CONTENT
        height      = ViewGroup.LayoutParams.WRAP_CONTENT
        isOutsideTouchable = true
        setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.rect_white_with_shadow_corners_20dp))
    }

    fun setSelectedDiffBg(desId: Int) {
        selectedDiffBg = desId
    }

    fun showPopup(view: View) {
        isFocusable  = true
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        showAtLocation(contentView, Gravity.NO_GRAVITY,location[0], location[1] + dp2px(10)+view.measuredHeight)
    }

    fun setData(diffList: RealmResults<Preference>) {
        views.add(contentView.saLevel)
        views.add(contentView.reLevel)
        views.add(contentView.gaLevel)
        views.add(contentView.maLevel)
        views.add(contentView.paLevel)

        for (i in 0 until views.size) {
            if(diffList[i]?.isSelected == true) {
                views[i].setBackgroundResource(selectedDiffBg)
                views[i].setTextColor(Color.WHITE)
                selectedDiffCount += 1
            } else {
                views[i].setBackgroundResource(R.drawable.rect_transparent_with_black_boder_20dp)
                views[i].setTextColor(Color.BLACK)
            }

            views[i].setOnClickListener { onDifficultyClicks(it,diffList[i]) }
        }

    }


    private fun onDifficultyClicks(view: View,decade: Preference?) {
        val textView = view as TextView
        if(decade?.isSelected == true) {
            if(selectedDiffCount == 1) {
                Toast.makeText(context,R.string.pls_select_one_difficulty,Toast.LENGTH_LONG).show()
                return
            }
            realm.beginTransaction()
            decade.isSelected = false
            realm.commitTransaction()
            textView.setBackgroundResource(R.drawable.rect_transparent_with_black_boder_20dp)
            textView.setTextColor(Color.BLACK)
            selectedDiffCount -= 1
        } else {
            realm.beginTransaction()
            decade?.isSelected = true
            realm.commitTransaction()
            textView.setBackgroundResource(selectedDiffBg)
            textView.setTextColor(Color.WHITE)
            selectedDiffCount += 1
        }

        UpdatePreferenceJob.schedulePreferenceTask(context)
    }
}