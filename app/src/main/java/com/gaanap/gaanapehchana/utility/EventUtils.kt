package com.gaanap.gaanapehchana.utility

import android.content.Context
import android.text.TextUtils
import android.util.Log

import com.gaanap.gaanapehchana.GaanaPApp

import org.json.JSONException
import org.json.JSONObject

import java.util.Date
import java.util.HashMap

private const val TAG   = "EventUtils"
private const val WHERE = "screenName"
private const val DATE  = "date"
private const val NAME  = "name"
private const val EMAIL = "Email"
private const val PHONE = "phone"
private const val PHOTO = "Photo"

object EventUtils {

    fun setCurrentUser(context: Context) {
        val user = SessionManager.getUser(context) ?: return
        GaanaPApp.sMixPanel.identify(user.user_id)
        val profileUpdate    = HashMap<String, Any>()
        profileUpdate[NAME]  = user.fullName
        profileUpdate[EMAIL] = user.email
        if (!TextUtils.isEmpty(user.pic))
            profileUpdate[PHOTO] = user.pic

        if (!TextUtils.isEmpty(user.country) && !TextUtils.isEmpty(user.mobileNumber))
            profileUpdate[PHONE] = user.country + user.mobileNumber

        val people = GaanaPApp.sMixPanel.people
        people.identify(user.user_id)
        people.setMap(profileUpdate)

        //OneSignal.setEmail(user.email)
    }

    private fun logEventOnMaxPanel(action: String, propertyKey: String?, propertyName: String?, screenName: String) {
        try {
            val jsonEvent = JSONObject()
            if (!TextUtils.isEmpty(propertyKey) && !TextUtils.isEmpty(propertyName))
                jsonEvent.put(propertyKey, propertyName)

            if(screenName.isBlank())
                jsonEvent.put(WHERE, screenName)

            jsonEvent.put(DATE, Date())
            GaanaPApp.sMixPanel.track(action, jsonEvent)
        } catch (e: JSONException) {
            e.printStackTrace()
            Log.e(TAG, "maxPanel:$e")
        }

    }

    fun logClickEvent(action: String, screenName: String = "") {
        logEventOnMaxPanel(action, null, null, screenName)
    }

    fun logGroupClickEvent(action: String, propertyKey: String, propertyName: String, screenName: String = "") {
        logEventOnMaxPanel(action, propertyKey, propertyName, screenName)
    }

    fun logTimeEvent(action: String) {
        GaanaPApp.sMixPanel.timeEvent(action)
    }
}