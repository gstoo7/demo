package com.gaanap.gaanapehchana.utility

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Rect
import android.net.ConnectivityManager
import android.support.annotation.StringRes
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import android.graphics.Typeface
import android.os.Environment
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.view.animation.AccelerateInterpolator
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.gaanap.gaanapehchana.BuildConfig
import com.gaanap.gaanapehchana.GlideApp
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.helper.Flip3dAnimation
import com.gaanap.gaanapehchana.helper.MyBounceInterpolator
import com.gaanap.gaanapehchana.models.Countries
import com.gaanap.gaanapehchana.models.Country
import com.gaanap.gaanapehchana.models.Preference
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.view.HomeActivity
import com.gaanap.gaanapehchana.widget.RoundedCornersTransformation
import com.google.gson.Gson
import io.realm.Realm
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.TimeUnit


inline fun <reified T : Activity> Activity.startActivity() = startActivity(Intent(this, T::class.java))

inline fun <reified T : Activity> Activity.startIntentActivity(result: Intent,resultCode: Int = -1) {
    result.setClass(this,T::class.java)
    startActivityForResult(result,resultCode)
}

fun startHomeActivity(activity: Activity?) {
    activity?.startIntentActivity<HomeActivity>(Intent().apply {
        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    })
}

fun AppCompatActivity.replaceFragment(@IdRes containerViewId: Int, fragment: Fragment, tag: String, addToBackStack: Boolean = false) {
    val transaction = supportFragmentManager.beginTransaction()
    transaction.replace(containerViewId,fragment,tag)
    if(addToBackStack)
        transaction.addToBackStack(tag)
    transaction.commitAllowingStateLoss()
}

fun TextView.getTextWidth() : Int {
    val result = Rect()
    paint.getTextBounds(text.toString(), 0, text.length, result)
    return result.width()
}

fun ViewGroup.inflate(layoutRes: Int,attachToRoot : Boolean = false): View = LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

fun Activity.showToast(@StringRes resId:Int,duration:Int = Toast.LENGTH_LONG) = showToast(getString(resId),duration)
fun Activity.showToast(msg: String,duration:Int = Toast.LENGTH_LONG) = Toast.makeText(this,msg,duration).show()

fun ImageView.loadUrl(url: String?,placeholder: Int = R.drawable.ic_avater) {
    try {
        GlideApp.with(this).load(url ?: "")
                .dontAnimate()
                .placeholder(placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(this)
    } catch (e: Exception) {
        Log.e("Error",": $e")
    }
}

fun ImageView.loadCircleImage(url: String?,placeholder: Int = R.drawable.ic_avater) {
    try {
        GlideApp.with(this)
                .load(url ?: "")
                .circleCrop()
                .placeholder(placeholder)
                .into(this)
    } catch (e: Exception) {
        Log.e("Error",": $e")
    }
}

fun ImageView.loadRoundedImage(url: String?,radius: Int = 10) {
    try {
        GlideApp.with(this)
                .load(url ?: "")
                .dontAnimate()
                .transform(MultiTransformation(RoundedCornersTransformation(dp2px(radius), 0)))
                .placeholder(R.drawable.ic_songs_placeholder)
                .into(this)
    } catch (e: Exception) {
        Log.e("Error",": $e")
    }
}

fun getDisplayMetrics() = Resources.getSystem().displayMetrics!!
fun getDeviceWidth()    = getDisplayMetrics().widthPixels
fun getDeviceHeight()   = getDisplayMetrics().heightPixels
fun dp2px(dp:Int)       = (dp * getDisplayMetrics().density + 0.5f).toInt()


fun isNetworkAvailable(context: Context): Boolean {
    val cm:ConnectivityManager? = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = cm?.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
}

fun TextView.addFont(name:String) {
    try {
        typeface = Typeface.createFromAsset(context.assets, "font/$name")
    } catch (e:Exception) {
        Log.d("fontError",": @e")
    }
}


fun logoutFromApp(context: Context) {
    try {
        logoutFromGoogle(context)
        MusicPlayer.stopMusicService()
        LoginManager.getInstance().logOut()
        SessionManager.clearPreferences(context)
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                realm.delete(Preference::class.java)
                realm.delete(PlayList::class.java)
            }
        }
    } catch (e: Exception) {
        Log.d("Error","in logout $e")
    }
}

fun logoutFromGoogle(context: Context) {
    val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail().build()
    GoogleSignIn.getClient(context, gso).signOut()
}


fun getDurationString(durationMs: Long): String {
    return String.format(Locale.getDefault(), "%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(durationMs),
            TimeUnit.MILLISECONDS.toSeconds(durationMs) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationMs)))
}

fun getAppVersion(context: Context) : String {
    try {
        val manager = context.packageManager
        val info = manager.getPackageInfo(context.packageName, 0)
        return info.versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }

    return "4.4"
}

fun getScreenShot(view: View) : Bitmap? {
    val screenView = view.rootView
    screenView.isDrawingCacheEnabled = true
    val bitmap = Bitmap.createBitmap(screenView.drawingCache)
    screenView.isDrawingCacheEnabled = false
    return bitmap
}

private fun saveToSDCard(bitmap: Bitmap,fileName: String) : File? {
    try {
        val dir = File(Environment.getExternalStorageDirectory().absolutePath + "/Screenshots")
        if(!dir.exists())
            dir.mkdirs()
        val file = File(dir, fileName)
        FileOutputStream(file).use {
            bitmap.compress(Bitmap.CompressFormat.PNG, 80,it)
        }
        return file
    } catch (e: Exception) {
        Log.d("error", "$e")
    }

    return null
}

fun shareScore(activity: Activity?) {
    val view    = activity?.window?.decorView?.findViewById<View>(android.R.id.content) ?: return
    val file    = saveToSDCard(getScreenShot(view) ?: return,"score.jpg")
    val uri     = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file ?: return)
    val intent  = Intent(Intent.ACTION_SEND)
    intent.type = "image/*"
//    intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Game Score")
//    intent.putExtra(android.content.Intent.EXTRA_TEXT, "")
    intent.putExtra(Intent.EXTRA_STREAM, uri)
    try {
        activity.startActivity(Intent.createChooser(intent, "Share Screenshot"))
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(activity, "No App Available", Toast.LENGTH_SHORT).show()
    }
}

private fun loadJSONFromAsset(activity: Activity?): String? {
    val json: String?
    try {
        val inputStream = activity?.assets?.open("Countries.json") ?: return null
        val size        = inputStream.available()
        val buffer      = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        json = String(buffer,Charset.defaultCharset())
    } catch (ex: IOException) {
        ex.printStackTrace()
        return null
    }

    return json
}

fun getAllCountries(activity: Activity?): List<Country> {
    val countries       = loadJSONFromAsset(activity) ?: return emptyList()
    val listOfCountries = Gson().fromJson<Countries>(countries,Countries::class.java)
    return listOfCountries?.countries ?: emptyList()
}

fun hideKeyboard(activity: Activity?) {
    try {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager ?: return
        imm.hideSoftInputFromWindow(activity.currentFocus.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    } catch (e: java.lang.Exception) {
        Log.e("Error", ":$e")
    }
}

fun addBounceEffect(view: ImageView?) {
    if(view == null)
        return

    val myAnim = AnimationUtils.loadAnimation(view.context, R.anim.bounce)
    val interpolator    = MyBounceInterpolator(0.2, 20.0)
    myAnim.interpolator = interpolator
    view.startAnimation(myAnim)
}

fun applyRotation(view: ImageView?,start:Float = 0f , end:Float = 180f) {
    if(view == null)
        return
    val centerX  = view.width / 2.0f
    val centerY  = view.height / 2.0f
    val rotation = Flip3dAnimation(start, end, centerX, centerY)
    rotation.duration     = 700
    rotation.interpolator = AccelerateInterpolator()
    view.startAnimation(rotation)
}
