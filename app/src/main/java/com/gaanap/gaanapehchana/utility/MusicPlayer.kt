package com.gaanap.gaanapehchana.utility

import android.app.Activity
import android.content.*
import android.os.IBinder
import com.gaanap.gaanapehchana.enums.PlayerType
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.services.MusicService
import java.util.*
import kotlin.collections.ArrayList

object MusicPlayer {

    private val mConnectionMap  = WeakHashMap<Context,ServiceBinder>()
    var mService: MusicService? = null

    fun bindToService(context: Context?, callback: ServiceConnection): ServiceToken? {
        if(context == null)
            return null
        var realActivity: Activity? = (context as Activity).parent
        if (realActivity == null) {
            realActivity = context
        }
        val contextWrapper = ContextWrapper(realActivity)
        contextWrapper.startService(Intent(contextWrapper, MusicService::class.java))
        val binder = ServiceBinder(callback)
        if (contextWrapper.bindService(Intent().setClass(contextWrapper, MusicService::class.java), binder, 0)) {
            mConnectionMap[contextWrapper] = binder
            return ServiceToken(contextWrapper)
        }
        return null
    }

    fun unbindFromService(token: ServiceToken?) {
        if (token == null) {
            return
        }
        val mContextWrapper = token.mWrappedContext
        val mBinder = mConnectionMap.remove(mContextWrapper) ?: return
        mContextWrapper.unbindService(mBinder)
    }

    fun registerServicePlayerListener(playerServiceListener: MusicService.PlayerServiceListener) {
        mService?.registerServicePlayerListener(playerServiceListener)
    }

    fun unregisterServicePlayerListener(playerServiceListener: MusicService.PlayerServiceListener) {
        mService?.unregisterServicePlayerListener(playerServiceListener)
    }

    fun pause()              = mService?.pause()
    fun next()               = mService?.next()
    fun previous()           = mService?.prev()
    fun isPlaying()          = mService?.isPlaying   ?: false
    fun isShuffling()        = mService?.isShuffling ?: false
    fun isRepeating()        = mService?.isRepeating ?: false
    fun getPlayList()        = mService?.playList
    fun getPlayListName()    = mService?.playListName
    fun getPlayListId()      = mService?.playListId
    fun seekTo(time: Int)    = mService?.seekTo(time)
    fun getSongPosition()    = mService?.songPosition
    fun getCurrentAudio()    = mService?.currentAudio
    fun getCurrentPosition() = mService?.currentPosition ?: 0
    fun getDuration()        = mService?.duration ?: 0
    fun getPlayerType()      = mService?.playerType
    fun stop()               = mService?.stop()

    fun play(songsIndex: Int)                   = mService?.play(songsIndex)
    fun repeatSongs(isRepeat: Boolean)          = mService?.repeatSongs(isRepeat)
    fun shuffleSongs(isShuffling: Boolean)      = mService?.shuffleSongs(isShuffling)

    fun setPlayList(list: ArrayList<PlayList>?) {
        mService?.playList = list
    }

    fun setPlayListName(name: String): Unit? {
        return mService?.setPlayListName(name)
    }

    fun setPlayListId(id: String): Unit? {
        return mService?.setPlayListId(id)
    }

    fun setPlayerType(playerType: PlayerType) {
        mService?.playerType = playerType
    }

    fun stopMusicService() {
        stop()
        mConnectionMap.clear()
        mService?.stopSelf()
        mService = null
    }

    class ServiceBinder(private val mCallback: ServiceConnection?) : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as MusicService.PlayerServiceBinder
            mService   = binder.service
            mCallback?.onServiceConnected(className, service)
        }

        override fun onServiceDisconnected(className: ComponentName) {
            mCallback?.onServiceDisconnected(className)
            mService = null
        }
    }

    class ServiceToken(var mWrappedContext: ContextWrapper)
}