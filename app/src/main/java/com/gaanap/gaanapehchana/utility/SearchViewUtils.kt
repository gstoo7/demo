package com.gaanap.gaanapehchana.utility

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.widget.TextView
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewAnimationUtils
import com.gaanap.gaanapehchana.R


class SearchViewUtils(val activity: Activity,val listener: (Boolean)->Unit) {

    private  var searchMenu     : Menu?             = null
    internal var itemSearch     : MenuItem?         = null
    internal var searchListener : ((String)->Unit)? = null


    fun setSearchTollbar(searchToolbar: Toolbar) {
        searchToolbar.setNavigationOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                circleReveal(searchToolbar, 1, true, false)
            else
                searchToolbar.visibility = View.GONE
        }

        searchMenu = searchToolbar.menu
        itemSearch = searchMenu?.findItem(R.id.action_filter_search)

        itemSearch?.setOnActionExpandListener(object : MenuItem.OnActionExpandListener{
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                listener.invoke(true)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    circleReveal(searchToolbar, 1, true, false)
                } else
                    searchToolbar.visibility = View.GONE

                listener.invoke(false)
                return true
            }

        })

        initSearchView(searchMenu!!)
        //initSearchView()
    }


    private fun initSearchView(search_menu: Menu) {
        val searchView = search_menu.findItem(R.id.action_filter_search).actionView as SearchView

        searchView.isSubmitButtonEnabled = false

        //val closeButton = searchView.findViewById(R.id.search_close_btn) as ImageView
        //closeButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)


        // set hint and the text colors

        val txtSearch = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        txtSearch.hint = "Search.."
        txtSearch.setHintTextColor(Color.DKGRAY)
        txtSearch.setTextColor(ContextCompat.getColor(activity,R.color.colorPrimary))


        // set the cursor

        val searchTextView = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as AutoCompleteTextView
        try {
            val mCursorDrawableRes = TextView::class.java.getDeclaredField("mCursorDrawableRes")
            mCursorDrawableRes.isAccessible = true
            mCursorDrawableRes.set(searchTextView, R.drawable.search_cursor) //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (e: Exception) {
            e.printStackTrace()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                onQuerySearch(query)
                searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                onQuerySearch(newText)
                return true
            }

            fun onQuerySearch(query: String) {
                searchListener?.invoke(query)
                Log.i("query", "" + query)
            }

        })

    }

    @SuppressLint("PrivateResource")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun circleReveal(myView: View, posFromRight: Int, containsOverflow: Boolean, isShow: Boolean) {
        var width = myView.width

        if(posFromRight>0)
            width-=(posFromRight*activity.resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_material))-(activity.resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)/ 2)

        if(containsOverflow)
            width-=activity.resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material)

        val cx = width
        val cy = myView.height/2

        val anim = if(isShow) ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0f,width.toFloat())
                   else ViewAnimationUtils.createCircularReveal(myView, cx, cy, width.toFloat(), 0f)

        anim.duration = 220L

        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                if(!isShow) {
                    super.onAnimationEnd(animation)
                    myView.visibility = View.GONE
                }
            }
        })

        if(isShow)
            myView.visibility = View.VISIBLE

        anim.start()
    }
}