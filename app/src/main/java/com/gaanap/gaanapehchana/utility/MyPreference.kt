package com.gaanap.gaanapehchana.utility

import android.content.Context
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.Preference
import com.gaanap.gaanapehchana.models.response.Decades
import io.realm.Realm

object MyPreference {

    fun onUpdateDecades(context: Context, myDecades: List<Decades>) {
        val realm = Realm.getDefaultInstance()
        val results = realm.where(Preference::class.java).equalTo("isDifficulty", false).findAll()
        if (results.size <= 0) {
            val decadeIds    = context.resources.getIntArray(R.array.decades)
            val decadeLevels = context.resources.getStringArray(R.array.decades_name)
            for(i in 0 until decadeIds.size) {
                realm.beginTransaction()
                val decade = realm.createObject(Preference::class.java,decadeIds[i])
                decade.labelName    = decadeLevels[i]
                decade.isDifficulty = false
                decade.isSelected   = myDecades.any { it.id == decade.id }
                realm.commitTransaction()
            }

        } else {
            myDecades.forEach {
                val decade = realm.where(Preference::class.java).equalTo("id", it.id).findFirst()
                if (decade != null) {
                    realm.beginTransaction()
                    decade.isSelected = true
                    realm.commitTransaction()
                }
            }
        }

        realm.close()
    }


    fun onUpdateDifficulty(context: Context,myDifficulty: List<String>) {
        val realm = Realm.getDefaultInstance()
        val results = realm.where(Preference::class.java).equalTo("isDifficulty", true).findAll()
        if (results.size <= 0) {
            val diffLevels = context.resources.getStringArray(R.array.difficultLevels)
            for (i in 0 until diffLevels.size) {
                realm.beginTransaction()
                val diff = realm.createObject(Preference::class.java, i)
                diff.labelName = diffLevels[i]
                diff.isDifficulty = true
                diff.isSelected = myDifficulty.contains(diffLevels[i])
                realm.commitTransaction()
            }

        } else {
            myDifficulty.forEach {
                val difficulty = realm.where(Preference::class.java).equalTo("labelName", it).findFirst()
                if (difficulty != null) {
                    realm.beginTransaction()
                    difficulty.isSelected = true
                    realm.commitTransaction()
                }
            }
        }
        realm.close()
    }
}