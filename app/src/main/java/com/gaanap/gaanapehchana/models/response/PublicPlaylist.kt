package com.gaanap.gaanapehchana.models.response

import com.google.gson.annotations.SerializedName

class PublicPlaylist {

    @SerializedName("total_songs")
    var totalSongs: Long = 0

    @SerializedName("id")
    var id: String = ""

    @SerializedName("radio_name")
    var radioName: String = ""

    @SerializedName("radio_description")
    var radioDescription: String = ""

    @SerializedName("radio_type")
    var radioType: String = ""

    @SerializedName("added_by")
    var addedBy: String = ""

    @SerializedName("added_by_id")
    var addedById: String = ""

    @SerializedName("created_on")
    var createdOn: String = ""

    @SerializedName("recomended")
    var recomended: String = ""

    @SerializedName("privacy")
    var privacy: String = ""

    @SerializedName("url")
    var url: String = ""

    @SerializedName("sortOrder")
    var sortOrder: String = ""

    @SerializedName("img_path")
    var imgPath: String = ""

    @SerializedName("limit_song")
    var limitSong: String = ""

    @SerializedName("id_not_in")
    var idNotIn: String = ""

    @SerializedName("total_song")
    var totalSong: Long = 0

    @SerializedName("r_id")
    var rId: String = ""

    @SerializedName("liked_status")
    var likedStatus: String = ""

    @SerializedName("thumbnail")
    var thumbnail: String = ""
}