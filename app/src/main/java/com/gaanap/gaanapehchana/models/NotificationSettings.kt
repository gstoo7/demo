package com.gaanap.gaanapehchana.models

class NotificationSettings {
    var blog            = Blog()
    var tmk_game        = Tmk_game()
    var mlb_position    = Mlb_position()
    var system_messages = System_messages()

    inner class Blog(var email: Int = 0,var sms: Int = 0,var push: Int = 0)
    inner class Tmk_game(var email: Int = 0,var sms: Int = 0,var push: Int = 0)
    inner class Mlb_position(var email: Int = 0,var sms: Int = 0,var push: Int = 0)
    inner class System_messages(var email: Int = 0,var sms: Int = 0,var push: Int = 0)
}