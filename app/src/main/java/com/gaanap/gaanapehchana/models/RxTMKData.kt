package com.gaanap.gaanapehchana.models

import com.gaanap.gaanapehchana.enums.LoadingStatus
import com.gaanap.gaanapehchana.models.response.TMKGameResponse

class RxTMKData(val status: LoadingStatus) {

    lateinit var response: TMKGameResponse
    var progress = 0

    constructor(response: TMKGameResponse) : this(LoadingStatus.DATA) {
        this.response = response
    }

    constructor(progress: Int) : this(LoadingStatus.PROGRESS) {
        this.progress = progress
    }
}