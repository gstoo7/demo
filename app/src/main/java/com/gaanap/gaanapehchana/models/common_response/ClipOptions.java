package com.gaanap.gaanapehchana.models.common_response;

import com.google.gson.annotations.SerializedName;

public class ClipOptions {
    @SerializedName("id")
    public String id;

    @SerializedName("ClipOptionID")
    public String clipOptionID;

    @SerializedName("ClipID")
    public String clipID;

    @SerializedName("clip_id")
    public String clip_id;

    @SerializedName("ClipOptionDesc")
    public String clipOptionDesc;

    @SerializedName("ClipCorrectOption")
    public String clipCorrectOption;

    @SerializedName("ClipIndex")
    public String clipIndex;
}