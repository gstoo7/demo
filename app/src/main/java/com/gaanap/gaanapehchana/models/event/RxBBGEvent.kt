package com.gaanap.gaanapehchana.models.event

import com.gaanap.gaanapehchana.enums.LoadingStatus
import com.gaanap.gaanapehchana.models.response.BBGameResponse

class RxBBGEvent {
    lateinit var response: BBGameResponse
    var status   = LoadingStatus.SHOW_PROGRESS
    var progress = 0

    fun setLoadingStatus(status: LoadingStatus) : RxBBGEvent {
        this.status = status
        return this
    }

    fun setProgress(progress: Int): RxBBGEvent {
        this.progress = progress
        this.status   = LoadingStatus.PROGRESS
        return this
    }

    fun setData(response: BBGameResponse): RxBBGEvent {
        this.response = response
        this.status   = LoadingStatus.DATA
        return this
    }
}