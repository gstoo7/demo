package com.gaanap.gaanapehchana.models.response;

import com.gaanap.gaanapehchana.models.common_response.Clips;
import com.gaanap.gaanapehchana.models.common_response.Scoring;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BBGameResponse extends BaseResponse {

    @SerializedName("clips")
    public List<Clips> clips;

    @SerializedName("scoring")
    public Scoring scoring;

    @SerializedName("bbg_length")
    public int bbgLength;
}
