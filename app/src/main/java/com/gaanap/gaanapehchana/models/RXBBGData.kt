package com.gaanap.gaanapehchana.models

import com.gaanap.gaanapehchana.enums.LoadingStatus
import com.gaanap.gaanapehchana.models.response.BBGameResponse

class RxBBGData(val status: LoadingStatus) {

    lateinit var response: BBGameResponse
    var progress = 0

    constructor(response: BBGameResponse) : this(LoadingStatus.DATA) {
        this.response = response
    }

    constructor(progress: Int) : this(LoadingStatus.PROGRESS) {
        this.progress = progress
    }
}