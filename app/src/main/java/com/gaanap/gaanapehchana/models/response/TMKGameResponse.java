package com.gaanap.gaanapehchana.models.response;

import com.gaanap.gaanapehchana.models.common_response.ClipOptions;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TMKGameResponse {

    @SerializedName("lists")
    public GameList lists;

    public class GameList {

        @SerializedName("id")
        public String id;

        @SerializedName("FeaturedGameStatus")
        public String FeaturedGameStatus;

        @SerializedName("Theme")
        public String theme;

        @SerializedName("is_demo")
        public int is_demo;

        @SerializedName("bonus")
        public String bonus;

        @SerializedName("bonus_tmk2")
        public String bonus_tmk2;

        @SerializedName("bonus_tmk3")
        public String bonus_tmk3;

        @SerializedName("score")
        public Score score;

        @SerializedName("clips")
        public List<Clips> clips;
    }

    public class Score {
        @SerializedName("sa1")
        public String sa1;

        @SerializedName("sa2")
        public String sa2;

        @SerializedName("sa3")
        public String sa3;

        @SerializedName("re1")
        public String re1;

        @SerializedName("re2")
        public String re2;

        @SerializedName("re3")
        public String re3;

        @SerializedName("ga1")
        public String ga1;

        @SerializedName("ga2")
        public String ga2;

        @SerializedName("ga3")
        public String ga3;

        @SerializedName("ma1")
        public String ma1;

        @SerializedName("ma2")
        public String ma2;

        @SerializedName("ma3")
        public String ma3;

        @SerializedName("pa1")
        public String pa1;

        @SerializedName("pa2")
        public String pa2;

        @SerializedName("pa3")
        public String pa3;
    }

    public class Clips {

        @SerializedName("song_full_length")
        public String songFullLength;

        @SerializedName("song_title")
        public String songTitle;

        @SerializedName("song_movie")
        public String songMovie;

        @SerializedName("song_year")
        public String songYear;

        @SerializedName("song_singer")
        public String songSinger;

        @SerializedName("song_composer")
        public String songComposer;

        @SerializedName("song_lyricist")
        public String songLyricist;

        @SerializedName("song_comment")
        public String songComment;

        @SerializedName("thumbnail")
        public String thumbnail;

        @SerializedName("song_mp3_fle")
        public String songMp3Fle;

        @SerializedName("rating")
        public String rating;

        @SerializedName("ClipFileName")
        public String clipFileName;

        @SerializedName("ClipLevel")
        public String clipLevel;

        @SerializedName("id")
        public String id;

        @SerializedName("ClipTitle")
        public String clipTitle;

        @SerializedName("songname")
        public String songname;

        @SerializedName("songid")
        public String songid;

        @SerializedName("Cliplength")
        public String cliplength;

        @SerializedName("chance")
        public int chance;

        @SerializedName("options")
        public List<ClipOptions> options;
    }
}