package com.gaanap.gaanapehchana.models.common_response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Clips {

    @SerializedName("details")
    public Details details;

    @SerializedName("options")
    public List<ClipOptions> options;

    @SerializedName("songdetails")
    public SongDetails songDetails;


    public class Details {

        @SerializedName("id")
        public String id;

        @SerializedName("ClipID")
        public String clipID;

        @SerializedName("ClipLevel")
        public String clipLevel;

        @SerializedName("ClipFileName")
        public String clipFileName;

        @SerializedName("ClipFileCreateDate")
        public String clipFileCreateDate;

        @SerializedName("ClipLastModifyDate")
        public String clipLastModifyDate;

        @SerializedName("ClipTitle")
        public String clipTitle;

        @SerializedName("ClipKeywords")
        public String clipKeywords;

        @SerializedName("ClipSinger")
        public String clipSinger;

        @SerializedName("ClipLyricist")
        public String clipLyricist;

        @SerializedName("ClipComposer")
        public String clipComposer;

        @SerializedName("ClipMovieName")
        public String clipMovieName;

        @SerializedName("ClipYear")
        public String clipYear;

        @SerializedName("ClipComments")
        public String clipComments;

        @SerializedName("Clipstarttime")
        public String clipstarttime;

        @SerializedName("Cliplength")
        public String clipLength;

        @SerializedName("songname")
        public String songname;

        @SerializedName("theme")
        public String theme;

        @SerializedName("actor")
        public String actor;

        @SerializedName("comment2")
        public String comment2;

        @SerializedName("songid")
        public String songid;

        @SerializedName("createdby")
        public String createdby;

        @SerializedName("ClipPopularity")
        public String clipPopularity;

        @SerializedName("used")
        public String used;

    }

    public class SongDetails {
        @SerializedName("id")
        public String id;

        @SerializedName("song_id")
        public String songId;

        @SerializedName("song")
        public String song;

        @SerializedName("singer1")
        public String singer1;

        @SerializedName("singer2")
        public String singer2;

        @SerializedName("singer3")
        public String singer3;

        @SerializedName("singer4")
        public String singer4;

        @SerializedName("singer5")
        public String singer5;

        @SerializedName("composer1")
        public String composer1;

        @SerializedName("composer2")
        public String composer2;

        @SerializedName("lyricist")
        public String lyricist;

        @SerializedName("year")
        public String year;

        @SerializedName("movie_name")
        public String movieName;

        @SerializedName("genre1")
        public String genre1;

        @SerializedName("genre2")
        public String genre2;

        @SerializedName("genre3")
        public String genre3;

        @SerializedName("comments1")
        public String comments1;

        @SerializedName("comments2")
        public String comments2;

        @SerializedName("pre_merged_comments1")
        public String preMergedComments1;

        @SerializedName("merged_users_info")
        public String mergedUsersInfo;

        @SerializedName("comments1_message")
        public String comments1Message;

        @SerializedName("mp3_file")
        public String mp3_file;

        @SerializedName("addedon")
        public String addedon;

        @SerializedName("status")
        public String status;

        @SerializedName("rating")
        public String rating;

        @SerializedName("rating2")
        public String rating2;

        @SerializedName("actor")
        public String actor;

        @SerializedName("songduration")
        public String songduration;

        @SerializedName("lyrics")
        public String lyrics;

        @SerializedName("Popularity")
        public String popularity;

        @SerializedName("upload_using_itunes")
        public String uploadUsingItunes;

        @SerializedName("total_clips")
        public String totalClips;

        @SerializedName("song_full_length")
        public String songFullLength;

        @SerializedName("added_by")
        public String addedBy;

        @SerializedName("added_on")
        public String addedOn;

        @SerializedName("last_updated_by")
        public String lastUpdatedBy;

        @SerializedName("last_updated_on")
        public String lastUpdatedOn;

        @SerializedName("blog_song")
        public String blogSong;

        @SerializedName("ratingavg")
        public String ratingavg;

        @SerializedName("thumbnail")
        public String thumbnail;
    }
}