package com.gaanap.gaanapehchana.models.response

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class SongCategory : RealmObject() {

    @PrimaryKey
    var id          = ""
    var radio_name  = ""
    var total_songs = 0
    var playList: RealmList<PlayList> ? = null

    override fun equals(other: Any?): Boolean {
        if(other == null || other !is SongCategory)
            return false
        return id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


}