package com.gaanap.gaanapehchana.models.response

data class ErrorResponse(var code: Int = 0,var status_code: Int = 0,var message: String? = null)
