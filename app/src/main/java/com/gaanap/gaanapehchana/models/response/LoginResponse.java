package com.gaanap.gaanapehchana.models.response;

import com.gaanap.gaanapehchana.models.User;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends BaseResponse {

    @SerializedName("data")
    public Data data;

    public class Data {
        public String token;
        public User   user;
    }
}
