package com.gaanap.gaanapehchana.models.response

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class TMKGameCategory : RealmObject() {

    @PrimaryKey
    var id     = 0
    var status = ""

    @SerializedName("category_name")
    var categoryName = ""

    @SerializedName("category_slug")
    var categorySlug = ""
}