package com.gaanap.gaanapehchana.models.request

class LoginRequest(val email: String, val password: String)