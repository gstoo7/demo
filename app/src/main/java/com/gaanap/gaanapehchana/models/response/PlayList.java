package com.gaanap.gaanapehchana.models.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PlayList extends RealmObject implements Parcelable {

    public PlayList() {}

    @PrimaryKey
    @SerializedName("id")
    public String id;

    @SerializedName("song_id")
    public String songId;

    @SerializedName("song")
    public String song;

    @SerializedName("singer1")
    public String singer1;

    @SerializedName("singer2")
    public String singer2;

    @SerializedName("singer3")
    public String singer3;

    @SerializedName("singer4")
    public String singer4;

    @SerializedName("singer5")
    public String singer5;

    @SerializedName("composer1")
    public String composer1;

    @SerializedName("composer2")
    public String composer2;

    @SerializedName("lyricist")
    public String lyricist;

    @SerializedName("year")
    public String year;

    @SerializedName("movie_name")
    public String movieName;

    @SerializedName("genre1")
    public String genre1;

    @SerializedName("genre2")
    public String genre2;

    @SerializedName("genre3")
    public String genre3;

    @SerializedName("comments1")
    public String comments1;

    @SerializedName("comments2")
    public String comments2;

    @SerializedName("mp3_file")
    public String mp3File;

    @SerializedName("addedon")
    public String addedon;

    @SerializedName("status")
    public String status;

    @SerializedName("rating")
    public String rating;

    @SerializedName("rating2")
    public String rating2;

    @SerializedName("ratingavg")
    public String ratingAvg;

    @SerializedName("actor")
    public String actor;

    @SerializedName("songduration")
    public String songduration;

    @SerializedName("lyrics")
    public String lyrics;

    @SerializedName("Popularity")
    public String popularity;

    @SerializedName("upload_using_itunes")
    public String uploadUsingItunes;

    @SerializedName("total_clips")
    public String totalClips;

    @SerializedName("song_full_length")
    public String songFullLength;

    @SerializedName("added_by")
    public String addedBy;

    @SerializedName("added_on")
    public String addedOn;

    @SerializedName("last_updated_by")
    public String lastUpdatedBy;

    @SerializedName("last_updated_on")
    public String lastUpdatedOn;

    @SerializedName("blog_song")
    public String blogSong;

    @SerializedName("avgRating")
    public String avgRating;

    @SerializedName("myRating")
    public String myRating;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("inmyplaylist")
    public boolean inMyPlaylist;

    protected PlayList(Parcel in) {
        id                = in.readString();
        songId            = in.readString();
        song              = in.readString();
        singer1           = in.readString();
        singer2           = in.readString();
        singer3           = in.readString();
        singer4           = in.readString();
        singer5           = in.readString();
        composer1         = in.readString();
        composer2         = in.readString();
        lyricist          = in.readString();
        year              = in.readString();
        movieName         = in.readString();
        genre1            = in.readString();
        genre2            = in.readString();
        genre3            = in.readString();
        comments1         = in.readString();
        comments2         = in.readString();
        mp3File           = in.readString();
        addedon           = in.readString();
        status            = in.readString();
        rating            = in.readString();
        rating2           = in.readString();
        ratingAvg         = in.readString();
        actor             = in.readString();
        songduration      = in.readString();
        lyrics            = in.readString();
        popularity        = in.readString();
        uploadUsingItunes = in.readString();
        totalClips        = in.readString();
        songFullLength    = in.readString();
        addedBy           = in.readString();
        addedOn           = in.readString();
        lastUpdatedBy     = in.readString();
        lastUpdatedOn     = in.readString();
        blogSong          = in.readString();
        avgRating         = in.readString();
        myRating          = in.readString();
        thumbnail         = in.readString();
        inMyPlaylist      = in.readByte() != 0;
    }

    public static final Creator<PlayList> CREATOR = new Creator<PlayList>() {
        @Override
        public PlayList createFromParcel(Parcel in) {
            return new PlayList(in);
        }

        @Override
        public PlayList[] newArray(int size) {
            return new PlayList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(songId);
        dest.writeString(song);
        dest.writeString(singer1);
        dest.writeString(singer2);
        dest.writeString(singer3);
        dest.writeString(singer4);
        dest.writeString(singer5);
        dest.writeString(composer1);
        dest.writeString(composer2);
        dest.writeString(lyricist);
        dest.writeString(year);
        dest.writeString(movieName);
        dest.writeString(genre1);
        dest.writeString(genre2);
        dest.writeString(genre3);
        dest.writeString(comments1);
        dest.writeString(comments2);
        dest.writeString(mp3File);
        dest.writeString(addedon);
        dest.writeString(status);
        dest.writeString(rating);
        dest.writeString(rating2);
        dest.writeString(ratingAvg);
        dest.writeString(actor);
        dest.writeString(songduration);
        dest.writeString(lyrics);
        dest.writeString(popularity);
        dest.writeString(uploadUsingItunes);
        dest.writeString(totalClips);
        dest.writeString(songFullLength);
        dest.writeString(addedBy);
        dest.writeString(addedOn);
        dest.writeString(lastUpdatedBy);
        dest.writeString(lastUpdatedOn);
        dest.writeString(blogSong);
        dest.writeString(avgRating);
        dest.writeString(myRating);
        dest.writeString(thumbnail);
        dest.writeByte((byte) (inMyPlaylist ? 1 : 0));
    }
}