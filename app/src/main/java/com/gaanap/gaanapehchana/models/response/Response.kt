package com.gaanap.gaanapehchana.models.response

import com.gaanap.gaanapehchana.models.User

data class UpdateProfileResponse(var user: User)

data class RadioSearchResponse(val error: Int,val data: ArrayList<PlayList>)

// GP Channel model classes
data class ChannelsResponse(val pagination: Pagination,val data: List<Channel>)
data class Pagination(val last_page: Int)
data class Channel(val id: String,val radio_name: String,val total_songs: Int,val songs: List<PlayList>?)


// Game Score model
data class ScoreResponse(val error: Boolean,val score: Score,val count: GameCount,var message: String = "")
data class Score(val sevendays: Long,val monthly: Long,val lifetime: Long)
data class GameCount(val sevendays: Long,val monthly: Long,val lifetime: Long)


// Force update model
data class ForceUpdateResponse(val data: ForceUpdate,val decades: List<Decades>,val categories: List<TMKGameCategory>)
data class ForceUpdate(val id: Int = 0, val os: String = "android",val version: String,val forceupdate: Int = 0)
data class Decades(val id: Int = 0,val label_name: String)


// Mobile number verification mode
data class NumberVerifyResponse(val user_id: String,val mobile: String,val country_code: String,val OTP: String)
data class VerifyOTPResponse(val verified: String,val message: String)