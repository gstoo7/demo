package com.gaanap.gaanapehchana.models.response;

import com.gaanap.gaanapehchana.models.common_response.ClipOptions;
import com.gaanap.gaanapehchana.models.common_response.Clips;
import com.gaanap.gaanapehchana.models.common_response.Scoring;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ASNGameResponse extends BaseResponse {

    @SerializedName("length")
    public float length;

    @SerializedName("interval")
    public int interval;

    @SerializedName("clips")
    public List<Clips> clips;

    @SerializedName("is_demo")
    public Integer isDemo;

    @SerializedName("scoring")
    public Scoring scoring;

}
