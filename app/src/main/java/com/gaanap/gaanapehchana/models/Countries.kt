package com.gaanap.gaanapehchana.models

data class Countries(val countries: List<Country>)

data class Country(val country_code: String,val country_name: String,val dialling_code: String)