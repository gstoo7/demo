package com.gaanap.gaanapehchana.models

import android.os.Parcel
import android.os.Parcelable
import com.gaanap.gaanapehchana.models.response.PlayList

data class RightOptionDetails(val playList: PlayList,val score: Int, val level: String,
                              val isRightOption: Boolean, val isBonus: Boolean) : Parcelable {

    constructor(parcel: Parcel) : this(parcel.readParcelable<PlayList>(PlayList::class.java.classLoader),
            parcel.readInt(), parcel.readString(),parcel.readByte() != 0.toByte(),parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(playList,flags)
        parcel.writeInt(score)
        parcel.writeString(level)
        parcel.writeByte(if (isRightOption) 1 else 0)
        parcel.writeByte(if (isBonus) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RightOptionDetails> {
        override fun createFromParcel(parcel: Parcel): RightOptionDetails {
            return RightOptionDetails(parcel)
        }

        override fun newArray(size: Int): Array<RightOptionDetails?> {
            return arrayOfNulls(size)
        }
    }
}