package com.gaanap.gaanapehchana.models.event

import com.gaanap.gaanapehchana.enums.LoadingStatus
import com.gaanap.gaanapehchana.models.response.ASNGameResponse

class RxASNEvent {
    lateinit var response: ASNGameResponse
    var status   = LoadingStatus.SHOW_PROGRESS
    var progress = 0

    fun setLoadingStatus(status: LoadingStatus) : RxASNEvent {
        this.status = status
        return this
    }

    fun setProgress(progress: Int): RxASNEvent {
        this.progress = progress
        this.status   = LoadingStatus.PROGRESS
        return this
    }

    fun setData(response: ASNGameResponse): RxASNEvent {
        this.response = response
        this.status   = LoadingStatus.DATA
        return this
    }
}