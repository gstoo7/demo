package com.gaanap.gaanapehchana.models.response

import com.google.gson.annotations.SerializedName

data class TMKListResponse(val error:Int, @SerializedName("paination") val pagination: PaginationInfo?, var games: List<TMKGame>?)

class TMKGame {
    var gameid    : String  = ""
    var title     : String  = ""
    var theme     : String  = ""
    var my_score  : String? = ""
    var thumbnail : String? = ""
}

data class PaginationInfo(@SerializedName("last_page_no") var lastPageNo: Int = 0)