package com.gaanap.gaanapehchana.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Preference : RealmObject() {

    @PrimaryKey
    var id: Int = 0

    @SerializedName("label_name")
    var labelName    = ""
    var isSelected   = false
    var isDifficulty = false
}