package com.gaanap.gaanapehchana.models.event

class ProgressUpdate(var progress:Int = 0) {

    fun setProgress(progress: Int) : ProgressUpdate {
        this.progress = progress
        return this
    }
}