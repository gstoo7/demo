package com.gaanap.gaanapehchana.models.response

data class AboutUsResponse(var id: String?,var title: String?,var description: String?)