package com.gaanap.gaanapehchana.models.common_response;

import com.google.gson.annotations.SerializedName;

public class Scoring {

    @SerializedName("sa")
    public String sa;

    @SerializedName("re")
    public String re;

    @SerializedName("ga")
    public String ga;

    @SerializedName("ma")
    public String ma;

    @SerializedName("pa")
    public String pa;

    @SerializedName("bonus")
    public int bonus;

}