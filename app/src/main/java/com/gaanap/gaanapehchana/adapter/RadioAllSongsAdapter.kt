package com.gaanap.gaanapehchana.adapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.response.Channel
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.utility.ITEM
import com.gaanap.gaanapehchana.utility.LOADING
import com.gaanap.gaanapehchana.utility.inflate
import kotlinx.android.synthetic.main.item_all_songs_adapter.view.*

class RadioAllSongsAdapter(private val channelList: ArrayList<Channel>, private val moreClick: (channel: Channel) -> Unit,
                           private val songClick: (Int,Channel,ArrayList<PlayList>) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoadingAdded = false


    override fun getItemCount()         = channelList.size
    private  fun getItem(position: Int) = channelList.getOrNull(position)

    override fun getItemViewType(position: Int): Int {
        return if (position == channelList.size - 1 && isLoadingAdded) LOADING else ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            ITEM -> ContentVH(parent.inflate(R.layout.item_all_songs_adapter))
            else -> LoadingVH(parent.inflate(R.layout.item_loader_adapter))
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? ContentVH)?.bindView(channelList[position])
    }

    fun add(channel: Channel) {
        channelList.add(channel)
        notifyItemChanged(channelList.size - 1)
    }

    fun addAll(channels: List<Channel>) {
        channels.forEach(this::add)
    }

    fun updateItem(playListId: String?,songId: String?) {
        if(playListId.isNullOrBlank() || songId.isNullOrBlank())
            return
        try {
            val index = channelList.indexOfFirst { it.id == playListId }
            if(index == -1) return
            channelList[index].songs?.find { it.id == songId} ?.inMyPlaylist = true
            notifyItemChanged(index)
        } catch (e: Exception) {}

    }

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(Channel("","",0, arrayListOf()))
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = channelList.size - 1
        val item = getItem(position)

        if (item != null && item.id == "") {
            channelList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    private fun remove(channel: Channel?) {
        val position = channelList.indexOf(channel ?: return)
        if (position > -1) {
            channelList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        isLoadingAdded = false
        while (itemCount > 0) {
            remove(getItem(0))
        }
    }

    fun onDestroy() {

    }

    inner class ContentVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val adapter = PlayListAdapter(arrayListOf(),this::onSongsClickListener)

        init {
            val layoutManager                = LinearLayoutManager(itemView.context)
            layoutManager.orientation        = LinearLayoutManager.HORIZONTAL
            itemView.songsList.layoutManager = layoutManager
            itemView.songsList.adapter       = adapter

            itemView.more.setOnClickListener { moreClick.invoke(channelList[adapterPosition]) }
        }

        fun bindView(channel: Channel) {
            itemView.title.text  = channel.radio_name
            adapter.playListName = channel.radio_name
            adapter.addAll(channel.songs ?: return)
        }

        private fun onSongsClickListener(position: Int,list: ArrayList<PlayList>) {
            val channel = getItem(adapterPosition) ?: return
            songClick.invoke(position,channel,list)
        }
    }

    class LoadingVH(view: View) : RecyclerView.ViewHolder(view)
}