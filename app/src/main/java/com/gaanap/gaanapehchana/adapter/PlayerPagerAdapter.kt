package com.gaanap.gaanapehchana.adapter

import android.os.Parcelable
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.ViewGroup
import com.gaanap.gaanapehchana.fragments.mymusic.PlayerViewFragment
import com.gaanap.gaanapehchana.models.response.PlayList
import android.os.Bundle

class PlayerPagerAdapter(fm: FragmentManager,private val list: ArrayList<PlayList>) : FragmentStatePagerAdapter(fm) {

    var currentFragment: PlayerViewFragment? = null

    override fun getItem(position: Int) = PlayerViewFragment.newInstance(list[position])
    override fun getCount()             = list.size
    fun getSong(position: Int)          = list.getOrNull(position)

    fun addAll(songsList: List<PlayList>) {
        list.clear()
        list.addAll(songsList)
        notifyDataSetChanged()
    }

    fun updateData(songsList: List<PlayList>) {
        list.addAll(songsList)
        notifyDataSetChanged()
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        if (currentFragment != `object`)
            currentFragment = `object` as PlayerViewFragment
        super.setPrimaryItem(container, position, `object`)
    }

    override fun saveState(): Parcelable? {
        val bundle = super.saveState() as Bundle?
        bundle?.putParcelableArray("states", null) // Never maintain any states from the base class, just null it out
        return bundle
    }
}