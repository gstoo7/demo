package com.gaanap.gaanapehchana.adapter

import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.View
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.response.PublicPlaylist
import com.gaanap.gaanapehchana.utility.inflate
import kotlinx.android.synthetic.main.item_my_play_list_adapter.view.*

class MyPlayListAdapter(private val playLists: ArrayList<PublicPlaylist>,
                        private val listener: (PublicPlaylist) -> Unit,
                        private val deleteListener: ((String) -> Unit)?) : RecyclerView.Adapter<MyPlayListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_my_play_list_adapter))
    override fun getItemCount() = playLists.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = playLists[position]
        val totalSongs = "Total songs: ${item.totalSongs}"
        holder.itemView.playListName.text = item.radioName
        holder.itemView.totalSongs.text   = totalSongs

        holder.itemView.playListType.setColorFilter(ContextCompat.getColor(holder.itemView.context, if(item.privacy == "1") R.color.redOrange else R.color.green),PorterDuff.Mode.SRC_IN)

        holder.itemView.setOnClickListener { listener.invoke(item) }
        if(deleteListener == null)
            holder.itemView.moreIcon.visibility = View.GONE
        else
            holder.itemView.moreIcon.setOnClickListener { deleteListener.invoke(item.id) }
    }

    fun addAll(list: List<PublicPlaylist>) {
        playLists.clear()
        playLists.addAll(list)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}