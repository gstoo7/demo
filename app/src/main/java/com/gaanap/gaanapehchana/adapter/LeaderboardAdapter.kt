package com.gaanap.gaanapehchana.adapter

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.firebasehelper.model.LeaderboardModel
import com.gaanap.gaanapehchana.utility.inflate
import com.gaanap.gaanapehchana.utility.loadUrl
import kotlinx.android.synthetic.main.item_leaderboard_adapter.view.*

class LeaderboardAdapter(private val userId: String,private val list: ArrayList<LeaderboardModel>,
                         private val listener: (LeaderboardModel) -> Unit) : RecyclerView.Adapter<LeaderboardAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_leaderboard_adapter))

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(list[position])
    }

    fun getAll() = list
    fun addAll(dataList : ArrayList<LeaderboardModel>) = dataList.forEach(this::add)

    fun add(model: LeaderboardModel) {
        list.add(model)
        notifyItemInserted(list.size-1)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(leaderboardModel: LeaderboardModel) {
            val gameInfo        = leaderboardModel.gameInfo
            itemView.rank.text  = gameInfo?.rank?.toString() ?: "0"
            itemView.name.text  = gameInfo?.name ?: ""
            itemView.score.text = gameInfo?.totalScore?.toString() ?: "0"
            itemView.profileIcon.loadUrl(gameInfo?.profileIcon)

            itemView.setOnClickListener { listener.invoke(list[adapterPosition]) }

            if(userId == leaderboardModel.userId) {
                itemView.setBackgroundColor(Color.BLACK)
                itemView.rank  .setTextColor(Color.WHITE)
                itemView.name  .setTextColor(Color.WHITE)
                itemView.score .setTextColor(Color.WHITE)
            } else {
                itemView.setBackgroundColor(Color.TRANSPARENT)
                itemView.rank  .setTextColor(Color.BLACK)
                itemView.name  .setTextColor(Color.BLACK)
                itemView.score .setTextColor(Color.BLACK)
            }
        }
    }
}