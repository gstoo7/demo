package com.gaanap.gaanapehchana.adapter

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.View
import android.widget.TextView
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.response.TMKGameCategory
import com.gaanap.gaanapehchana.utility.inflate

class TMKCategoriesAdapter(private val categories: List<TMKGameCategory>,private val listener:(String) -> Unit) : RecyclerView.Adapter<TMKCategoriesAdapter.ViewHolder> (){

    private var selectedPosition = -1
    override fun getItemCount()  = categories.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_tmk_category_adapter))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(holder.itemView as? TextView != null) {
            val category = categories[position]
            holder.itemView.text = category.categoryName
            if(selectedPosition == position) {
                holder.itemView.setTextColor(Color.WHITE)
                holder.itemView.setBackgroundResource(R.drawable.rect_gradient_radius_20dp_tmk)
            } else {
                holder.itemView.setTextColor(Color.BLACK)
                holder.itemView.setBackgroundResource(R.drawable.rect_transparent_with_black_boder_20dp)
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                if(selectedPosition == adapterPosition) {
                    selectedPosition = -1
                    listener.invoke("")
                } else {
                    selectedPosition = adapterPosition
                    listener.invoke(categories[selectedPosition].categorySlug)
                }
                notifyDataSetChanged()
            }
        }
    }
}