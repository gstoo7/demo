package com.gaanap.gaanapehchana.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.response.PublicPlaylist
import com.gaanap.gaanapehchana.utility.inflate
import com.gaanap.gaanapehchana.utility.loadRoundedImage
import kotlinx.android.synthetic.main.item_public_play_list_adapter.view.*

class PublicPlayListAdapter(private val playLists: ArrayList<PublicPlaylist>
                            , private val listener: (PublicPlaylist) -> Unit) : RecyclerView.Adapter<PublicPlayListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_public_play_list_adapter))
    override fun getItemCount() = playLists.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(playLists[position])
    }

    fun addAll(list: List<PublicPlaylist>) {
        playLists.clear()
        playLists.addAll(list)
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { listener.invoke(playLists[adapterPosition]) }
        }

        fun bindView(item: PublicPlaylist) {
            val totalSongs = "Total songs:${item.totalSongs}"
            itemView.thumbIcon.loadRoundedImage(item.thumbnail)
            itemView.listTitle.text  = item.radioName
            itemView.totalSongs.text = totalSongs
        }

    }
}