package com.gaanap.gaanapehchana.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.gaanap.gaanapehchana.models.Country

class CountriesAdapter(context: Context,countries: List<Country>) : ArrayAdapter<Country>(context,android.R.layout.simple_dropdown_item_1line,countries) {

    override fun isEnabled(position: Int) = position != 0

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view  = super.getView(position, convertView, parent) as TextView
        view.text = getItem(position).country_name
        return view
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view  = super.getView(position, convertView, parent) as TextView
        view.text = getItem(position).country_name
        return view
    }
}