package com.gaanap.gaanapehchana.adapter

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.common_response.ClipOptions
import com.gaanap.gaanapehchana.utility.inflate
import kotlinx.android.synthetic.main.item_game_options.view.*

class ClipOptionAdapter(private val options:MutableList<ClipOptions>
                        , private val listener: (Boolean, ClipOptions) -> Unit) : RecyclerView.Adapter<ClipOptionAdapter.ViewHolder>() {

    private val correctAnswer = "c"
    private var isClickEnable = true

    fun updateGameList(list: List<ClipOptions>) {
        options.clear()
        options.addAll(list)
        isClickEnable = true
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_game_options))
    override fun getItemCount() = options.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(options[position])
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val delayTime:Long = 500
        private val handler = Handler()

        fun bindView(option: ClipOptions) {
            itemView.answer.text = option.clipOptionDesc
            itemView.answer.setBackgroundResource(R.drawable.ic_game_option_bg)
            itemView.setOnClickListener {
                if(!isClickEnable)
                    return@setOnClickListener

                isClickEnable = false

                if(option.clipCorrectOption.equals(correctAnswer,true)) {
                    itemView.answer.setBackgroundResource(R.drawable.ic_right_ans_bg)
                    sendResultBack(true,option)
                } else {
                    itemView.answer.setBackgroundResource(R.drawable.ic_wrong_ans_bg)
                    sendResultBack(false,option)
                }
            }
        }

        private fun sendResultBack(result: Boolean,answer: ClipOptions) = handler.postDelayed({listener.invoke(result,answer)},delayTime)
    }
}