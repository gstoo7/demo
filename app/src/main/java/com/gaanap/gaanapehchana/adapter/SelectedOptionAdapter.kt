package com.gaanap.gaanapehchana.adapter

import android.graphics.Color
import android.graphics.PorterDuff
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.PlayerState
import com.gaanap.gaanapehchana.fragments.dialogs.AddToPlayListDialog
import com.gaanap.gaanapehchana.fragments.dialogs.SongInfoDialog
import com.gaanap.gaanapehchana.interfaces.PlayerListener
import com.gaanap.gaanapehchana.models.RightOptionDetails
import com.gaanap.gaanapehchana.models.SongsProgress
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.item_selected_option_adapter.view.*

class SelectedOptionAdapter(private val context      : FragmentActivity,
                            private val optionList   : List<RightOptionDetails>,
                            private val songListener : PlayerListener,
                            private val gameColor: Int) : RecyclerView.Adapter<SelectedOptionAdapter.ViewHolder>() {

    private var currentPosition = -1
    private var progress        = SongsProgress(0,100)
    private var playerState     = PlayerState.INIT
    private var lastPlayerState = PlayerState.INIT

    private var playListSongs   = hashSetOf<String>()

    override fun getItemCount() = optionList.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_selected_option_adapter))
    override fun onBindViewHolder(holder: ViewHolder, position: Int)  = holder.bindView(position)

    fun setProgress(progress: SongsProgress) {
        this.progress = progress
        notifyItemChanged(currentPosition)
    }

    fun setPlayerState(state: PlayerState) {
        playerState = state

        if(lastPlayerState == playerState)
            return
        lastPlayerState = playerState
        notifyItemChanged(currentPosition)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(position: Int) {
            val option  = optionList[position]
            val details = optionList[position].playList
            itemView.clipScore.text  = option.score.toString()
            itemView.songName.text   = details.song
            itemView.songSinger.text = details.singer1
            itemView.songMovie.text  = details.movieName
            val clipLevel            = "Level ${option.level}"
            itemView.songLevel.text  = clipLevel
            itemView.songLevel.setTextColor(Color.parseColor(levelColors[option.level.toLowerCase()]))
            itemView.songThumbnail.loadUrl(details.thumbnail)
            itemView.scoreInfo.setColorFilter(gameColor,PorterDuff.Mode.SRC_IN)

            if(option.isRightOption) {
                itemView.scoreIcon.visibility = View.VISIBLE
                itemView.scoreIcon.setImageResource(if(option.isBonus) R.drawable.ic_jewels else R.drawable.ic_star_24dp)
                itemView.fabIsCorrect.setImageResource(R.drawable.ic_check_24dp)
                DrawableCompat.setTint(itemView.fabIsCorrect.drawable,ContextCompat.getColor(context, R.color.green))
            } else {
                itemView.scoreIcon.visibility = View.INVISIBLE
                itemView.fabIsCorrect.setImageResource(R.drawable.ic_close_black_24dp)
                DrawableCompat.setTint(itemView.fabIsCorrect.drawable,ContextCompat.getColor(context, R.color.redOrange))
            }


            if(playListSongs.contains(details.id))
                itemView.fabFavorite.setColorFilter(Color.RED,PorterDuff.Mode.SRC_IN)
            else
                itemView.fabFavorite.setColorFilter(Color.parseColor("#B2B2B2"),PorterDuff.Mode.SRC_IN)

            itemView.fabFavorite.setOnClickListener {
                val songId = details.id
                when {
                    songId.isNullOrBlank()       -> context.showToast(R.string.something_wrong_error)
                    !isNetworkAvailable(context) -> context.showToast(R.string.connection_error)
                    else -> {
                        AddToPlayListDialog.newInstance(songId!!) { _, _ ->
                            playListSongs.add(songId)
                            itemView.fabFavorite.setColorFilter(Color.RED,PorterDuff.Mode.SRC_IN)
                        }.show(context.supportFragmentManager, DIALOG)
                    }
                }
            }

            itemView.scoreInfo.setOnClickListener {
                SongInfoDialog.newInstance(details).show(context.supportFragmentManager, DIALOG)
            }

            itemView.pauseIcon.setOnClickListener {
                songListener.onSongsPause()
            }

            itemView.playIcon.setOnClickListener {
                if(currentPosition == adapterPosition) {
                    songListener.onSongsPause()
                } else {
                    if(currentPosition != -1)
                        songListener.onSongsStop()
                    currentPosition = adapterPosition
                    songListener.onSongsPlay(details.mp3File!!)
                    playerState = PlayerState.PREPARING
                    notifyDataSetChanged()
                }
            }

            if(currentPosition == position) {
                when (playerState) {

                    PlayerState.PREPARING -> {
                        if(!itemView.progressbar.isIndeterminate)
                            itemView.progressbar.isIndeterminate = true
                        itemView.playIcon.visibility     = View.GONE
                        itemView.progressView.visibility = View.VISIBLE
                        itemView.songThumbnail.setPadding(dp2px(5),dp2px(5),dp2px(5),dp2px(5))
                    }

                    PlayerState.START -> {
                        if(itemView.progressbar.isIndeterminate)
                            itemView.progressbar.isIndeterminate = false

                        if(itemView.progressView.visibility != View.VISIBLE)
                            itemView.progressView.visibility = View.VISIBLE

                        if(itemView.playIcon.visibility == View.VISIBLE)
                            itemView.playIcon.visibility = View.GONE

                        if(progress.duration > 0)
                            itemView.progressbar.progress = ((progress.progress * 100)/progress.duration).toFloat()
                        itemView.songThumbnail.setPadding(dp2px(5),dp2px(5),dp2px(5),dp2px(5))
                    }

                    else -> {
                        if(itemView.progressView.visibility == View.VISIBLE)
                            itemView.progressView.visibility = View.GONE

                        if(itemView.playIcon.visibility != View.VISIBLE)
                            itemView.playIcon.visibility = View.VISIBLE
                        itemView.songThumbnail.setPadding(0,0,0,0)

                    }
                }
            } else {
                itemView.playIcon     .visibility = View.VISIBLE
                itemView.progressView .visibility = View.GONE
                itemView.progressbar  .progress   = 0F
                itemView.songThumbnail.setPadding(0,0,0,0)

            }
        }
    }
}