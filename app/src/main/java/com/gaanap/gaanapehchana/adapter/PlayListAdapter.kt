package com.gaanap.gaanapehchana.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.utility.getDeviceWidth
import com.gaanap.gaanapehchana.utility.inflate
import com.gaanap.gaanapehchana.utility.loadRoundedImage
import kotlinx.android.synthetic.main.item_play_list_adapter.view.*

class PlayListAdapter(private val playLists: ArrayList<PlayList>,
                      private val songClick: (Int,ArrayList<PlayList>) -> Unit) :RecyclerView.Adapter<PlayListAdapter.ViewHolder>() {

    var playListName: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_play_list_adapter))
    override fun getItemCount() = playLists.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(playLists[position])
    }

    fun addAll(list: List<PlayList>) {
        playLists.clear()
        playLists.addAll(list)
        notifyDataSetChanged()
    }

    fun clear() {
        playLists.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            val thumbSize = (getDeviceWidth() / 3)
            itemView.layoutParams.width = thumbSize
            itemView.thumbIcon.setOnClickListener { songClick.invoke(adapterPosition,playLists) }
        }

        fun bindView(item: PlayList) {
            itemView.thumbIcon.loadRoundedImage(item.thumbnail)
            itemView.songName.text = item.song
        }

    }
}