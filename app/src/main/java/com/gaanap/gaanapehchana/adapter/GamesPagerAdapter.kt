package com.gaanap.gaanapehchana.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.GlideApp
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.utility.inflate
import kotlinx.android.synthetic.main.item_games_pager_adapter.view.*

class GamesPagerAdapter(val context: Context,val listener: (Int) -> Unit) : PagerAdapter() {

    private val iconArray : IntArray      = intArrayOf(R.drawable.home_tmk_icon,R.drawable.home_bbg_icon,R.drawable.home_aursunao_icon)
    private val gameNames : Array<String> = context.resources.getStringArray(R.array.games_name)

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = container.inflate(R.layout.item_games_pager_adapter)
        view.gameName.text = gameNames[position]
        //view.thumbIcon.setImageResource(iconArray[position])
        GlideApp.with(context).load(iconArray[position]).centerCrop().into(view.thumbIcon)
        view.thumbIcon.setOnClickListener { listener.invoke(position) }
        container.addView(view)
        return view
    }

    override fun getCount() = gameNames.size
    override fun isViewFromObject(view: View, obj: Any) = view === obj
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}