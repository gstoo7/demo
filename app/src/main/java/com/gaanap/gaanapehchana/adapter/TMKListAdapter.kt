package com.gaanap.gaanapehchana.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.response.TMKGame
import com.gaanap.gaanapehchana.utility.ITEM
import com.gaanap.gaanapehchana.utility.LOADING
import com.gaanap.gaanapehchana.utility.inflate
import com.gaanap.gaanapehchana.utility.loadUrl
import kotlinx.android.synthetic.main.item_tmk_game.view.*

class TMKListAdapter(private val games: MutableList<TMKGame>,private val listener: (TMKGame) -> Unit)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(),Filterable {

    private var filterGames = mutableListOf<TMKGame>()

    init {
        filterGames.addAll(games)
    }

    override fun getFilter()    = FilterAdapter()
    override fun getItemCount() = filterGames.size
    override fun getItemViewType(position: Int) = if(filterGames[position].gameid == "-1") LOADING else ITEM
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : RecyclerView.ViewHolder {
        return when(viewType) {
            LOADING -> LoadingVH(parent.inflate(R.layout.item_loader_adapter))
            else    -> ContentVH(parent.inflate(R.layout.item_tmk_game))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder as? ContentVH != null) holder.bindView(filterGames[position])
    }

    fun addAll(updatedGames: List<TMKGame>) {
        games.addAll(updatedGames)
        filterGames.addAll(updatedGames)
        notifyDataSetChanged()
    }

    fun updateGameScore(gameId: String,score: Int) {
        val game = filterGames.firstOrNull{ it.gameid == gameId }
        game?.my_score = score.toString()
        notifyDataSetChanged()
    }

    fun clear() {
        games.clear()
        filterGames.clear()
        notifyDataSetChanged()
    }

    fun addLoadingFooter() {
        filterGames.add(TMKGame().apply { gameid = "-1" })
        notifyItemInserted(filterGames.size -1)
    }

    fun removeLoadingFooter() {
        if (filterGames.size <= 0)
            return
        val position = filterGames.size - 1
        if (filterGames[position].gameid == "-1") {
            filterGames.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    inner class ContentVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(game:TMKGame) {
            itemView.title.text   = game.theme
            itemView.categoryIcon.loadUrl(game.thumbnail)
            if(game.my_score.isNullOrBlank() || game.my_score == "--")
                itemView.myScore.visibility = View.INVISIBLE
            else {
                val score = "My Score: ${game.my_score}"
                itemView.myScore.visibility = View.VISIBLE
                itemView.myScore.text = score
            }

            itemView.setOnClickListener { listener.invoke(game) }
        }
    }

    inner class LoadingVH(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class FilterAdapter : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filterString = constraint.toString().toLowerCase()
            val filterList   = games.filter { it.theme.toLowerCase().contains(filterString) }
            return FilterResults().apply {
                values = filterList
                count  = filterList.size
            }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            @Suppress("UNCHECKED_CAST")
            filterGames = results?.values as? MutableList<TMKGame> ?: return
            notifyDataSetChanged()
        }
    }
}