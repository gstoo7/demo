package com.gaanap.gaanapehchana.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.fragments.dialogs.AddToPlayListDialog
import com.gaanap.gaanapehchana.models.event.PlayListEvent
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.item_play_list_songs_adapter.view.*

class PlayListSongsAdapter(private val activity  : FragmentActivity?,
                           private val playLists : ArrayList<PlayList>,
                           private val listener  : (position: Int,list: ArrayList<PlayList>) -> Unit,
                           private val moreClick : (PlayList,position: Int) -> Unit) : RecyclerView.Adapter<PlayListSongsAdapter.ViewHolder>() {

    private var addedSongs   = hashSetOf<String>()

    override fun getItemCount() = playLists.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_play_list_songs_adapter))
    override fun onBindViewHolder(holder: ViewHolder, position: Int)  = holder.bindView(playLists[position])

    fun addAll(list: List<PlayList>) {
        playLists.clear()
        playLists.addAll(list)
        notifyDataSetChanged()
    }

    fun updateItem(songId: String?) {
        if(songId.isNullOrBlank()) return
        try {
            val index = playLists.indexOfFirst { it.id == songId }
            if(index == -1) return
            playLists[index].inMyPlaylist = true
            notifyItemChanged(index)
        } catch (e: Exception) {}

    }

    fun removeSong(position: Int) {
        playLists.removeAt(position)
        notifyItemRemoved(position)
    }

    fun clear() {
        playLists.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener { listener.invoke(adapterPosition,playLists) }
        }

        fun bindView(item: PlayList) {
            itemView.thumbIcon.loadRoundedImage(item.thumbnail)
            itemView.name.text   = item.song
            itemView.movie.text  = item.movieName
            itemView.singer.text = item.singer1

            if(item.inMyPlaylist)
                itemView.favouriteIcon.setImageResource(R.drawable.ic_favorite_black_24dp)
            else {
                if (addedSongs.contains(item.id))
                    itemView.favouriteIcon.setImageResource(R.drawable.ic_favorite_black_24dp)
                else
                    itemView.favouriteIcon.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            }

            itemView.favouriteIcon.setOnClickListener {
                val songId = item.id
                when {
                    songId.isNullOrBlank() -> activity?.showToast(R.string.something_wrong_error)
                    !isNetworkAvailable(activity ?: return@setOnClickListener) -> activity.showToast(R.string.connection_error)
                    else -> {
                        AddToPlayListDialog.newInstance(songId) { _, playlistId ->
                            RxBus.publish(PlayListEvent(playlistId,songId))
                        }.show(activity.supportFragmentManager, DIALOG)
                    }
                }
            }
            itemView.moreInfoIcon.setOnClickListener { moreClick.invoke(item,adapterPosition) }
        }
    }
}