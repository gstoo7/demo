package com.gaanap.gaanapehchana.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView

import com.gaanap.gaanapehchana.R
import kotlinx.android.synthetic.main.fragment_profile.*
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.gaanap.gaanapehchana.ConstantsFlavor.ABOUT_US_URL
import com.gaanap.gaanapehchana.ConstantsFlavor.BLOG_URL
import com.gaanap.gaanapehchana.GlideApp
import com.gaanap.gaanapehchana.adapter.CountriesAdapter
import com.gaanap.gaanapehchana.fragments.dialogs.OtpDialog
import com.gaanap.gaanapehchana.helper.ImagePicker
import com.gaanap.gaanapehchana.interfaces.NumberVerifyView
import com.gaanap.gaanapehchana.interfaces.ProfileView
import com.gaanap.gaanapehchana.models.Country
import com.gaanap.gaanapehchana.models.User
import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.models.response.ErrorResponse
import com.gaanap.gaanapehchana.models.response.NumberVerifyResponse
import com.gaanap.gaanapehchana.models.response.UpdateProfileResponse
import com.gaanap.gaanapehchana.presenter.LogoutPresenter
import com.gaanap.gaanapehchana.presenter.NumberVerifyPresenter
import com.gaanap.gaanapehchana.presenter.ProfilePresenter
import com.gaanap.gaanapehchana.utility.*
import com.gaanap.gaanapehchana.view.LoginActivity
import io.reactivex.disposables.CompositeDisposable
import java.lang.Exception

class ProfileFragment : BaseFragment(),ProfileView, NumberVerifyView {

    private lateinit var profilePresenter : ProfilePresenter

    private var currentUser : User? = null
    private var currentView : View? = null

    private val logoutPresenter = LogoutPresenter(this)
    private val disposable      = CompositeDisposable()
    private var selectedCountry = Country("","Select your country","")
    private var isVerified      = false

    private var dialog:Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance   = true
        currentUser      = SessionManager.getUser(context)
        profilePresenter = ProfilePresenter(context,this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if(currentView == null) inflater.inflate(R.layout.fragment_profile,container,false) else currentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        EventUtils.logTimeEvent(MOST_TIME_SPENT_IN_TAB)
        if(currentView == null) {
            currentView = view
            setCountryAdapter()
            setListener()
            setData()
        }
    }

    override fun showProgress() {
        if(dialog == null)
            dialog = ProgressDialog.show(context ?: return)
        dialog?.show()
    }

    override fun hideProgress() {
        if(dialog?.isShowing == true)
            dialog?.dismiss()
    }

    override fun onError(error: ErrorResponse?) {
        if(error?.status_code == 500 && !error.message.isNullOrBlank())
            activity?.showToast("Old Password is incorrect")
        else
            activity?.showToast(R.string.something_wrong_error)
    }

    override fun onResponse() {
        clear()
    }

    override fun onResponse(response: UpdateProfileResponse) {
        currentUser = response.user
        clear()
    }

    override fun onResponse(response: BaseResponse) {
        if(response.error == 0) {
            EventUtils.logClickEvent(LOGOUT_COUNT)
            logoutFromApp(activity?: return)
            val intent = Intent()
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity?.startIntentActivity<LoginActivity>(intent)
        }
    }

    override fun onResponse(response: NumberVerifyResponse) {

    }

    private fun setCountryAdapter() {
        val countries = getAllCountries(activity)
        val list = arrayListOf<Country>()
        list.add(Country("","Select your country",""))
        list.addAll(countries)
        countrySpinner.adapter = CountriesAdapter(context?: return,list)
        if(!currentUser?.country.isNullOrBlank()) {
            val pos = list.indexOfFirst { it.country_name ==  currentUser!!.country }
            countrySpinner.setSelection(pos)
        }
    }

    @SuppressLint("HardwareIds")
    private fun setListener() {
        backIcon     .setOnClickListener { onGoBack() }
        infoView     .setOnClickListener { onInfoClick() }
        passwordView .setOnClickListener { onPasswordClick() }
        aboutUsIcon  .setOnClickListener { loadFragment(WebViewFragment.newInstance(ABOUT_US_URL ,getString(R.string.about_us)) ,ABOUT_US_FRAGMENT)   }
        blogIcon     .setOnClickListener { loadFragment(WebViewFragment.newInstance(BLOG_URL    ,getString(R.string.blogs))    ,"WebViewFragment")   }
        settingIcon  .setOnClickListener { loadFragment(SettingsFragment(),SETTINGS_FRAGMENT) }
        countryCode  .addTextChangedListener(textWatcher)
        edtMobileNo  .addTextChangedListener(textWatcher)

        editIcon.setOnClickListener {
            if(PermissionUtils.checkCameraPermission(context ?: return@setOnClickListener))
                ImagePicker.pickImage(this, "Select your image:")
            else
                requestPermissions(arrayOf(Manifest.permission.CAMERA),CAMERA_PC)
        }

        verifyNumber.setOnClickListener {
            val mobileNo = edtMobileNo.text.toString()
            if(mobileNo.isBlank() || mobileNo.length != 10) {
                activity?.showToast(R.string.pls_enter_valid_number)
                return@setOnClickListener
            }

            if(!isNetworkAvailable(activity ?: return@setOnClickListener)) {
                activity?.showToast(R.string.connection_error)
                return@setOnClickListener
            }

            disposable.add(NumberVerifyPresenter(object : NumberVerifyView {
                override fun onResponse(response: NumberVerifyResponse) {
                    hideProgress()
                    OtpDialog.newInstance(response.country_code,response.mobile,response.OTP,this@ProfileFragment::onNumberVerified).show(activity?.fragmentManager, DIALOG)
                }

                override fun showProgress() {
                    verifyNumber?.visibility = View.GONE
                    progressBar?.visibility  = View.VISIBLE
                }

                override fun hideProgress() {
                    progressBar?.visibility  = View.GONE
                    verifyNumber?.visibility = View.VISIBLE
                }

                override fun onError(error: ErrorResponse?) {
                    hideProgress()
                    activity?.showToast(error?.message ?: getString(R.string.pls_enter_valid_email))
                }

                override fun onNetworkError() {
                    activity?.showToast(R.string.connection_error)
                }

            }).verifyNumber(currentUser?.user_id ?: return@setOnClickListener,mobileNo,selectedCountry.dialling_code))

        }

        logout.setOnClickListener {
            DialogUtils.dialogWithYesNo(activity ?: return@setOnClickListener,getString(R.string.logout_dialog_msg)) { dialog, _ ->
                dialog.dismiss()
                if(!isNetworkAvailable(activity ?: return@dialogWithYesNo)) {
                    activity?.showToast(R.string.connection_error)
                    return@dialogWithYesNo
                }

                val deviceId = Settings.Secure.getString(activity?.contentResolver ?: return@dialogWithYesNo, Settings.Secure.ANDROID_ID)
                logoutPresenter.logout(currentUser?.user_id ?: return@dialogWithYesNo,deviceId)
            }
        }

        rootContainer.setOnTouchListener { touchView, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> hideKeyboard(activity ?: return@setOnTouchListener false)
                MotionEvent.ACTION_UP -> touchView.performClick()
            }
            false
        }

        countrySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedCountry   = parent?.getItemAtPosition(position) as Country
                countryCode?.text = selectedCountry.dialling_code
            }

        }
    }

    private fun setData() {
        edtName     .setText(currentUser?.fullName?: "")
        edtMobileNo .setText(currentUser?.mobileNumber?: "")
        profileIcon .loadUrl(currentUser?.pic ?: "")


        if("M".equals(currentUser?.source,true)) {
            oldPassword.visibility    = View.VISIBLE
            edtOldPassword.visibility = View.VISIBLE
        }
    }
    private fun closeAll() {
        if(innerInfoView.visibility == View.VISIBLE) {
            innerInfoView.visibility = View.GONE
            infoEdit.text = getString(R.string.edit)
        }

        if(innerPasswordView.visibility == View.VISIBLE) {
            innerPasswordView.visibility = View.GONE
            passwordEdit.text = getString(R.string.edit)
        }

    }

    private fun onInfoClick() {
        if(innerInfoView.visibility == View.VISIBLE) {
            if(updateInfo()) {
                innerInfoView.visibility = View.GONE
                infoEdit.text = getString(R.string.edit)
            }

        } else {
            closeAll()
            innerInfoView.visibility = View.VISIBLE
            infoEdit.text = getString(R.string.done)
        }
    }

    private fun onPasswordClick() {
        if(innerPasswordView.visibility == View.VISIBLE) {
            if(updatePassword()) {
                innerPasswordView.visibility = View.GONE
                passwordEdit.text = getString(R.string.edit)
            }

        } else {
            closeAll()
            innerPasswordView.visibility = View.VISIBLE
            passwordEdit.text = getString(R.string.done)
        }
    }

    private fun updateInfo() : Boolean {
        val fullName = edtName.text.toString()
        val mobile   = edtMobileNo.text.toString()
        if(fullName.isBlank()) {
            edtName.requestFocus()
            edtName.error = getString(R.string.pls_enter_your_name)
            return false
        }

        if(mobile.isNotBlank() && !isVerified) {
            activity?.showToast(R.string.Pls_verify_number)
            return false
        }

        if(!isNetworkAvailable(activity ?: return false)) {
            activity?.showToast(R.string.connection_error)
            return false
        }

        disposable.add(profilePresenter.updateProfileInfo(currentUser,fullName,mobile,selectedCountry.country_name))

        return true
    }

    private fun updatePassword() : Boolean {
        val newPassword    = edtNewPassword.text.toString()
        val retypePassword = edtRetypePassword.text.toString()
        val oldPassword    = edtOldPassword.text.toString()

        if (newPassword.isBlank() && retypePassword.isBlank() && oldPassword.isBlank()) {
            return true
        } else if(newPassword.isBlank()) {
            edtNewPassword.requestFocus()
            edtNewPassword.error = getString(R.string.enter_new_password)
            return false
        } else if(newPassword.length < 6) {
            edtNewPassword.requestFocus()
            edtNewPassword.error = getString(R.string.password_minimum_error)
            return false
        } else if(retypePassword.isBlank()) {
            edtRetypePassword.requestFocus()
            edtRetypePassword.error = getString(R.string.enter_retype_password)
            return false
        } else if(newPassword != retypePassword) {
            edtRetypePassword.requestFocus()
            edtRetypePassword.error = getString(R.string.retype_not_match)
            return false
        } else {
            if(edtOldPassword.visibility == View.VISIBLE) {
                if(oldPassword.isBlank()) {
                    edtOldPassword.requestFocus()
                    edtOldPassword.error = getString(R.string.enter_old_password)
                    return false
                }
            }
        }

        if(!isNetworkAvailable(activity ?: return false)) {
            activity?.showToast(R.string.connection_error)
            return false
        }

        disposable.add(profilePresenter.updatePassword(currentUser?.user_id?:return false,oldPassword,newPassword))
        return true
    }

    private fun clear() {
        edtNewPassword?.setText("")
        edtRetypePassword?.setText("")
        edtOldPassword?.setText("")
    }

    private fun onNumberVerified() {
        verifyNumber.visibility = View.GONE
        isVerified = true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            val filePath = ImagePicker.getImageFromResult(activity
                    ?: return, requestCode, resultCode, data)
            if (!filePath.isNullOrBlank()) {
                GlideApp.with(this).load(filePath)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(profileIcon)

                if (!isNetworkAvailable(activity ?: return)) {
                    activity?.showToast(R.string.connection_error)
                    return
                }

                disposable.add(profilePresenter.changeProfilePic(currentUser?.user_id
                        ?: return, filePath!!))
            }
        } catch (e: Exception){
            Log.e("Error",":$e")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            CAMERA_PC -> {
                if(PermissionUtils.verifyPermissions(grantResults))
                    ImagePicker.pickImage(this, "Select your image:")
                else
                    DialogUtils.openPermissionDenyDialog(context,getString(R.string.camera_permission_msg))
            }
        }


    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            when {
                countryCode.text.isNullOrBlank() || edtMobileNo.text.isNullOrBlank() -> verifyNumber.visibility = View.GONE
                edtMobileNo.text.toString() == currentUser?.mobileNumber && selectedCountry.country_name == currentUser?.country -> {
                    verifyNumber.visibility = View.GONE
                    isVerified = true
                }
                else -> {
                    isVerified = false
                    verifyNumber.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventUtils.logGroupClickEvent(MOST_TIME_SPENT_IN_TAB, TAB, "Profile")
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog = null
        disposable.clear()
    }
}
