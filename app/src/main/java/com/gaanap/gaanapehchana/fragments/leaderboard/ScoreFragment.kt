package com.gaanap.gaanapehchana.fragments.leaderboard

import android.animation.ValueAnimator
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.LeaderboardAdapter
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.enums.ScoreType
import com.gaanap.gaanapehchana.firebasehelper.model.GameInfo
import com.gaanap.gaanapehchana.firebasehelper.model.LeaderboardModel
import com.gaanap.gaanapehchana.fragments.BaseFragment
import com.gaanap.gaanapehchana.fragments.dialogs.GLeaderboardDialog
import com.gaanap.gaanapehchana.fragments.dialogs.MLeaderboardDialog
import com.gaanap.gaanapehchana.helper.DividerItemDecoration
import com.gaanap.gaanapehchana.utility.*
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_score.*
import kotlinx.android.synthetic.main.view_leaderboard_header.*
import org.joda.time.DateTime
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.DatabaseReference

class ScoreFragment : BaseFragment() {

    private var gameType  = GameType.NONE
    private var scoreType = ScoreType.WEEKLY
    private val handler   = Handler()

    private lateinit var adapter: LeaderboardAdapter

    private var query: Query? = null
    private var valueEventListener : ValueEventListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter   = LeaderboardAdapter(getUser()?.user_id ?: "",arrayListOf(),this::onItemClick)
        gameType  = arguments?.getSerializable(GAME)  as GameType
        scoreType = arguments?.getSerializable(SCORE) as ScoreType
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_score, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle()
        hideView()
        setupRecyclerView()
        setQuery()
        fetchScores()
    }

    private fun setQuery() {
        progressBar.visibility = View.VISIBLE
        query = when(gameType) {
            GameType.TMK -> {
                when(scoreType) {
                    ScoreType.WEEKLY    -> makeQueryForWeekly(FirebaseNodeRef.getWeeklyTMKRef())
                    ScoreType.MONTHLY   -> makeQueryForMonthly(FirebaseNodeRef.getMonthlyTMKRef())
                    ScoreType.LIFE_TIME -> makeQuery(FirebaseNodeRef.getLifetimeTMKRef())
                }
            }

            GameType.BBG -> {
                when(scoreType) {
                    ScoreType.WEEKLY    -> makeQueryForWeekly(FirebaseNodeRef.getWeeklyBBGRef())
                    ScoreType.MONTHLY   -> makeQueryForMonthly(FirebaseNodeRef.getMonthlyBBGRef())
                    ScoreType.LIFE_TIME -> makeQuery(FirebaseNodeRef.getLifetimeBBGRef())
                }
            }

            GameType.ASN -> {
                when(scoreType) {
                    ScoreType.WEEKLY    -> makeQueryForWeekly(FirebaseNodeRef.getWeeklyASNRef())
                    ScoreType.MONTHLY   -> makeQueryForMonthly(FirebaseNodeRef.getMonthlyASNRef())
                    ScoreType.LIFE_TIME -> makeQuery(FirebaseNodeRef.getLifetimeASNRef())
                }
            }

            else -> {
                when(scoreType) {
                    ScoreType.WEEKLY    -> makeQueryForWeekly(FirebaseNodeRef.getWeeklyMasterRef())
                    ScoreType.MONTHLY   -> makeQueryForMonthly(FirebaseNodeRef.getMonthlyMasterRef())
                    ScoreType.LIFE_TIME -> makeQuery(FirebaseNodeRef.getLifetimeMasterRef())
                }
            }
        }
    }

    private fun makeQueryForWeekly(databaseRef: DatabaseReference) : Query {
        return databaseRef.orderByChild("timestamp")
                .startAt(DateTime().minusDays(7).millis.toString())
    }

    private fun makeQueryForMonthly(databaseRef: DatabaseReference) : Query {
        return databaseRef.orderByChild("timestamp")
                .startAt(DateTime().minusDays(30).millis.toString())
    }

    private fun makeQuery(databaseRef: DatabaseReference) : Query {
        return databaseRef.orderByChild("totalScore")
    }

    private fun fetchScores() {
        query?.keepSynced(true)
        valueEventListener = object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                progressBar?.visibility = View.GONE
                activity?.showToast(R.string.something_wrong_error)
            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                try {
                    if (dataSnapshot != null && dataSnapshot.childrenCount > 0) {
                        val children = dataSnapshot.children.sortedByDescending {
                            it.child("totalScore")?.value as? Long ?: 0
                        }
                        val list = arrayListOf<LeaderboardModel>()
                        val user = getUser()
                        val myModel = LeaderboardModel()
                        myModel.userId = user?.user_id ?: return
                        myModel.gameInfo?.name = user.fullName
                        myModel.gameInfo?.profileIcon = user.pic
                        list.add(myModel)
                        for (i in 0 until children.count()) {
                            val model = LeaderboardModel()
                            val userId = children.elementAt(i).key
                            model.userId = userId
                            model.gameInfo = children.elementAt(i).getValue(GameInfo::class.java) ?: GameInfo()
                            model.gameInfo?.rank = (i + 1).toLong()
                            if (userId == getUser()?.user_id) {
                                list[0] = model
                                if (i > 10) list.add(model)
                            } else
                                list.add(model)
                        }
                        takeTop3User(list)
                        if (list.size > 3) {
                            val newList = arrayListOf<LeaderboardModel>()
                            val drop = list.take(4).sortedByDescending { it.gameInfo?.totalScore }.drop(3)
                            newList.addAll(drop)
                            newList.addAll(list.takeLast(list.size - 4))
                            adapter.addAll(newList)
                        }
                    }
                } catch (e: Exception) {
                   activity?.showToast(R.string.something_wrong_error)
                }

                progressBar?.visibility = View.GONE
            }

        }

        query?.addListenerForSingleValueEvent(valueEventListener)

        if(!isNetworkAvailable(context ?: return)) {
            handler.postDelayed({
                if(adapter.itemCount <= 0)
                    connectionError?.visibility = View.VISIBLE
            },5000)
        }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        scoreBoard.layoutManager = layoutManager
        scoreBoard.adapter       = adapter
        scoreBoard.addItemDecoration(DividerItemDecoration(activity))
    }

    private fun setTitle() {
        whichLeaderbord.text = when(gameType) {
            GameType.TMK -> getString(R.string.tmk_title)
            GameType.BBG -> getString(R.string.bbg_title)
            GameType.ASN -> getString(R.string.aursunao_title)
            else -> getString(R.string.master)
        }
    }

    private fun takeTop3User(list: List<LeaderboardModel>) {
        val sublist = list.take(4).sortedByDescending { it.gameInfo?.totalScore }

        when {
            sublist.isEmpty() -> {}
            sublist.size == 1 -> {
                rankView1.visibility = View.VISIBLE
                val rank1 = sublist[0]
                nameRank1  .text = rank1.gameInfo?.name ?: ""
                scoreRank1 .text = rank1.gameInfo?.totalScore?.toString() ?: ""
                picRank1.loadUrl(rank1.gameInfo?.profileIcon)
                rankView1.setOnClickListener { onItemClick(sublist[0]) }
                if(rank1.userId == getUser()?.user_id)
                    startZoomInAnimation(picRank1)
            }

            sublist.size == 2 -> {
                rankView2.visibility = View.VISIBLE
                rankView3.visibility = View.VISIBLE

                val rank1 = sublist[0]
                val rank2 = sublist[1]

                nameRank2  .text = rank1.gameInfo?.name ?: ""
                rankTxt2   .text = "1"
                scoreRank2 .text = rank1.gameInfo?.totalScore?.toString() ?: ""
                picRank2.loadUrl(rank1.gameInfo?.profileIcon)

                nameRank3  .text = rank2.gameInfo?.name ?: ""
                rankTxt3   .text = "2"
                scoreRank3 .text = rank2.gameInfo?.totalScore?.toString() ?: ""
                picRank3.loadUrl(rank2.gameInfo?.profileIcon)

                rankView2.setOnClickListener { onItemClick(sublist[0]) }
                rankView3.setOnClickListener { onItemClick(sublist[1]) }

                when {
                    rank1.userId == getUser()?.user_id -> startZoomInAnimation(picRank2)
                    rank2.userId == getUser()?.user_id -> startZoomInAnimation(picRank3)
                }
            }

            else -> {
                val rank1 = sublist.getOrNull(0)
                val rank2 = sublist.getOrNull(1)
                val rank3 = sublist.getOrNull(2)
                if(rank1 != null) {
                    rankView1.visibility = View.VISIBLE
                    nameRank1  .text     = rank1.gameInfo?.name ?: ""
                    scoreRank1 .text     = rank1.gameInfo?.totalScore?.toString() ?: ""
                    picRank1.loadUrl(rank1.gameInfo?.profileIcon)
                    rankView1.setOnClickListener { onItemClick(sublist[0]) }
                }

                if(rank2 != null) {
                    rankView2.visibility = View.VISIBLE
                    nameRank2  .text     = rank2.gameInfo?.name ?: ""
                    rankTxt2   .text     = "2"
                    scoreRank2 .text     = rank2.gameInfo?.totalScore?.toString() ?: ""
                    picRank2.loadUrl(rank2.gameInfo?.profileIcon)
                    rankView2.setOnClickListener { onItemClick(sublist[1]) }
                }

                if(rank3 != null) {
                    rankView3.visibility = View.VISIBLE
                    nameRank3  .text     = rank3.gameInfo?.name ?: ""
                    rankTxt3   .text     = "3"
                    scoreRank3 .text     = rank3.gameInfo?.totalScore?.toString() ?: ""
                    picRank3.loadUrl(rank3.gameInfo?.profileIcon)
                    rankView3.setOnClickListener { onItemClick(sublist[2]) }
                }

                when {
                    rank1?.userId == getUser()?.user_id -> startZoomInAnimation(picRank1)
                    rank2?.userId == getUser()?.user_id -> startZoomInAnimation(picRank2)
                    rank3?.userId == getUser()?.user_id -> startZoomInAnimation(picRank3)
                }
            }
        }
    }

    private fun onItemClick(leaderboard: LeaderboardModel) {
        if(gameType == GameType.NONE)
            MLeaderboardDialog.newInstance(leaderboard,scoreType).show(activity?.supportFragmentManager, DIALOG)
        else
            GLeaderboardDialog.newInstance(leaderboard,gameType,scoreType).show(activity?.supportFragmentManager, DIALOG)
    }

    private fun hideView() {
        rankView1.visibility = View.GONE
        rankView2.visibility = View.GONE
        rankView3.visibility = View.GONE
    }

    private fun startZoomInAnimation(view: View) {
        val scaleAnimation = ScaleAnimation(1f,0.9f,1f,0.9f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        scaleAnimation.duration    = 500
        scaleAnimation.startOffset = 100
        scaleAnimation.repeatMode  = ValueAnimator.REVERSE
        scaleAnimation.repeatCount = ValueAnimator.INFINITE
        view.startAnimation(scaleAnimation)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if(valueEventListener != null)
            query?.ref?.removeEventListener(valueEventListener)
    }

    companion object {
        @JvmStatic
        fun newInstance(gameType: GameType,scoreType: ScoreType) = ScoreFragment().apply {
            arguments = Bundle().apply {
                putSerializable(GAME,gameType)
                putSerializable(SCORE,scoreType)
            }
        }
    }
}