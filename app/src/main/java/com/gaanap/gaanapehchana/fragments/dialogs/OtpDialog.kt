package com.gaanap.gaanapehchana.fragments.dialogs

import android.content.DialogInterface
import android.os.Bundle
import android.os.CountDownTimer
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.interfaces.NumberVerifyView
import com.gaanap.gaanapehchana.models.response.NumberVerifyResponse
import com.gaanap.gaanapehchana.models.response.VerifyOTPResponse
import com.gaanap.gaanapehchana.presenter.NumberVerifyPresenter
import com.gaanap.gaanapehchana.utility.isNetworkAvailable
import com.gaanap.gaanapehchana.utility.showToast

import java.util.Locale
import java.util.concurrent.TimeUnit

import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_number_verfication_dialog.*

class OtpDialog : BaseDialogFragment(),NumberVerifyView {
    private var countryCode  = ""
    private var mobileNumber = ""
    private var sendOTP      = ""
    private var counter : Counter? = null
    private val presenter  = NumberVerifyPresenter(this)
    private val disposable = CompositeDisposable()

    private val userId by lazy {
        SessionManager.getUser(activity)?.user_id ?: ""
    }

    private var listener : (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
        arguments?.let {
            countryCode  = it.getString(COUNTRY_CODE)
            mobileNumber = it.getString(MOBILE_NUMBER)
            sendOTP      = it.getString(OTP)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_number_verfication_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        setResource()
        startCounter()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        //        SmsReceiver.bindListener(messageText -> {
        //            opt.setFocusable(true);
        //            opt.setCode(messageText);
        //        });
    }

    override fun onResponse(response: VerifyOTPResponse) {
        activity?.showToast(response.message)
        if(response.verified == "1") {
            listener?.invoke()
            dismiss()
        } else {
            otp.clear()
            confirm.isEnabled = true
            confirm.alpha     = 1f
        }
    }

    override fun onNetworkError() {
        activity?.showToast(R.string.connection_error)
        confirm.isEnabled = true
        confirm.alpha     = 1f
    }

    override fun onResponse(response: NumberVerifyResponse) {
        sendOTP = response.OTP
    }

    override fun showProgress() {}
    override fun hideProgress() {}

    private fun setListener() {
        confirm   .setOnClickListener { verifyPhoneNumberWithCode() }
        resendOTP .setOnClickListener { resendVerificationCode() }
        fabClose  .setOnClickListener { dismiss() }
    }

    private fun setResource() {
        val numberMsg = getString(R.string.sent_to) + " " + countryCode + mobileNumber
        number.text         = numberMsg
        resendOTP.isEnabled = false
        resendOTP.alpha     = 0.2f
        otp.setInputType(InputType.TYPE_CLASS_NUMBER)
    }

    private fun verifyPhoneNumberWithCode() {
        val characters = otp?.code
        val newChars   = characters?.filterNotNull()
        if(newChars == null || newChars.isEmpty()) {
            activity?.showToast("Please Enter 4 Digit OTP Number.")
            return
        }

        val code = StringBuilder()
        for (c in newChars)
            code.append(c)

        if (code.length != 4) {
            activity?.showToast("Please Enter 4 Digit OTP Number.")
            return
        }

        if(!isNetworkAvailable(activity)) {
            activity?.showToast(R.string.connection_error)
            return
        }

        confirm.isEnabled = false
        confirm.alpha     = 0.5f
        disposable.clear()
        disposable.add(presenter.verifyOTP(userId,mobileNumber,code.toString(),sendOTP))
    }


    private fun resendVerificationCode() {
        if(!isNetworkAvailable(activity)) {
            activity?.showToast(R.string.connection_error)
            return
        }

        startCounter()
        disposable.clear()
        disposable.add(presenter.verifyNumber(userId,mobileNumber,countryCode))
    }

    private fun startCounter() {
        counter = Counter()
        counter?.start()
    }

    private fun stopCounter() {
        counter?.cancel()
        counter = null
    }

    private inner class Counter : CountDownTimer(60000, 1000) {

        init {
            resendOTP.isEnabled = false
            resendOTP.alpha = 0.2f
        }

        override fun onTick(millisUntilFinished: Long) {
            optTimer.text = String.format(Locale.getDefault(), "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished))
        }

        override fun onFinish() {
            optTimer.setText(R.string._00)
            resendOTP.isEnabled = true
            resendOTP.alpha = 1f
        }
    }

    override fun onPause() {
        super.onPause()
        //SmsReceiver.bindListener(null);
    }

    override fun onDismiss(dialog: DialogInterface) {
        stopCounter()
        disposable.clear()
        super.onDismiss(dialog)
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }

    companion object {

        private const val COUNTRY_CODE  = "country_code"
        private const val MOBILE_NUMBER = "mobile_number"
        private const val OTP           = "otp"


        fun newInstance(country_code: String, mobile_number: String,otp: String,listener : (() -> Unit)?) = OtpDialog().apply {
            arguments = Bundle().apply {
                putString(COUNTRY_CODE, country_code)
                putString(MOBILE_NUMBER, mobile_number)
                putString(OTP, otp)
            }

            this.listener = listener
        }
    }
}
