package com.gaanap.gaanapehchana.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.interfaces.CommonView
import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.presenter.ContactUsPresenter
import com.gaanap.gaanapehchana.utility.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_contact_us.*

class ContactUsFragment : BaseFragment(),CommonView{

    private val presenter  = ContactUsPresenter(this)
    private val disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_contact_us,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userName  .setText(getUser()?.fullName ?: "")
        userEmail .setText(getUser()?.email ?: "")

        backIcon.setOnClickListener { onGoBack() }
        submit.setOnClickListener { submitData() }

        rootContainer.setOnTouchListener { touchView, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> hideKeyboard(activity ?: return@setOnTouchListener false)
                MotionEvent.ACTION_UP -> touchView.performClick()
            }
            false
        }
    }

    private fun submitData() {
        val name  = userName.text.toString()
        val email = userEmail.text.toString()
        val sub   = subject.text.toString()
        val msg   = message.text.toString()

        if (name.isBlank()) {
            userName.error = getString(R.string.pls_enter_your_name)
            return
        } else if (!ValidationUtils.isValidEmail(email)) {
            userEmail.error = getString(R.string.pls_enter_valid_email)
            return
        } else if (sub.isBlank()) {
            subject.error = getString(R.string.pls_enter_your_subject)
            return
        } else if (msg.isBlank()) {
            message.error = getString(R.string.pls_enter_your_msg)
            return
        } else if (!isNetworkAvailable(activity?:return)) {
            activity?.showToast(R.string.connection_error)
            return
        } else
            disposable.add(presenter.submitComments(name,email,sub,msg))
    }

    override fun onResponse(response: BaseResponse) {
        activity?.showToast(response.message?:"", Toast.LENGTH_LONG)
        subject.setText("")
        message.setText("")
    }

    override fun onError(throwable: Throwable) {
        val error = ErrorUtils.parseErrorResponse(throwable)
        activity?.showToast(error?.message ?: getString(R.string.something_wrong_error),Toast.LENGTH_LONG)
    }

    override fun showProgress() {
        submit      .visibility = View.GONE
        progressBar .visibility = View.VISIBLE
    }

    override fun hideProgress() {
        submit      .visibility = View.VISIBLE
        progressBar .visibility = View.GONE
    }

}