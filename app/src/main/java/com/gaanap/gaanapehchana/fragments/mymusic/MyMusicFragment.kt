package com.gaanap.gaanapehchana.fragments.mymusic

import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.fragments.BaseFragment
import com.gaanap.gaanapehchana.fragments.RadioSearchFragment
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_radio.*

class MyMusicFragment : BaseFragment() {

    private var currentFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_radio,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        EventUtils.logTimeEvent(MOST_TIME_SPENT_IN_TAB)
        setListener()
        if(currentFragment != null) {
            loadChildFragment(currentFragment!!, currentFragment?.tag ?: "")
            when(currentFragment?.tag) {
                "All Songs"      -> addElevation(allSongs)
                "MyPlayList"     -> addElevation(myPlaylist)
                "PublicPlayList" -> addElevation(publicPlaylist)
            }
        } else {
            loadChildFragment(AllSongsFragment.newInstance(), "All Songs")
            addElevation(allSongs)
            EventUtils.logGroupClickEvent(MOST_SELECTED,MY_MUSIC,"All Songs")
        }

        backIcon.visibility = View.INVISIBLE
    }

    override fun onResume() {
        super.onResume()
        loadPlayerFragment()
        radioTitle.text = getString(R.string.my_music)
    }

    private fun setListener() {
        searchIcon.setOnClickListener {
            EventUtils.logClickEvent(MY_MUSIC_SEARCH)
            loadFragment(RadioSearchFragment(),RADIO_SEARCH_FRAGMENT)
        }

        allSongs.setOnClickListener {
            resetElevation()
            val fragment = childFragmentManager.findFragmentByTag("All Songs")
            when {
                fragment == null || !fragment.isVisible -> loadChildFragment(AllSongsFragment.newInstance(),"All Songs")
                else-> {}
            }

            EventUtils.logGroupClickEvent(MOST_SELECTED,MY_MUSIC,"All Songs")
            addElevation(it)
        }

        myPlaylist.setOnClickListener {
            resetElevation()
            loadChildFragment(MyPlayListFragment(), "MyPlayList")
            EventUtils.logGroupClickEvent(MOST_SELECTED,MY_MUSIC,"My playlist")
            addElevation(it)
        }

        publicPlaylist.setOnClickListener {
            resetElevation()
            loadChildFragment(PublicPlayList.newInstance(),"PublicPlayList")
            EventUtils.logGroupClickEvent(MOST_SELECTED,MY_MUSIC,"Public Playlist")
            addElevation(it)
        }
    }

    fun loadChildFragment(fragment: Fragment, tag: String) {
        currentFragment = fragment
        val transaction = childFragmentManager.beginTransaction().replace(R.id.container,fragment,tag)
        transaction.addToBackStack(null)
        transaction.commitAllowingStateLoss()
    }

    override fun loadFragment(fragment: Fragment, tag: String, addToBackStack: Boolean) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.container,fragment,tag)
        if(addToBackStack)
            transaction?.addToBackStack(null)
        transaction?.commitAllowingStateLoss()
    }


    private fun loadPlayerFragment() {
        if(MusicPlayer.mService?.currentAudio != null) {
            val fragment = childFragmentManager.findFragmentByTag("SmallPlayer")
            if(fragment == null || !fragment.isVisible) {
                val transaction = childFragmentManager.beginTransaction().replace(R.id.playerContainer, SmallPlayerFragment.newInstance(), "SmallPlayer")
                transaction.commitAllowingStateLoss()
            }
        }
    }

    private fun resetElevation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            allSongs.elevation       = 0f
            myPlaylist.elevation     = 0f
            publicPlaylist.elevation = 0f
        }
    }

    private fun addElevation(view: View) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.elevation = dp2px(20).toFloat()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventUtils.logGroupClickEvent(MOST_TIME_SPENT_IN_TAB, TAB, "MyMusic")
    }
}
