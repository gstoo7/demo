package com.gaanap.gaanapehchana.fragments

import android.support.v4.app.Fragment
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.User
import com.gaanap.gaanapehchana.utility.replaceFragment
import com.gaanap.gaanapehchana.view.HomeActivity
import io.reactivex.disposables.CompositeDisposable

open class  BaseFragment : Fragment() {

    protected val baseDisposable = CompositeDisposable()

    private var user: User? = null

    protected fun getUser() : User? {
        if(user == null)
            user = SessionManager.getUser(activity)
        return user
    }

    protected open fun loadFragment(fragment: Fragment, tag: String, addToBackStack: Boolean = true) {
        if(activity is HomeActivity)
            (activity as HomeActivity).replaceFragment(R.id.container, fragment, tag, addToBackStack)
    }

    protected fun onGoBack() {
        activity?.supportFragmentManager?.popBackStackImmediate()
    }

    override fun onDestroy() {
        super.onDestroy()
        baseDisposable.clear()
    }
}