package com.gaanap.gaanapehchana.fragments

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.utility.EventUtils
import com.gaanap.gaanapehchana.utility.MOST_TIME_SPENT_IN_TAB
import com.gaanap.gaanapehchana.utility.TAB
import com.gaanap.gaanapehchana.utility.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_web_view.*

class WebViewFragment : BaseFragment() {

    private var pageUrl     = ""
    private var pageTitle   = ""
    private var previousUrl = ""
    private var isBlog      = false

    companion object {
        private const val PAGE_URL = "url"
        private const val TITLE    = "title"
        private const val IS_BLOG  = "isBlog"

        fun newInstance(url: String?,title: String,isBlog: Boolean = false) = WebViewFragment().apply {
            arguments = Bundle().apply {
                putString(PAGE_URL,url)
                putString(TITLE,title)
                putBoolean(IS_BLOG,isBlog)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            pageUrl   = it.getString(PAGE_URL) ?: ""
            pageTitle = it.getString(TITLE)    ?: ""
            isBlog    = it.getBoolean(IS_BLOG)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_web_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(isBlog)
            EventUtils.logTimeEvent(MOST_TIME_SPENT_IN_TAB)
        title.text = pageTitle
        backIcon.setOnClickListener { activity?.onBackPressed() }
        setupWebView()
        swipeRefresh.setOnRefreshListener {
            webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
            webView.reload()
        }
    }

    fun onBackNavigation() {
        previousUrl = ""
        webView.goBack()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        webView.webViewClient   = AppWebViewClients()
        webView.webChromeClient = AppWebChromeClient()
        webView.settings.javaScriptEnabled    = true
        webView.settings.useWideViewPort      = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort      = true
        webView.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        webView.loadUrl(pageUrl)
    }

    private inner class AppWebChromeClient : WebChromeClient()


    private inner class AppWebViewClients : WebViewClient() {

        private fun loadUrl(view: WebView?,url: String?) : Boolean {
            if(previousUrl == url) {
                webView.scrollTo(0,0)
                return true
            }
            previousUrl  = url ?: ""
            view?.loadUrl(url)
            return false
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            if(swipeRefresh?.isRefreshing == false)
                progressBar?.visibility = View.VISIBLE
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return loadUrl(view,url)
        }

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            return loadUrl(view,request?.url?.toString())
        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            super.onReceivedError(view, request, error)
            if(!isNetworkAvailable(activity ?: return))
                connectionError?.visibility = View.VISIBLE
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            progressBar?.visibility    = View.GONE
            swipeRefresh?.isRefreshing = false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (isBlog)
            EventUtils.logGroupClickEvent(MOST_TIME_SPENT_IN_TAB, TAB, "Blog")
    }
}
