package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.view.*
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.interfaces.ForgotPasswordView
import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.models.response.ErrorResponse
import com.gaanap.gaanapehchana.presenter.ForgotPasswordPresenter
import com.gaanap.gaanapehchana.rx.RxDisposable
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.fragment_forgot_password.*


class ForgotPasswordDialog : KeyBoardListenerFragment(), ForgotPasswordView {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_forgot_password,container,false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        resetPassword.setOnClickListener {
            val email = edtEmailId.text.toString()
            if(ValidationUtils.isValidEmail(email)) {
                if(isNetworkAvailable(activity ?: return@setOnClickListener))
                    RxDisposable.add(ForgotPasswordPresenter(this).forgotPassword(email))
                else
                    onNetworkError()
            } else {
                edtEmailId.requestFocus()
                edtEmailId.error = getString(R.string.enter_valid_email_address)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout((getDeviceWidth() * .9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }


    override fun showProgress() {
        resetPassword.visibility = View.GONE
        progressBar.visibility   = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility   = View.GONE
        resetPassword?.visibility = View.VISIBLE
    }

    override fun onResponse(response: BaseResponse) {
        ResetPasswordDialog().show(activity?.supportFragmentManager, DIALOG)
        dismiss()
    }

    override fun onNetworkError() {
        activity?.showToast(R.string.connection_error)
    }

    override fun onError(error: ErrorResponse?) {
        activity?.showToast(R.string.email_does_not_exist)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        RxDisposable.clear()
    }
}
