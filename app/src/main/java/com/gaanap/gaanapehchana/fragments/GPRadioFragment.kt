package com.gaanap.gaanapehchana.fragments

import android.view.View
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.interfaces.GPRadioView
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.presenter.GPRadioPresenter
import com.gaanap.gaanapehchana.utility.*
import com.gaanap.gaanapehchana.widget.CustomViewPager
import kotlinx.android.synthetic.main.activity_player.*

class GPRadioFragment : BaseMusicFragment(),GPRadioView {

    private var isLoading     = false
    private var isPlayerSetup = false
    private val presenter     = GPRadioPresenter(this)


    override fun setRadioPlayer() {
        EventUtils.logTimeEvent(MOST_TIME_SPENT_IN_TAB)
        playListName = getString(R.string.kuch_bhi_sunao)
        if(playListName == MusicPlayer.getPlayListName()) {
            songsList = MusicPlayer.getPlayList()
            position  = MusicPlayer.getSongPosition() ?: 0
            setData()
            isPlayerSetup = true
            setupPlayer()
        } else
            getRandomSongs()
    }

    override fun onPageSelected(position: Int) {
        if(adapter.count > 19 &&  position >= adapter.count - 5 && !isLoading) {
            isLoading = true
            baseDisposable.add(presenter.getRandomSongsFromServer(getUser()?.user_id ?: return)
                    .subscribe ({
                        if(it.isEmpty()) return@subscribe
                        songsList!!.addAll(it)
                        adapter.updateData(it)
                        if(isPlayerSetup) setupPlayer()
                        isLoading = false
                    },Throwable::printStackTrace))
        }
    }

    private fun getRandomSongs() {
        baseDisposable.add(presenter.getRandomSongs(getUser()?.user_id ?: return)
                .subscribe(this::onResponse,this::onError))
    }

    override fun onResponse(response: ArrayList<PlayList>) {
        if(response.isEmpty()) return
        if(songsList == null)
            songsList = ArrayList()
        songsList!!.addAll(response.shuffled())
        setData()
    }

    override fun showProgress() {}

    override fun hideProgress() {}

    override fun setupViewPager() {
        super.setupViewPager()
        viewPager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.right)
        previousIcon.visibility = View.INVISIBLE
        repeatIcon.visibility   = View.INVISIBLE
        shuffleIcon.visibility  = View.INVISIBLE
    }

    override fun playPauseSongs() {
        if(isPlayerSetup)
            super.playPauseSongs()
        else {
            isPlayerSetup = true
            setupPlayer()
        }
    }

    override fun play() {
        if(isPlayerSetup)
            super.play()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        baseDisposable.clear()
        EventUtils.logGroupClickEvent(MOST_TIME_SPENT_IN_TAB, TAB, "Radio")
    }

    companion object {
        @JvmStatic
        fun newInstance() = GPRadioFragment()
    }
}
