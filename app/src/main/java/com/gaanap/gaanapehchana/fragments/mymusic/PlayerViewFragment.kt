package com.gaanap.gaanapehchana.fragments.mymusic

import android.animation.ObjectAnimator
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.fragments.BaseFragment
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.fragment_player_view.*

class PlayerViewFragment : BaseFragment() {

    private var playList: PlayList? = null

    private var anim: ObjectAnimator? = null

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(!isVisibleToUser)
            stop()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_player_view,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        thumbIcon.loadCircleImage(playList?.thumbnail,R.drawable.ic_player_disc)
        songName.text  = playList?.song
        songMovie.text = playList?.movieName
    }

    fun start(duration: Long = 30000) {
        startAnimation(duration)
    }

    fun resumeAnimation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            anim?.resume()
        } else
            anim?.start()
    }

    fun pauseAnimation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            anim?.pause()
        } else
            anim?.cancel()
    }

    fun stop() {
        anim?.cancel()
        thumbIcon?.animate()?.rotation(0f)
        thumbIcon?.clearAnimation()
    }

    private fun startAnimation(duration: Long) {
        anim = ObjectAnimator.ofFloat(thumbIcon ?: return, "rotation", 0f, 360f)
        anim?.duration    = duration
        anim?.repeatCount = Animation.INFINITE
        anim?.repeatMode  = ObjectAnimator.RESTART
        anim?.start()
    }

    companion object {
        @JvmStatic
        fun newInstance(playList: PlayList) = PlayerViewFragment().apply {
            this.playList = playList
        }
    }
}