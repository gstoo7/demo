package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.helper.SoftKeyboard

open class KeyBoardListenerFragment : DialogFragment() {

    private var keyboard: SoftKeyboard? = null
    private var isKeyboardOpen = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE,STYLE_NORMAL)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layout = view.findViewById<ViewGroup>(R.id.rootContainer) ?: return
        layout.isClickable = true
        layout.isFocusable = true
        setListener(layout)
    }
    
    private fun setListener(viewGroup: ViewGroup) {
        keyboard   = SoftKeyboard(viewGroup,activity ?: return)
        keyboard?.setSoftKeyboardCallback(object : SoftKeyboard.SoftKeyboardChanged {
            override fun onSoftKeyboardHide() {
                isKeyboardOpen = false
            }

            override fun onSoftKeyboardShow() {
                isKeyboardOpen = true
            }

        })

        viewGroup.setOnTouchListener { touchView, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    if(isKeyboardOpen) keyboard?.closeSoftKeyboard()
                }

                MotionEvent.ACTION_UP -> touchView.performClick()
            }
            false
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.decorView?.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    if(isKeyboardOpen) keyboard?.closeSoftKeyboard() else dismiss()
                }

                MotionEvent.ACTION_UP -> {}
            }
            false
        }

        dialog?.setCanceledOnTouchOutside(false)
    }

    override fun onDestroy() {
        super.onDestroy()
        keyboard?.unRegisterSoftKeyboardCallback()
    }
}