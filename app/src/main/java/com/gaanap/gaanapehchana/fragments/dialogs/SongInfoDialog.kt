package com.gaanap.gaanapehchana.fragments.dialogs

import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetDialogFragment
import android.text.TextUtils
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v7.widget.GridLayout
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.utility.PLAY_LIST
import com.gaanap.gaanapehchana.utility.getDeviceWidth
import com.gaanap.gaanapehchana.utility.getTextWidth
import kotlinx.android.synthetic.main.fragment_song_info_dialog.*


class SongInfoDialog : BottomSheetDialogFragment() {

    private var info: PlayList? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { info = it.getParcelable(PLAY_LIST) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_song_info_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        singer.text   = info?.singer1   ?: ""
        year.text     = info?.year      ?: ""
        movie.text    = info?.movieName ?: ""
        composer.text = info?.composer1 ?: ""
        lyricist.text = info?.lyricist  ?: ""

        if(singer.getTextWidth() > getDeviceWidth()/2) {
            val gridParams = ConstraintLayout.LayoutParams(ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT))
            gridLayout.layoutParams = gridParams
            val params = GridLayout.LayoutParams(ViewGroup.LayoutParams(0,ViewGroup.LayoutParams.WRAP_CONTENT))
            params.setGravity(Gravity.FILL)
            singer.layoutParams = params
        }

        if(TextUtils.isEmpty(info?.comments1))  {
            didYouNoIcon1.visibility = View.GONE
            labelDidYouNo.visibility = View.GONE
            didYouNo.visibility      = View.GONE
        } else {
            didYouNo.text = info?.comments1
        }


        gpRating.rating   = info?.ratingAvg?.toFloatOrNull() ?: 0f
    }


    companion object {
        @JvmStatic
        fun newInstance(info: PlayList) = SongInfoDialog().apply {
            arguments = Bundle().apply { putParcelable(PLAY_LIST, info) }
        }
    }
}
