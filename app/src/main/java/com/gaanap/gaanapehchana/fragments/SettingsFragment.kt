package com.gaanap.gaanapehchana.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.NotificationSettings
import com.gaanap.gaanapehchana.models.User
import com.gaanap.gaanapehchana.services.jobs.SettingsJob
import com.gaanap.gaanapehchana.utility.CONTACT_US_FRAGMENT
import com.gaanap.gaanapehchana.utility.getAppVersion
import com.gaanap.gaanapehchana.utility.showToast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : BaseFragment() {

    private var currentUser: User? = null

    private val request     = NotificationSettings()
    private val listener    = CompoundButton.OnCheckedChangeListener { _, _ -> updateSettings() }
    private val smsListener = CompoundButton.OnCheckedChangeListener (this::updateSMSSettings)
    private var newSettings = ""
    private var oldSettings = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentUser             = SessionManager.getUser(context)
        request.blog            = request.Blog()
        request.tmk_game        = request.Tmk_game()
        request.system_messages = request.System_messages()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_settings,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInitState()
        setListener()
    }

    private fun setInitState() {
        val version     = getString(R.string.app_version) + " " + getAppVersion(activity ?: return)
        appVersion.text = version

        val user    = currentUser?.notificationSettings
        oldSettings = Gson().toJson(user)

        swPushBlog  .isChecked = user?.blog?.push  ?: 0 > 0
        swEmailBlog .isChecked = user?.blog?.email ?: 0 > 0
        swSMSBlog   .isChecked = user?.blog?.sms   ?: 0 > 0

        swPushTMK  .isChecked = user?.tmk_game?.push  ?: 0 > 0
        swEmailTMK .isChecked = user?.tmk_game?.email ?: 0 > 0
        swSMSTMK   .isChecked = user?.tmk_game?.sms   ?: 0 > 0

        swPushCustomMsg  .isChecked = user?.system_messages?.push  ?: 0 > 0
        swEmailCustomMsg .isChecked = user?.system_messages?.email ?: 0 > 0
        swSMSCustomMsg   .isChecked = user?.system_messages?.sms   ?: 0 > 0
    }

    private fun setListener() {
        backIcon         .setOnClickListener{activity?.supportFragmentManager?.popBackStackImmediate()}
        pushSettingView  .setOnClickListener{ onPushIconClick()  }
        emailSettingView .setOnClickListener{ onEmailIconClick() }
        smsSettingView   .setOnClickListener{ onSMSIconClick()   }
        callIcon         .setOnClickListener{ loadFragment(ContactUsFragment(),CONTACT_US_FRAGMENT)  }

        swPushBlog  .setOnCheckedChangeListener(listener)
        swEmailBlog .setOnCheckedChangeListener(listener)
        swSMSBlog   .setOnCheckedChangeListener(smsListener)

        swPushTMK   .setOnCheckedChangeListener(listener)
        swEmailTMK  .setOnCheckedChangeListener(listener)
        swSMSTMK    .setOnCheckedChangeListener(smsListener)

        swPushCustomMsg  .setOnCheckedChangeListener(listener)
        swEmailCustomMsg .setOnCheckedChangeListener(listener)
        swSMSCustomMsg   .setOnCheckedChangeListener(smsListener)
    }


    private fun closeAll() {
        if(pushView.visibility == View.VISIBLE) {
            pushView.visibility = View.GONE
            pushSettingIcon.rotation   = 90f
        }

        if(emailView.visibility == View.VISIBLE) {
            emailView.visibility = View.GONE
            emailSettingIcon.rotation   = 90f
        }

        if(smsView.visibility == View.VISIBLE) {
            smsView.visibility = View.GONE
            smsSettingIcon.rotation = 90f
        }

    }

    private fun onPushIconClick() {
        if(pushView.visibility == View.VISIBLE) {
            pushView.visibility = View.GONE
            pushSettingIcon.rotation = 90f
        } else {
            closeAll()
            pushView.visibility = View.VISIBLE
            pushSettingIcon.rotation = 270f
        }
    }

    private fun onEmailIconClick() {
        if(emailView.visibility == View.VISIBLE) {
            emailView.visibility = View.GONE
            emailSettingIcon.rotation = 90f
        } else {
            closeAll()
            emailView.visibility = View.VISIBLE
            emailSettingIcon.rotation = 270f
        }
    }

    private fun onSMSIconClick() {
        if(smsView.visibility == View.VISIBLE) {
            smsView.visibility = View.GONE
            smsSettingIcon.rotation = 90f
        } else {
            closeAll()
            smsView.visibility = View.VISIBLE
            smsSettingIcon.rotation = 270f
        }
    }


    private fun updateSettings() {

        request.blog.push  = if (swPushBlog.isChecked)  1 else 0
        request.blog.email = if (swEmailBlog.isChecked) 1 else 0
        request.blog.sms   = if (swSMSBlog.isChecked)   1 else 0

        request.tmk_game.push  = if (swPushTMK.isChecked)  1 else 0
        request.tmk_game.email = if (swEmailTMK.isChecked) 1 else 0
        request.tmk_game.sms   = if (swSMSTMK.isChecked)   1 else 0

        request.system_messages.push  = if (swPushCustomMsg.isChecked)  1 else 0
        request.system_messages.email = if (swEmailCustomMsg.isChecked) 1 else 0
        request.system_messages.sms   = if (swSMSCustomMsg.isChecked)   1 else 0

        newSettings =  Gson().toJson(request)

        if(newSettings.isNotBlank() && oldSettings != newSettings) {
            currentUser?.notificationSettings = request
            SessionManager.saveUser(context,currentUser)
            SettingsJob.scheduleSettingsTask(activity ?: return, currentUser?.user_id ?: return, newSettings)
        }

    }

    private fun updateSMSSettings(buttonView: CompoundButton, isChecked: Boolean) {
        if(isChecked) {
            when {
                currentUser?.mobileNumber.isNullOrBlank() -> {
                    activity?.showToast(R.string.pls_update_mobile_number)
                    buttonView.isChecked = false
                    return
                }

                currentUser?.isMobileVerified == "0" -> {
                    activity?.showToast(R.string.pls_verify_mobile_number)
                    buttonView.isChecked = false
                    return
                }
            }
        }

        updateSettings()
    }
}