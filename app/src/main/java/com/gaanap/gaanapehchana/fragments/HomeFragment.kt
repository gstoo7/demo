package com.gaanap.gaanapehchana.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.GamesPagerAdapter
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.firebasehelper.model.GameInfo
import com.gaanap.gaanapehchana.helper.ZoomOutPageTransformer
import com.gaanap.gaanapehchana.utility.*
import com.gaanap.gaanapehchana.view.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {

    private val bgArray: IntArray = intArrayOf(R.drawable.rect_gradient_radius_20dp_tmk,R.drawable.rect_gradient_radius_20dp_bbg,R.drawable.rect_gradient_radius_20dp_aursunao)
    private var currentItem = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_home,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        EventUtils.logTimeEvent(MOST_TIME_SPENT_IN_TAB)
        setupViewPager()
        setListener()
        profilePicView.loadCircleImage(SessionManager.getUser(context)?.pic)
    }

    override fun onResume() {
        super.onResume()
        setTotalScore()
        userName.text = SessionManager.getUser(context)?.fullName ?: ""
    }

    private fun setupViewPager() {
        val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (50 * 2).toFloat(), resources.displayMetrics).toInt()
        viewPager.pageMargin         = -margin
        viewPager.clipToPadding      = false
        viewPager.offscreenPageLimit = 3
        viewPager.adapter            = GamesPagerAdapter(activity?:return,this::onItemClick)
        viewPager.setPageTransformer(true, ZoomOutPageTransformer(true))
        changeBackgroundColor(viewPager.currentItem)
    }

    private fun setListener() {
        howToPlay.setOnClickListener   { startHowToPlayActivity(currentItem) }

        leaderboardIcon.setOnClickListener {
            EventUtils.logClickEvent(LEADERBOARD_BUTTON,"Home Screen")
            activity?.startActivity<LeaderboardActivity>()
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                changeBackgroundColor(position)
            }

        })

        //profilePicView.setOnClickListener { activity?.profileIcon?.performClick() }
    }

    private fun changeBackgroundColor(index: Int) {
        currentItem = index
        leaderboard  .setBackgroundResource(bgArray[index])
        howToPlay    .setBackgroundResource(bgArray[index])
    }

    private fun onItemClick(position:Int) {
        when(position) {
           0 -> loadFragment(TMKListFragment(),TMK_LIST_FRAGMENT,true)
           1 -> startDecadesActivity(GameType.BBG)
           2 -> startDecadesActivity(GameType.ASN)
        }
    }

    private fun startDecadesActivity(game: GameType) {
        val intent = Intent()
        intent.putExtra(GAME,game)
        activity?.startIntentActivity<DecadesActivity>(intent)
    }

    private fun startHowToPlayActivity(position:Int) {
        val intent = Intent()
        when(position) {
            0 -> intent.putExtra(GAME,GameType.TMK)
            1 -> intent.putExtra(GAME,GameType.BBG)
            2 -> intent.putExtra(GAME,GameType.ASN)
        }
        activity?.startIntentActivity<HowToPlayActivity>(intent)
    }

    fun playGame() {
        onItemClick(viewPager.currentItem)
    }

    private fun setTotalScore() {
        val lifeTimeScore = FirebaseNodeRef.getLifetimeMasterRef(getUser()?.user_id ?: return)
        lifeTimeScore.keepSynced(true)
        lifeTimeScore.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                try {
                    val scoreInfo  = dataSnapshot?.getValue(GameInfo::class.java)
                    score?.text = scoreInfo?.totalScore?.toString() ?: "0"
                } catch (e: Exception) {
                    Log.d("scoreError",":$e")
                }

            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventUtils.logGroupClickEvent(MOST_TIME_SPENT_IN_TAB, TAB, "Game")
    }
}
