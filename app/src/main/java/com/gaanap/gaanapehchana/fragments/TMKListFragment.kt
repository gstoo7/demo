package com.gaanap.gaanapehchana.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.TMKListAdapter
import com.gaanap.gaanapehchana.helper.GridItemDecoration
import com.gaanap.gaanapehchana.interfaces.TMKGameView
import com.gaanap.gaanapehchana.models.response.TMKListResponse
import com.gaanap.gaanapehchana.presenter.TMKPresenter
import android.os.Build
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.gaanap.gaanapehchana.adapter.TMKCategoriesAdapter
import com.gaanap.gaanapehchana.firebasehelper.model.GameInfo
import com.gaanap.gaanapehchana.helper.SpaceItemDecoration
import com.gaanap.gaanapehchana.models.response.TMKGame
import com.gaanap.gaanapehchana.utility.*
import com.gaanap.gaanapehchana.view.TMKInfoActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_tmklist.*
import kotlinx.android.synthetic.main.search_toolbar.*

class TMKListFragment : BaseFragment(), TMKGameView {

    private val adapter     : TMKListAdapter   = TMKListAdapter(mutableListOf(),this::onGameSelect)
    private val presenter   : TMKPresenter     = TMKPresenter(this)
    private var searchUtils : SearchViewUtils? = null

    private val limit        = 10
    private val threshold    = 2
    private var currentPage  = 1
    private var totalPages   = 0
    private var isLoading    = false
    private var categorySlug = ""
    private var searchQuery  = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tmklist,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupToolbar()
        setListener()
        getData(currentPage,categorySlug)
    }

    override fun onResume() {
        super.onResume()
        userName.text = getUser()?.fullName
        profilePic.loadCircleImage(getUser()?.pic)
        val lifeTimeScore = FirebaseNodeRef.getLifetimeMasterRef(getUser()?.user_id ?: return)
        lifeTimeScore.keepSynced(true)
        lifeTimeScore.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val scoreInfo = dataSnapshot?.getValue(GameInfo::class.java)
                scoreView?.text = scoreInfo?.totalScore?.toString() ?: "0"
            }

        })
    }

    private fun setupRecyclerView() {
        categoryRecyclerView.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)
        categoryRecyclerView.adapter       = TMKCategoriesAdapter(presenter.getGameCategories(),this::updateCategorySlug)
        categoryRecyclerView.setHasFixedSize(true)
        categoryRecyclerView.addItemDecoration(SpaceItemDecoration(dp2px(10)))

        val layoutManager = GridLayoutManager(activity,2)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter       = adapter
        recyclerView.setHasFixedSize(true)
        recyclerView.addItemDecoration(GridItemDecoration(dp2px(10), 2))

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (currentPage >= totalPages)
                    return
                val totalItemCount = layoutManager.itemCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItemCount <= lastVisibleItem + threshold) {
                    currentPage++
                    getData(currentPage,categorySlug)
                    isLoading = true
                }
            }
        })

        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (adapter.getItemViewType(position)) {
                    ITEM    -> 1
                    LOADING -> 2
                    else    -> -1
                }
            }
        }
    }

    fun setupToolbar() {
        if(searchUtils == null)
            searchUtils = SearchViewUtils(activity ?: return,this::onMenuItemExpand)
        searchToolbar.inflateMenu(R.menu.menu_search)
        searchUtils?.setSearchTollbar(searchToolbar)
        searchUtils?.searchListener = this::onQuerySearch
    }

    fun setListener() {
        searchView.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                searchUtils?.circleReveal(searchToolbar, 1, true, true)
            else
                searchToolbar?.visibility = View.VISIBLE

            searchUtils?.itemSearch?.expandActionView()

            EventUtils.logClickEvent(TMK_SEARCH_BUTTON)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_tmk_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    searchUtils?.circleReveal(searchToolbar, 1, true, true)
                else
                    searchToolbar?.visibility = View.VISIBLE

                searchUtils?.itemSearch?.expandActionView()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onMenuItemExpand(isExpand: Boolean) {
        toolbar.visibility = if(isExpand) View.GONE else View.VISIBLE
    }

    override fun showProgress() {
        if(adapter.itemCount <= 0)
            progressBar?.visibility = View.VISIBLE
        else
            adapter.addLoadingFooter()
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
        adapter.removeLoadingFooter()
    }

    override fun onResponse(response: TMKListResponse) {
        hideProgress()
        if (response.error == 0 && response.games?.size?:0 > 0) {
            totalPages = response.pagination?.lastPageNo ?: 0
            adapter.addAll(response.games!!)
            adapter.filter.filter(searchQuery)
        }
        isLoading = false
    }

    override fun onError(throwable: Throwable) {
        super.onError(throwable)
        hideProgress()
    }

    override fun onNetworkError() {
        if(adapter.itemCount <= 0)
            connectionError.visibility = View.VISIBLE
    }

    private fun getData(page: Int,categorySlug: String = "") {
        baseDisposable.clear()
        baseDisposable.add(presenter.getGameList(getUser()?.user_id ?: "",page,limit,categorySlug))
    }

    private fun onGameSelect(game: TMKGame) {
        activity?.startIntentActivity<TMKInfoActivity>(Intent().apply {
            putExtra(GAME_ID,game.gameid)
            putExtra(THEME,game.theme)
            putExtra(PREVIOUS_SCORE,game.my_score)
        })
    }

    private fun updateCategorySlug(categorySlug : String) {
        this.categorySlug = categorySlug
        currentPage = 1
        totalPages  = 0
        adapter.clear()
        getData(currentPage,categorySlug)
    }

    private fun onQuerySearch(query: String) {
        searchQuery = query
        adapter.filter.filter(query)
    }

    fun updateGameScore(gameId: String,score: Int) {
        adapter.updateGameScore(gameId,score)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        baseDisposable.clear()
    }
}