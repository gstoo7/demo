package com.gaanap.gaanapehchana.fragments.mymusic

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.PlayerType
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.services.MusicService
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.fragment_small_player.*

class SmallPlayerFragment : BaseRadioFragment(), MusicService.PlayerServiceListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_small_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        MusicPlayer.registerServicePlayerListener(this)
        view.setOnClickListener {
            if(!isNetworkAvailable(activity ?: return@setOnClickListener)) {
                activity?.showToast(R.string.connection_error)
                return@setOnClickListener
            }
            try {
                if(PlayerType.RADIO == MusicPlayer.getPlayerType()) {
                    val navigationView = activity?.findViewById<BottomNavigationView>(R.id.bottomNavView)
                    navigationView?.selectedItemId = R.id.actionRadio
                } else {
                    loadFragment(PlayerFragment.newInstance(MusicPlayer.getPlayListName() ?: "",
                            MusicPlayer.getPlayListId() ?: "", MusicPlayer.getSongPosition() ?: 0,
                            MusicPlayer.getPlayList()!!, false), PLAYER_FRAGMENT)
                }

            } catch (e: Exception) {
                Log.d("Error",":$e")
            }

        }

        playPauseIcon.setOnClickListener {
            if(!isNetworkAvailable(activity ?: return@setOnClickListener)) {
                activity?.showToast(R.string.connection_error)
                return@setOnClickListener
            }

            if(MusicPlayer.isPlaying())
                MusicPlayer.pause()
            else
                MusicPlayer.play(MusicPlayer.getSongPosition() ?: return@setOnClickListener)
        }

        playListIcon.setOnClickListener {
            if(!isNetworkAvailable(activity ?: return@setOnClickListener)) {
                activity?.showToast(R.string.connection_error)
                return@setOnClickListener
            }
            try {
                val list = ArrayList<PlayList>(MusicPlayer.getPlayList())
                val fragment = activity?.supportFragmentManager?.findFragmentByTag("PlayListSongs") as? PlayListSongsFragment
                        ?: PlayListSongsFragment.newInstance(MusicPlayer.getPlayListName(), list)
                if (fragment.isVisible) {
                    fragment.onResponse(list)
                } else {
                    loadFragment(PlayListSongsFragment.newInstance(MusicPlayer.getPlayListName()
                            ?: "", list, false), "PlayListSongs")
                }
            } catch (e: Exception) {
               Log.e("error",":$e")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        playPauseIcon.setImageResource(if(MusicPlayer.isPlaying()) R.drawable.ic_pause else R.drawable.ic_play)
        val currentSongs = MusicPlayer.getCurrentAudio()
        if(currentSongs != null) {
            songName.text = currentSongs.song ?: ""
            thumbIcon.loadUrl(currentSongs.thumbnail,R.drawable.ic_player_disc)
        }

        if(PlayerType.RADIO == MusicPlayer.getPlayerType())
            playListIcon.visibility = View.INVISIBLE
    }

    override fun onPreparing(position: Int) {
        val currentSongs = MusicPlayer.getCurrentAudio()
        songName.text    = currentSongs?.song ?: ""
        thumbIcon.loadUrl(currentSongs?.thumbnail ?: "")
    }

    override fun onPlaying() {
        playPauseIcon.setImageResource(R.drawable.ic_pause)
    }

    override fun onPaused() {
        playPauseIcon.setImageResource(R.drawable.ic_play)
    }

    override fun onPreparedAudio(audioName: String?, duration: Long) {
        playPauseIcon.setImageResource(R.drawable.ic_pause)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MusicPlayer.unregisterServicePlayerListener(this)
    }

    companion object {
        @JvmStatic
        fun newInstance() = SmallPlayerFragment()
    }
}