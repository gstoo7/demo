package com.gaanap.gaanapehchana.fragments.mymusic

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.MyPlayListAdapter
import com.gaanap.gaanapehchana.fragments.dialogs.CreatePlayListDialog
import com.gaanap.gaanapehchana.helper.DividerItemDecoration
import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.models.response.PublicPlaylist
import com.gaanap.gaanapehchana.presenter.RadioPresenter
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_my_playlist.*
import android.support.design.widget.BottomSheetDialog
import android.view.MotionEvent
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.bottomsheet_myplaylist.view.*

class MyPlayListFragment : BaseRadioFragment() {

    private val presenter  = RadioPresenter(this)
    private val adapter    = MyPlayListAdapter(arrayListOf(),this::onClickPlayList,this::onDeletePlayList)
    private val disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_playlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        setListener()
        disposable.add(presenter.getMyPlayList(getUser()?.user_id))
    }

    private fun setRecyclerView() {
        myPlaylist.layoutManager = LinearLayoutManager(activity)
        myPlaylist.adapter       = adapter
        myPlaylist.addItemDecoration(DividerItemDecoration(activity))
    }

    private fun setListener() {
        createFirst    .setOnClickListener { createFirstPlayList() }
        createPlaylist .setOnClickListener { CreatePlayListDialog.newInstance(this::onResponse).show(activity?.supportFragmentManager,DIALOG) }
        rootContainer  .setOnTouchListener { touchView, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> hideKeyboard(activity ?: return@setOnTouchListener false)
                MotionEvent.ACTION_UP -> touchView.performClick()
            }
            false
        }
    }

    private fun createFirstPlayList() {
        val name = title.text.toString()
        if(name.isBlank()) {
            activity?.showToast(R.string.enter_valid_title)
            return
        }

        val isPrivate = if (radioPrivate.isChecked) 1 else 0

        disposable.add(presenter.createPlayList(name,isPrivate,getUser()?.user_id))
    }

    override fun onResponse(response: BaseResponse) {
        disposable.add(presenter.getMyPlayList(getUser()?.user_id))
        hideProgress()
    }

    override fun onResponse(response: ArrayList<PublicPlaylist>) {
        hideProgress()
        if(response.isEmpty())
            setViewState(true)
        else {
            setViewState(false)
            adapter.addAll(response)
        }
    }

    override fun onNetworkError() {
        connectionError.visibility = View.VISIBLE
    }

    private fun setViewState(flag: Boolean) {
        if(flag) {
            firstPlayList.visibility = View.VISIBLE
            createFirst.visibility   = View.VISIBLE
            newPlayList.visibility   = View.GONE
        } else {
            firstPlayList.visibility = View.GONE
            createFirst.visibility   = View.GONE
            newPlayList.visibility   = View.VISIBLE
        }
    }

    private fun onClickPlayList(playList: PublicPlaylist) {
        if(playList.totalSongs <= 0) {
            activity?.showToast(R.string.pls_add_songs)
            return
        }

        if(!isNetworkAvailable(activity ?: return)) {
            activity?.showToast(R.string.connection_error)
            return
        }

        loadFragment(PlayListSongsFragment.newInstance(playList.id,playList.radioName), PLAYLIST_FRAGMENT)
    }

    private fun onDeletePlayList(playListId: String) {
        if(!isNetworkAvailable(activity ?: return)) {
            activity?.showToast(R.string.connection_error)
            return
        }

        val dialogView = layoutInflater.inflate(R.layout.bottomsheet_myplaylist,null)
        val dialog     = BottomSheetDialog(context ?: return)
        dialog.setContentView(dialogView)
        dialog.setCancelable(true)
        dialogView.delete.setOnClickListener {
            if(isNetworkAvailable(activity ?: return@setOnClickListener)) {
                disposable.clear()
                disposable.add(presenter.deletePlayList(playListId))
            } else {
                activity?.showToast(R.string.connection_error)
            }
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.clear()
    }

    companion object {
        @JvmStatic
        fun newInstance() = MyPlayListFragment()
    }
}