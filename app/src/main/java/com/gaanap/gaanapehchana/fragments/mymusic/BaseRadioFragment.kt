package com.gaanap.gaanapehchana.fragments.mymusic

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.fragments.BaseFragment
import com.gaanap.gaanapehchana.interfaces.RadioView
import com.gaanap.gaanapehchana.services.MusicService

open class BaseRadioFragment : BaseFragment(),RadioView, MusicService.PlayerServiceListener {

    private var progressBar : ProgressBar? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressBar)
    }

    override fun showProgress() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
    }

    override fun onPreparing(position: Int) {}
    override fun onPreparedAudio(audioName: String?, duration: Long) {}
    override fun onCompletedAudio() {}
    override fun onPaused() {}
    override fun onContinueAudio() {}
    override fun onPlaying() {}
    override fun onTimeChanged(currentTime: Long, duration: Long) {}
}