package com.gaanap.gaanapehchana.fragments.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import com.gaanap.gaanapehchana.R
import kotlinx.android.synthetic.main.paused_game_dialog.*

class PausedGameDialog(context: Context) : Dialog(context,R.style.DialogTheme) {

    private var callbacks: Callbacks? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.paused_game_dialog)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setCancelable(false)
        setListener()
    }

    fun setListener() {
        closeGame.setOnClickListener { callbacks?.onClose() }

        resetGame.setOnClickListener {
            dismiss()
            callbacks?.onReset()
        }

        playGame.setOnClickListener  {
            dismiss()
            callbacks?.onPlay()
        }
    }

    fun setCallbacks(callbacks: Callbacks) {
        this.callbacks = callbacks
    }



    interface Callbacks {
        fun onClose()
        fun onReset()
        fun onPlay()
    }
}