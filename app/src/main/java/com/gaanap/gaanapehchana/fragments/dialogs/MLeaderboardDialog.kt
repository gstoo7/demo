package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.ScoreType
import com.gaanap.gaanapehchana.firebasehelper.model.GameInfo
import com.gaanap.gaanapehchana.firebasehelper.model.LeaderboardModel
import com.gaanap.gaanapehchana.utility.GAME_PLAYED_INFO
import com.gaanap.gaanapehchana.utility.SCORE
import com.gaanap.gaanapehchana.utility.showToast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_mleaderboard_dialog.*


class MLeaderboardDialog : DialogFragment() {

    private var userId    = ""
    private var scoreType = ScoreType.WEEKLY

    private var leaderboard: LeaderboardModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE,R.style.DialogTheme)
        arguments?.let {
            leaderboard = it.getParcelable(GAME_PLAYED_INFO)
            scoreType   = it.getSerializable(SCORE) as ScoreType
        }

        userId = leaderboard?.userId ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mleaderboard_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        totalScore.text = leaderboard?.gameInfo?.totalScore?.toString() ?: "0"
        profileIcon.loadUrl(leaderboard?.gameInfo?.profileIcon ?: "")
        buildQuery()
        closeIcon.setOnClickListener { dismiss() }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawableResource(R.drawable.rect_white_with_bottom_corners_20dp)
    }

    private fun buildQuery() {
        when(scoreType) {
            ScoreType.WEEKLY    -> onWeeklyData()
            ScoreType.MONTHLY   -> onMonthlyData()
            ScoreType.LIFE_TIME -> onLifeTimeData()
        }
    }

    private fun onWeeklyData() {
        FirebaseNodeRef.getWeeklyTMKScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val scoreInfo   = dataSnapshot?.getValue(GameInfo::class.java)
                gamesOfTMK?.text = scoreInfo?.totalGame?.toString()  ?: "0"
                scoreOfTMK?.text = scoreInfo?.totalScore?.toString() ?: "0"
            }

        })

        FirebaseNodeRef.getWeeklyBBGScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val scoreInfo   = dataSnapshot?.getValue(GameInfo::class.java)
                gamesOfBBG?.text = scoreInfo?.totalGame?.toString()  ?: "0"
                scoreOfBBG?.text = scoreInfo?.totalScore?.toString() ?: "0"

            }

        })

        FirebaseNodeRef.getWeeklyASNScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val scoreInfo   = dataSnapshot?.getValue(GameInfo::class.java)
                gamesOfASN?.text = scoreInfo?.totalGame?.toString()  ?: "0"
                scoreOfASN?.text = scoreInfo?.totalScore?.toString() ?: "0"
            }

        })
    }

    private fun onMonthlyData() {

        FirebaseNodeRef.getMonthlyTMKScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val scoreInfo   = dataSnapshot?.getValue(GameInfo::class.java)
                gamesOfTMK?.text = scoreInfo?.totalGame?.toString()  ?: "0"
                scoreOfTMK?.text = scoreInfo?.totalScore?.toString() ?: "0"
            }

        })

        FirebaseNodeRef.getMonthlyBBGScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val scoreInfo   = dataSnapshot?.getValue(GameInfo::class.java)
                gamesOfBBG?.text = scoreInfo?.totalGame?.toString()  ?: "0"
                scoreOfBBG?.text = scoreInfo?.totalScore?.toString() ?: "0"
            }

        })

        FirebaseNodeRef.getMonthlyASNScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val scoreInfo   = dataSnapshot?.getValue(GameInfo::class.java)
                gamesOfASN?.text = scoreInfo?.totalGame?.toString()  ?: "0"
                scoreOfASN?.text = scoreInfo?.totalScore?.toString() ?: "0"
            }

        })
    }

    private fun onLifeTimeData() {
        FirebaseNodeRef.getLifetimeTMKScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                try {
                    val scoreInfo = dataSnapshot?.getValue(GameInfo::class.java)
                    gamesOfTMK?.text = scoreInfo?.totalGame?.toString() ?: "0"
                    scoreOfTMK?.text = scoreInfo?.totalScore?.toString() ?: "0"
                } catch (e:Exception) {
                    activity?.showToast(R.string.something_wrong_error)
                }
            }

        })

        FirebaseNodeRef.getLifetimeBBGScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                try {
                    val scoreInfo = dataSnapshot?.getValue(GameInfo::class.java)
                    gamesOfBBG?.text = scoreInfo?.totalGame?.toString() ?: "0"
                    scoreOfBBG?.text = scoreInfo?.totalScore?.toString() ?: "0"
                } catch (e:Exception) {
                    activity?.showToast(R.string.something_wrong_error)
                }
            }

        })

        FirebaseNodeRef.getLifetimeASNScoreRef(userId).apply {
            keepSynced(true)
        }.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                try {
                    val scoreInfo   = dataSnapshot?.getValue(GameInfo::class.java)
                    gamesOfASN?.text = scoreInfo?.totalGame?.toString()  ?: "0"
                    scoreOfASN?.text = scoreInfo?.totalScore?.toString() ?: "0"
                } catch (e:Exception) {
                    activity?.showToast(R.string.something_wrong_error)
                }
            }

        })
    }

    companion object {

        @JvmStatic
        fun newInstance(leaderboard: LeaderboardModel, scoreType: ScoreType) =
                MLeaderboardDialog().apply {
                    arguments = Bundle().apply {
                        putParcelable(GAME_PLAYED_INFO,leaderboard)
                        putSerializable(SCORE,scoreType)
                    }
                }
    }
}
