package com.gaanap.gaanapehchana.fragments.mymusic

import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.PublicPlayListAdapter
import com.gaanap.gaanapehchana.interfaces.RadioView
import com.gaanap.gaanapehchana.models.response.PublicPlaylist
import com.gaanap.gaanapehchana.presenter.RadioPresenter
import com.gaanap.gaanapehchana.utility.PLAYLIST_FRAGMENT
import com.gaanap.gaanapehchana.utility.isNetworkAvailable
import com.gaanap.gaanapehchana.utility.showToast
import kotlinx.android.synthetic.main.fragment_public_playlist.*

class PublicPlayList : BaseRadioFragment(),RadioView {

    private val presenter = RadioPresenter(this)
    private val adapter   = PublicPlayListAdapter(arrayListOf(),this::onClickPlayList)
    private var currentView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseDisposable.add(presenter.getPublicPlayList(getUser()?.user_id))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if(currentView == null) inflater.inflate(R.layout.fragment_public_playlist, container, false)
        else currentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(currentView == null) {
            currentView = view
            publicPlaylist.adapter = adapter
            publicPlaylist.layoutManager = LinearLayoutManager(activity)
            ViewCompat.setNestedScrollingEnabled(publicPlaylist,false)
        }
    }

    override fun showProgress() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
    }

    override fun onResponse(response: ArrayList<PublicPlaylist>) {
        hideProgress()
        adapter.addAll(response)
    }

    override fun onNetworkError() {
        connectionError?.visibility = View.VISIBLE
    }

    private fun onClickPlayList(playList: PublicPlaylist) {
        if(!isNetworkAvailable(activity ?: return)) {
            activity?.showToast(R.string.connection_error)
            return
        }
        loadFragment(PlayListSongsFragment.newInstance(playList.id,playList.radioName), PLAYLIST_FRAGMENT)
    }

    override fun onDestroy() {
        super.onDestroy()
        baseDisposable.clear()
    }


    companion object {
        @JvmStatic
        fun newInstance() = PublicPlayList()
    }
}