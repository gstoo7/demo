package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.MyPlayListAdapter
import com.gaanap.gaanapehchana.interfaces.RadioView
import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.models.response.PublicPlaylist
import com.gaanap.gaanapehchana.presenter.RadioPresenter
import com.gaanap.gaanapehchana.utility.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_add_to_play_list_dialog.*

class AddToPlayListDialog : DialogFragment(),RadioView {

    private var songId     : String? = null
    private var playListId : String? = null
    private var listener   : ((String?,String?) -> Unit)? = null
    private val presenter  = RadioPresenter(this)
    private val adapter    = MyPlayListAdapter(arrayListOf(),this::onClickPlayList,null)
    private val disposable = CompositeDisposable()
    private var selPLName  = ""

    private val userId by lazy { SessionManager.getUser(activity)?.user_id }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE,STYLE_NORMAL)
        songId = arguments?.getString(SONGS_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_to_play_list_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

        closeIcon.setOnClickListener { dismiss() }

        createPlaylist.setOnClickListener {
            CreatePlayListDialog.newInstance {
                disposable.clear()
                disposable.add(presenter.getMyPlayList(userId))
            }.show(activity?.supportFragmentManager, DIALOG)
        }

        disposable.add(presenter.getMyPlayList(userId))
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout((getDeviceWidth() * 0.8).toInt(), (getDeviceHeight() * 0.7).toInt())
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }

    private fun setupRecyclerView() {
        rvMyPlaylist.layoutManager = LinearLayoutManager(activity)
        rvMyPlaylist.adapter       = adapter
        rvMyPlaylist.addItemDecoration(DividerItemDecoration(activity,LinearLayoutManager.VERTICAL))

    }

    override fun showProgress() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
    }

    override fun onResponse(response: ArrayList<PublicPlaylist>) {
        hideProgress()
        adapter.addAll(response)
    }

    override fun onResponse(response: BaseResponse) {
        hideProgress()
        listener?.invoke(response.message,playListId)
        activity?.showToast("Song has been added to $selPLName.",Toast.LENGTH_LONG)
        disposable.clear()
        disposable.add(presenter.getMyPlayList(userId))
    }

    private fun onClickPlayList(playList: PublicPlaylist) {
        playListId = playList.id
        selPLName  = playList.radioName
        disposable.add(presenter.addToPlayList(songId,playListId))
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    companion object {

        private const val SONGS_ID = "songs_id"

        fun newInstance(songsId: String,listener: ((String?,String?) -> Unit)?): AddToPlayListDialog {
            val fragment = AddToPlayListDialog()
            val args = Bundle()
            args.putString(SONGS_ID, songsId)
            fragment.arguments = args
            fragment.listener  = listener
            return fragment
        }
    }

}
