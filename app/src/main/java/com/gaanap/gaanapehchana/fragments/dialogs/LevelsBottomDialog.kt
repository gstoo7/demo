package com.gaanap.gaanapehchana.fragments.dialogs

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.Preference
import com.gaanap.gaanapehchana.presenter.PreferencePresenter
import com.gaanap.gaanapehchana.services.jobs.UpdatePreferenceJob
import com.gaanap.gaanapehchana.utility.*
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_levels_bottom_dialog.*

class LevelsBottomDialog : BottomSheetDialogFragment() {

    private val views = arrayListOf<TextView>()

    private var selectedDecadesBg = R.drawable.rect_gradient_radius_20dp_tmk

    private lateinit var levels : MutableList<Preference>

    private var listener :(() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            selectedDecadesBg = it.getInt(SELECTED_COLOR,R.drawable.rect_gradient_radius_20dp_tmk)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_levels_bottom_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()
        selectLevels.setBackgroundResource(selectedDecadesBg)
        done.setBackgroundResource(selectedDecadesBg)
        done.setOnClickListener { _ ->
            if(!levels.any { it.isSelected }) {
                activity?.showToast(R.string.pls_select_one_difficulty)
                return@setOnClickListener
            }

            Realm.getDefaultInstance().use {
                it.executeTransaction { realm ->
                    realm.insertOrUpdate(levels)
                }
            }

            levels.filter { it.isSelected }.forEach { level-> EventUtils.logGroupClickEvent(MOST_SELECTED,LEVEL,level.labelName) }

            if(context != null)
                UpdatePreferenceJob.schedulePreferenceTask(context!!.applicationContext)

            listener?.invoke()
            dismiss()
        }
    }

    private fun setData() {
        val presenter = PreferencePresenter()
        val realm     = Realm.getDefaultInstance()
        levels        = realm.copyFromRealm(presenter.getDifficultyFromDb(context ?: return,realm))
        realm.close()
        views.add(saLevel)
        views.add(reLevel)
        views.add(gaLevel)
        views.add(maLevel)
        views.add(paLevel)

        for (i in 0 until views.size) {
            if(levels[i].isSelected) {
                views[i].setBackgroundResource(selectedDecadesBg)
                views[i].setTextColor(Color.WHITE)
            } else {
                views[i].setBackgroundResource(R.drawable.rect_transparent_with_black_boder_20dp)
                views[i].setTextColor(Color.BLACK)
            }

            views[i].setOnClickListener{onDecadeClicks(it,levels[i])}
        }
    }


    private fun onDecadeClicks(view: View,level: Preference?) {
        val textView = view as TextView
        if(level?.isSelected == true) {
            level.isSelected = false
            textView.setBackgroundResource(R.drawable.rect_transparent_with_black_boder_20dp)
            textView.setTextColor(Color.BLACK)
        } else {
            level?.isSelected = true
            textView.setBackgroundResource(selectedDecadesBg)
            textView.setTextColor(Color.WHITE)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(color: Int,listener :(() -> Unit)?) = LevelsBottomDialog().apply {
            arguments = Bundle().apply {
                putInt(SELECTED_COLOR,color)
            }

            this.listener = listener
        }
    }
}
