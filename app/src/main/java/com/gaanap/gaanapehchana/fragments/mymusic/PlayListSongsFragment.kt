package com.gaanap.gaanapehchana.fragments.mymusic

import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.PlayListSongsAdapter
import com.gaanap.gaanapehchana.fragments.RadioSearchFragment
import com.gaanap.gaanapehchana.fragments.dialogs.SongInfoDialog
import com.gaanap.gaanapehchana.helper.DividerItemDecoration
import com.gaanap.gaanapehchana.models.event.PlayListEvent
import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.presenter.RadioPresenter
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.bottomsheet_myplaylist.view.*
import kotlinx.android.synthetic.main.bottomsheet_playlist_option.*
import kotlinx.android.synthetic.main.fragment_playlist_songs.*
import kotlinx.android.synthetic.main.view_for_music_toolbar.*

class PlayListSongsFragment : BaseRadioFragment() {

    private val presenter    = RadioPresenter(this)
    private var isMyPlayList = true
    private var playListId   = ""
    private var playListName = ""
    private lateinit var adapter: PlayListSongsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isMyPlayList = it.getBoolean(IS_MY_PLAY_LIST,false)
            playListId   = it.getString(PLAY_LIST_ID) ?: ""
            playListName = it.getString(PLAY_LIST_NAME) ?: getString(R.string.gaanap_radio)
        }

        adapter = PlayListSongsAdapter(activity,arrayListOf(),this::onClickSongs,this::performMoreOption)

        baseDisposable.add(RxBus.listen(PlayListEvent::class.java).subscribe ({
            adapter.updateItem(it.songId)
        },Throwable::printStackTrace))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_playlist_songs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSongsList()
        setListener()
        if (arguments?.containsKey(PLAY_LIST) == true) {
            val list: List<PlayList> = arguments!!.getParcelableArrayList(PLAY_LIST)
            hideProgress()
            onResponse(list)
        } else
            presenter.getPlayListSongs(playListId,getUser()?.user_id)


    }

    private fun setupSongsList() {
        radioTitle.text         = playListName
        songsList.layoutManager = LinearLayoutManager(activity)
        songsList.adapter       = adapter
        songsList.addItemDecoration(DividerItemDecoration(activity,R.drawable.line_divider))

    }

    private fun setListener() {
        backIcon   .setOnClickListener { activity?.onBackPressed() }
        searchIcon .setOnClickListener {
            EventUtils.logClickEvent(MY_MUSIC_SEARCH)
            loadFragment(RadioSearchFragment(),RADIO_SEARCH_FRAGMENT)
        }
    }

    override fun onResponse(response: List<PlayList>) {
        if(response.isEmpty())
            emptyView?.visibility = View.VISIBLE
        else
            adapter.addAll(response)
    }

    override fun onResponse(response: BaseResponse) {}

    override fun onNetworkError() {
        connectionError?.visibility = View.VISIBLE
    }

    private fun onClickSongs(position: Int,list: ArrayList<PlayList>) {
        if(!isNetworkAvailable(activity ?: return)) {
            activity?.showToast(R.string.connection_error)
            return
        }

        loadFragment(PlayerFragment.newInstance(playListName,playListId,position,list,isMyPlayList),PLAYER_FRAGMENT)
    }

    private fun performMoreOption(info: PlayList,position: Int) {
        val dialogView = layoutInflater.inflate(R.layout.bottomsheet_playlist_option,null)
        val dialog     = BottomSheetDialog(context ?: return)
        dialog.setContentView(dialogView)
        dialog.setCancelable(true)
        dialog.viewInfo.setOnClickListener {
            SongInfoDialog.newInstance(info).show(activity?.supportFragmentManager, DIALOG)
            dialog.dismiss()
        }
        if(isMyPlayList) {
            dialogView.delete.setOnClickListener {
                if(!isNetworkAvailable(activity ?: return@setOnClickListener)) {
                    activity?.showToast(R.string.connection_error)
                } else {
                    presenter.removeSongFromPlayList(playListId, info.id)
                    adapter.removeSong(position)
                    hideProgress()
                    if (adapter.itemCount <= 0)
                        emptyView.visibility = View.VISIBLE
                }
                dialog.dismiss()
            }
        } else
            dialog.delete.visibility = View.GONE
        dialog.show()
    }

    companion object {

        @JvmStatic
        fun newInstance(playlistId: String,playListName: String,isPlayList : Boolean = false) = PlayListSongsFragment().apply {
            arguments = Bundle().apply {
                putString(PLAY_LIST_ID,playlistId)
                putString(PLAY_LIST_NAME,playListName)
                putBoolean(IS_MY_PLAY_LIST,isPlayList)
            }
        }

        @JvmStatic
        fun newInstance(playListName: String?,playLists: ArrayList<PlayList>,isPlayList : Boolean = true) = PlayListSongsFragment().apply {
            arguments = Bundle().apply {
                putParcelableArrayList(PLAY_LIST,playLists)
                putString(PLAY_LIST_NAME,playListName)
                putBoolean(IS_MY_PLAY_LIST,isPlayList)
            }
        }
    }
}