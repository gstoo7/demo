package com.gaanap.gaanapehchana.fragments

import android.content.ComponentName
import android.content.ServiceConnection
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.PlayerPagerAdapter
import com.gaanap.gaanapehchana.enums.PlayerType
import com.gaanap.gaanapehchana.fragments.dialogs.AddToPlayListDialog
import com.gaanap.gaanapehchana.fragments.dialogs.SongInfoDialog
import com.gaanap.gaanapehchana.helper.ZoomOutPageTransformer
import com.gaanap.gaanapehchana.models.event.PlayListEvent
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.services.MusicService
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_player.*
import java.lang.Exception

open class BaseMusicFragment : BaseFragment(), MusicService.PlayerServiceListener {

    private var serviceToken : MusicPlayer.ServiceToken? = null
    protected var songsList  : ArrayList<PlayList>?      = null

    protected lateinit var adapter : PlayerPagerAdapter

    private var isRepeat       = false
    private var isShuffling    = false
    protected var playing      = false
    protected var paused       = false
    protected var isMyPlayList = false

    protected var position   = 0
    protected var songsIndex = 0

    protected var playListName = ""
    protected var playListId   = ""
    private val myPlayListSongs = hashSetOf<Int>()

    protected var playerType = PlayerType.RADIO


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_player,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewPager()
        setListener()
        if(playerType == PlayerType.MUSIC)
            setData()
        else
            setRadioPlayer()
    }


    protected open fun setRadioPlayer() {}

    protected open fun setupViewPager() {
        adapter           = PlayerPagerAdapter(childFragmentManager, arrayListOf())
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 3
        viewPager.setPageTransformer(true, ZoomOutPageTransformer(true))
    }

    protected open fun setListener() {
        backIcon      .setOnClickListener { activity?.onBackPressed() }
        playPauseIcon .setOnClickListener { playPauseSongs() }
        repeatIcon    .setOnClickListener { repeatSongs()    }
        shuffleIcon   .setOnClickListener { shuffleSongs()   }

        nextIcon.setOnClickListener {
            ++songsIndex
            playNextSongs()
        }

        previousIcon.setOnClickListener {
            --songsIndex
            playNextSongs()
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                this@BaseMusicFragment.onPageSelected(position)
                if(position != songsIndex) {
                    songsIndex = position
                    playNextSongs()
                }

                favoriteIcon.setImageResource(if(adapter.getSong(position)?.inMyPlaylist == true) R.drawable.ic_favorite_black_24dp else R.drawable.ic_favorite_border_black_24dp)
            }

        })

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if(fromUser) MusicPlayer.seekTo(progress)
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        favoriteIcon.setOnClickListener {
            val songId   = adapter.getSong(songsIndex)?.id
            val position = songsIndex
            when {
                songId.isNullOrBlank()    -> activity?.showToast(R.string.something_wrong_error)
                position == -1            -> activity?.showToast(R.string.something_wrong_error)
                !isNetworkAvailable(activity ?: return@setOnClickListener) -> activity?.showToast(R.string.connection_error)
                else -> {
                    AddToPlayListDialog.newInstance(songId!!) { _, _ ->
                        try {
                            if(isRemoving || isDetached)
                                return@newInstance
                            myPlayListSongs.add(position)
                            favoriteIcon.setImageResource(R.drawable.ic_favorite_black_24dp)
                            MusicPlayer.getPlayList()?.getOrNull(position)?.inMyPlaylist = true
                            RxBus.publish(PlayListEvent(playListId,songId))
                        } catch (e: Exception) {}

                    }.show(activity?.supportFragmentManager ?: return@setOnClickListener, DIALOG)
                }
            }
        }

        infoIcon.setOnClickListener {
            val currentSong = adapter.getSong(songsIndex) ?: return@setOnClickListener
            SongInfoDialog.newInstance(currentSong).show(activity?.supportFragmentManager ?: return@setOnClickListener, DIALOG)
        }
    }

    protected open fun setData() {
        progressbaView.visibility = View.GONE
        radioTitle.text = playListName
        if(songsList != null) {
            adapter.addAll(songsList!!)
            favoriteIcon.setImageResource(if(songsList?.getOrNull(0)?.inMyPlaylist == true) R.drawable.ic_favorite_black_24dp else R.drawable.ic_favorite_border_black_24dp)
        }
        songsIndex            = position
        viewPager.currentItem = position
        //setupPlayer()
    }

    private fun playNextSongs() {
        when {
            songsIndex < 0 -> {
                songsIndex = 0
                previousIcon.alpha     = 0.5f
                previousIcon.isEnabled = false
                return
            }
            songsIndex == 0 -> {
                previousIcon.alpha     = 0.5f
                previousIcon.isEnabled = false
            }

            songsIndex == adapter.count-1 -> {
                nextIcon.alpha     = 0.5f
                nextIcon.isEnabled = false
            }

            songsIndex >= adapter.count -> {
                songsIndex = adapter.count -1
                nextIcon.alpha     = 0.5f
                nextIcon.isEnabled = false
                return
            }
            else -> {
                previousIcon.alpha     = 1f
                previousIcon.isEnabled = true
                nextIcon.alpha         = 1f
                nextIcon.isEnabled     = true
            }
        }

        if(viewPager.currentItem != songsIndex)
            viewPager.currentItem = songsIndex

        play()
    }

    protected open fun play() {
        MusicPlayer.stop()
        MusicPlayer.play(songsIndex)
    }

    protected open fun playPauseSongs() {
        if (playing && !paused)
            MusicPlayer.pause()
        else
            MusicPlayer.play(MusicPlayer.getSongPosition() ?: return)
    }

    private fun repeatSongs() {
        isRepeat = !isRepeat

        if (isRepeat)
            repeatIcon.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        else
            repeatIcon.clearColorFilter()

        MusicPlayer.repeatSongs(isRepeat)
    }

    private fun shuffleSongs() {
        isShuffling = !isShuffling

        if (isShuffling)
            shuffleIcon.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        else
            shuffleIcon.clearColorFilter()

        MusicPlayer.shuffleSongs(isShuffling)
    }

    override fun onPreparedAudio(audioName: String?, duration: Long) {
        seekBar.progress = 0
        seekBar.max      = 100
        playPauseIcon.visibility = View.VISIBLE
        progressBar.visibility   = View.GONE
        adapter.currentFragment?.start()
    }

    override fun onCompletedAudio() {}

    override fun onPaused() {
        playing = false
        paused  = true
        playPauseIcon.setImageResource(R.drawable.ic_play_new)
        adapter.currentFragment?.pauseAnimation()
    }

    override fun onPlaying() {
        playing = true
        paused  = false
        playPauseIcon.setImageResource(R.drawable.ic_pause_new)
        adapter.currentFragment?.resumeAnimation()
    }

    override fun onTimeChanged(currentTime: Long, duration: Long) {
        activity?.runOnUiThread {
            if(duration > 0) {
                seekBar?.progress   = ((currentTime * 100)/duration).toInt()
                timer?.text         = getDurationString(currentTime)
                remainingTime?.text = getDurationString(duration - currentTime)
            }
        }
    }

    override fun onPreparing(position: Int) {
        songsIndex = position
        viewPager.currentItem    = position
        playPauseIcon.visibility = View.GONE
        progressBar.visibility   = View.VISIBLE

        when {
            songsIndex == 0 -> {
                previousIcon.alpha     = 0.5f
                previousIcon.isEnabled = false
                if(songsIndex == adapter.count-1) {
                    nextIcon.alpha     = 0.5f
                    nextIcon.isEnabled = false
                } else {
                    nextIcon.alpha     = 1f
                    nextIcon.isEnabled = true
                }
            }

            songsIndex == adapter.count-1 -> {
                nextIcon.alpha         = 0.5f
                nextIcon.isEnabled     = false
                previousIcon.alpha     = 1f
                previousIcon.isEnabled = true
            }
            else -> {
                previousIcon.alpha     = 1f
                previousIcon.isEnabled = true
                nextIcon.alpha         = 1f
                nextIcon.isEnabled     = true
            }
        }
    }

    override fun onContinueAudio() {}

    protected fun setupPlayer() {
        if (MusicPlayer.mService == null)
            serviceToken = MusicPlayer.bindToService(activity,mConnection)
        else {
            MusicPlayer.registerServicePlayerListener(this)
            MusicPlayer.setPlayList(songsList)
            MusicPlayer.setPlayListName(playListName)
            MusicPlayer.setPlayListId(playListId)
            MusicPlayer.setPlayerType(playerType)
            val currentSongs = MusicPlayer.getCurrentAudio()
            val details      = adapter.getSong(songsIndex)
            if(currentSongs?.id == details?.id)
                setPlayerState()
            else
                playNextSongs()
        }
    }

    private fun setPlayerState() {
        playing     = MusicPlayer.isPlaying()
        isShuffling = MusicPlayer.isShuffling()
        isRepeat    = MusicPlayer.isRepeating()
        paused      = !playing

        onTimeChanged(MusicPlayer.getCurrentPosition(), MusicPlayer.getDuration())

        playPauseIcon.setImageResource(if(playing) R.drawable.ic_pause_new else R.drawable.ic_play_new)

        if(isShuffling)
            shuffleIcon.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        else
            shuffleIcon.clearColorFilter()

        if(isRepeat)
            repeatIcon.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        else
            repeatIcon.clearColorFilter()

        Handler().postDelayed({
            adapter.currentFragment?.start()
        },1500)

    }

    protected open fun onPageSelected(position: Int) {}

    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, service: IBinder) {
            MusicPlayer.registerServicePlayerListener(this@BaseMusicFragment)
            MusicPlayer.setPlayList(songsList)
            MusicPlayer.setPlayListName(playListName)
            MusicPlayer.setPlayListId(playListId)
            MusicPlayer.setPlayerType(playerType)
            playNextSongs()
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            MusicPlayer.mService = null
            playing = false
            paused  = true
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MusicPlayer.unregisterServicePlayerListener(this)
        if (serviceToken != null) {
            MusicPlayer.unbindFromService(serviceToken)
            serviceToken = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        MusicPlayer.unregisterServicePlayerListener(this)
        if (serviceToken != null) {
            MusicPlayer.unbindFromService(serviceToken)
            serviceToken = null
        }
    }
}
