package com.gaanap.gaanapehchana.fragments

import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devbrackets.android.exomedia.AudioPlayer
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.SelectedOptionAdapter
import com.gaanap.gaanapehchana.ConstantsFlavor
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.enums.PlayerState
import com.gaanap.gaanapehchana.enums.PlayerState.PAUSE
import com.gaanap.gaanapehchana.enums.PlayerState.START
import com.gaanap.gaanapehchana.helper.SpaceItemDecoration
import com.gaanap.gaanapehchana.interfaces.PlayerListener
import com.gaanap.gaanapehchana.interfaces.ScoreView
import com.gaanap.gaanapehchana.models.RightOptionDetails
import com.gaanap.gaanapehchana.models.SongsProgress
import com.gaanap.gaanapehchana.models.response.ScoreResponse
import com.gaanap.gaanapehchana.presenter.ScoresPresenter
import com.gaanap.gaanapehchana.utility.*
import com.gaanap.gaanapehchana.view.LeaderboardActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_game_win.*

class GameWinFragment : BaseFragment(), PlayerListener,ScoreView {

    private var incorrect      = 0
    private var correct        = 0
    private var score          = 0
    private var previousScore  = 0
    private var gameId         = ""
    private var isWin          = false
    private var stopCallbacks  = false
    private val handler        = Handler()
    private var playerState    = PlayerState.INIT
    private var currentGame    = GameType.TMK
    private val disposable     = CompositeDisposable()
    private var scorePresenter = ScoresPresenter(this)
    private var gameColor      = Color.BLACK


    private lateinit var player     : AudioPlayer
    private lateinit var adapter    : SelectedOptionAdapter
    private lateinit var optionList : List<RightOptionDetails>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            currentGame   = it.getSerializable(GAME) as GameType
            score         = it.getInt(SCORE,0)
            gameId        = it.getString(GAME_ID) ?: ""
            optionList    = it.getParcelableArrayList(SELECTED_OPTION_LIST)
            isWin         = it.getBoolean(IS_WIN,false)
            previousScore = it.getInt(PREVIOUS_SCORE,-1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_game_win,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getDataFromIntent()
        setupData()
        setListener()
        setupPlayer()
    }


    override fun onResume() {
        super.onResume()
        if(playerState == PlayerState.AUTO_PAUSE) {
            playerState = START
            player.start()
            adapter.setPlayerState(playerState)
            stopCallbacks = false
            handler.post(runnable)
        }
    }

    private fun getDataFromIntent() {
        scorePresenter.setGameInfo(getUser() ?: return)

        when(currentGame) {

            GameType.ASN -> {
                disposable.add(scorePresenter.updateASNScore(getUser()?.user_id ?: return,score))
                appBar.setBackgroundResource(R.drawable.rect_gradient_asn)
                EventUtils.logGroupClickEvent(GAME_COMPLETION, GAME_CHOICE, ASN)
                gameColor = ContextCompat.getColor(context?: return,R.color.asn_bg_end_color)
            }

            GameType.BBG  -> {
                disposable.add(scorePresenter.updateBBGScore(getUser()?.user_id ?: return,score))
                appBar.setBackgroundResource(R.drawable.rect_gradient_bbg)
                EventUtils.logGroupClickEvent(GAME_COMPLETION, GAME_CHOICE,BBG)
                gameColor = ContextCompat.getColor(context?: return,R.color.bbg_bg_end_color)
            }

            else -> {
                appBar.setBackgroundResource(R.drawable.rect_gradient_tmk)
                winLabel.visibility = View.VISIBLE
                winLabel.text = if(isWin) getString(R.string.you_win) else getString(R.string.you_lose)
                if(previousScore == -1)
                    disposable.add(scorePresenter.updateTMKScore(getUser()?.user_id ?: return,score,gameId))
                val tmkListFragment = activity?.supportFragmentManager?.findFragmentByTag(TMK_LIST_FRAGMENT) as? TMKListFragment
                tmkListFragment?.updateGameScore(gameId,score)
                EventUtils.logGroupClickEvent(GAME_COMPLETION, GAME_CHOICE,TMK)
                gameColor = ContextCompat.getColor(context?: return,R.color.tmk_bg_end_color)
            }
        }
    }

    private fun setupData() {
        adapter = SelectedOptionAdapter(activity ?: return,optionList,this,gameColor)
        selectedOptionList.layoutManager = LinearLayoutManager(activity)
        selectedOptionList.adapter       = adapter
        selectedOptionList.setHasFixedSize(true)
        selectedOptionList.addItemDecoration(SpaceItemDecoration(dp2px(10)))
        (selectedOptionList.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        optionList.forEach { if(it.isRightOption) correct++ else incorrect++ }

        correctOption.text   = correct.toString()
        incorrectOption.text = incorrect.toString()
        totalScore.text      = score.toString()

        profileIcon.loadUrl(getUser()?.pic)

        if(adapter.itemCount <= 0)
            emptyView.visibility = View.VISIBLE
        else
            emptyView.visibility = View.GONE
    }

    private fun setListener() {
        shareIcon.setOnClickListener {

            if(PermissionUtils.checkExternalStoragePermission(context ?: return@setOnClickListener))
                shareScore(activity)
            else
                PermissionUtils.requestExternalStoragePermission(activity ?: return@setOnClickListener)
        }

        leaderboardIcon.setOnClickListener {
            EventUtils.logClickEvent(LEADERBOARD_BUTTON,"GameWin Screen")
            activity?.startActivity<LeaderboardActivity>()
        }
    }

    private fun setupPlayer() {
        player = AudioPlayer(context?: return)
        player.setAudioStreamType(AudioManager.STREAM_MUSIC)
        player.setOnPreparedListener(this::onPrepared)
        player.setOnErrorListener(this::onError)
        player.setOnCompletionListener(this::onCompletion)
    }

    private fun onPrepared() {
        player.start()
        playerState = START
        adapter.setPlayerState(playerState)
        stopCallbacks = false
        handler.postDelayed(runnable,100)
    }

    private fun onError(e: Exception): Boolean {
        Log.e(tag,":$e")
        return false
    }

    private fun onCompletion() {
        playerState = PlayerState.STOP
        adapter.setPlayerState(playerState)
        onStopHandlerCallbacks()
    }

    private val runnable = this::updateProgressBar
    private fun updateProgressBar() {
        if(stopCallbacks)
            return

        adapter.setProgress(SongsProgress(player.currentPosition,player.duration))
        handler.postDelayed(runnable, 1000)
    }

    private fun onPauseSongs() {
        when(playerState) {
            START -> {
                playerState = PAUSE
                player.pause()
                adapter.setPlayerState(playerState)
                onStopHandlerCallbacks()
            }

            PAUSE -> {
                playerState = START
                player.start()
                adapter.setPlayerState(playerState)
                stopCallbacks = false
                handler.post(runnable)
            }
            else -> {}
        }
    }

    override fun onSongsPlay(songsFile: String) {
        val clipUrl = ConstantsFlavor.SONGS_URL + songsFile
        player.reset()
        player.setDataSource(Uri.parse(clipUrl.replace("\\s+".toRegex(), "%20")))
        playerState = PlayerState.PREPARING
        player.prepareAsync()
    }

    override fun onSongsPause() {
        onPauseSongs()
    }

    override fun onSongsStop() {
        onStopHandlerCallbacks()
        player.stopPlayback()
    }

    private fun onStopHandlerCallbacks() {
        stopCallbacks = true
        handler.removeCallbacks(runnable)
    }


    override fun onResponse(response: ScoreResponse) {
        //totalScore.visibility = View.VISIBLE
    }

    override fun showProgress() {
        //progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        //progressBar?.visibility = View.GONE
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            EXTERNAL_STORAGE_PC -> {
                if (PermissionUtils.verifyPermissions(grantResults))
                    shareScore(activity)
                else
                    DialogUtils.openPermissionDenyDialog(context,getString(R.string.storage_permission_msg))
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if(player.isPlaying) {
            player.pause()
            playerState = PlayerState.AUTO_PAUSE
            onStopHandlerCallbacks()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
        onStopHandlerCallbacks()
        player.stopPlayback()
        player.release()
    }

    companion object {
        fun newInstance(intent: Intent) = GameWinFragment().apply {
            arguments = Bundle().apply {
                putString(GAME_ID,intent.getStringExtra(GAME_ID))
                putInt(SCORE,intent.getIntExtra(SCORE,0))
                putInt(PREVIOUS_SCORE,intent.getIntExtra(PREVIOUS_SCORE,-1))
                putSerializable(GAME,intent.getSerializableExtra(GAME))
                putParcelableArrayList(SELECTED_OPTION_LIST,intent.getParcelableArrayListExtra(SELECTED_OPTION_LIST))
                putBoolean(IS_WIN, intent.getBooleanExtra(IS_WIN,false))
            }
        }
    }
}
