package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.interfaces.RadioView
import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.presenter.RadioPresenter
import com.gaanap.gaanapehchana.rx.RxDisposable
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.fragment_create_playlist_dialog.*

class CreatePlayListDialog : KeyBoardListenerFragment(),RadioView {

    private var listener: ((BaseResponse) -> Unit)? = null

    private val presenter = RadioPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, STYLE_NORMAL)
        EventUtils.logClickEvent(MY_MUSIC_PL_CREATION)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_playlist_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }

    private fun setListener() {
        cancel.setOnClickListener { dismiss() }
        createPlaylist.setOnClickListener {
            val name = title.text.toString()
            when {
                name.isBlank() -> activity?.showToast(R.string.enter_valid_title)
                !isNetworkAvailable(activity?: return@setOnClickListener) -> activity?.showToast(R.string.connection_error)
                else -> {
                    val isPrivate = if (radioPrivate.isChecked) 1 else 0
                    RxDisposable.add(presenter.createPlayList(name, isPrivate, SessionManager.getUser(activity)?.user_id))
                }
            }
        }
    }

    override fun showProgress() {
        createPlaylist?.visibility = View.INVISIBLE
        progressBar?.visibility    = View.VISIBLE
    }

    override fun hideProgress() {
        createPlaylist?.visibility = View.VISIBLE
        progressBar?.visibility    = View.GONE
    }

    override fun onResponse(response: BaseResponse){
        hideProgress()
        activity?.showToast(response.message)
        listener?.invoke(response)
        dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        RxDisposable.clear()
    }

    companion object {

        @JvmStatic
        fun newInstance(listener: ((BaseResponse) -> Unit)?) = CreatePlayListDialog().apply {
            this.listener = listener
        }
    }

}