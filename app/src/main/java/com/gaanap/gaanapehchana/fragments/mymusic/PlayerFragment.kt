package com.gaanap.gaanapehchana.fragments.mymusic

import android.os.Bundle
import android.util.TypedValue
import android.view.View
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.PlayerType
import com.gaanap.gaanapehchana.fragments.BaseMusicFragment
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.services.MusicService
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_player.*

class PlayerFragment : BaseMusicFragment(), MusicService.PlayerServiceListener {

    companion object {
        fun newInstance(plName: String,plId:String,position: Int,list: ArrayList<PlayList>,isMyPlayList: Boolean) = PlayerFragment().apply {
            arguments = Bundle().apply {
                putString(PLAY_LIST_NAME,plName)
                putString(PLAY_LIST_ID,plId)
                putInt(POSITION,position)
                putBoolean(IS_MY_PLAY_LIST,isMyPlayList)
                putParcelableArrayList(SONG_LIST,list)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        playerType = PlayerType.MUSIC
        arguments?.let {
            playListId   = it.getString(PLAY_LIST_ID)   ?: ""
            playListName = it.getString(PLAY_LIST_NAME) ?: getString(R.string.gaanap_radio)
            position     = it.getInt(POSITION,0)
            isMyPlayList = it.getBoolean(IS_MY_PLAY_LIST,false)
            songsList    = it.getParcelableArrayList<PlayList>(SONG_LIST)
        }
    }

    override fun setupViewPager() {
        super.setupViewPager()
        val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (40 * 2).toFloat(), resources.displayMetrics).toInt()
        viewPager.pageMargin    = -margin
        viewPager.clipToPadding = false

        backIcon.visibility = View.VISIBLE
    }

    override fun setData() {
        super.setData()
        setupPlayer()
    }
}
