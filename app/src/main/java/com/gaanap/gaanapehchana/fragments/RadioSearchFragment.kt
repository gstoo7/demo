package com.gaanap.gaanapehchana.fragments

import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.TypedValue
import android.view.*
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.helper.DividerItemDecoration
import com.gaanap.gaanapehchana.interfaces.RadioView
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.models.response.RadioSearchResponse
import com.gaanap.gaanapehchana.presenter.RadioPresenter
import com.gaanap.gaanapehchana.utility.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_radio_search.*
import com.gaanap.gaanapehchana.adapter.PlayListSongsAdapter
import com.gaanap.gaanapehchana.fragments.dialogs.SongInfoDialog
import com.gaanap.gaanapehchana.fragments.mymusic.PlayerFragment
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.bottomsheet_playlist_option.*

class RadioSearchFragment : BaseFragment(),RadioView,SearchView.OnQueryTextListener {

    private val presenter    = RadioPresenter(this)
    private val disposable   = CompositeDisposable()
    private val querySubject = PublishSubject.create<String>()

    private val adapter by lazy {
        PlayListSongsAdapter(activity,arrayListOf(),this::onSongsClickListener,this::performMoreOption)
    }

    private var searchQuery = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_radio_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecycleView()
        setupSearchView()
        backIcon.setOnClickListener { activity?.onBackPressed() }
        val userId = SessionManager.getUser(context).user_id ?: return
        disposable.add(presenter.searchQuery(querySubject,userId))
    }

    private fun setRecycleView() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter       = adapter
        recyclerView.addItemDecoration(DividerItemDecoration(context))
    }

    private fun setupSearchView() {
        searchView.setIconifiedByDefault(false)
        searchView.setOnQueryTextListener(this)
        searchView.isIconified = false
        searchView.queryHint   = getString(R.string.search_song_movie)
        val searchAutoComplete = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as? SearchView.SearchAutoComplete
        searchAutoComplete?.setTextSize(TypedValue.COMPLEX_UNIT_SP,14f)

    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        onQueryTextChange(query?: "")
        return true
    }

    override fun onQueryTextChange(newText: String): Boolean {
        searchQuery = newText
        if(newText.isBlank()) {
            adapter.clear()
        } else
            querySubject.onNext(newText)

        return true
    }

    override fun showProgress() {}
    override fun hideProgress() {}

    override fun onResponse(response: RadioSearchResponse) {
        hideProgress()
        when {
            searchQuery.isBlank() -> return
            response.error == 0   -> adapter.addAll(response.data)
            //else                  -> showToast(getString(R.string.no_records_found),Toast.LENGTH_SHORT)
        }

    }

    override fun onError(throwable: Throwable) {
        hideProgress()
        val error = ErrorUtils.parseErrorResponse(throwable)
        activity?.showToast(error?.message ?: getString(R.string.something_wrong_error))
    }

    private fun onSongsClickListener(position: Int,list: ArrayList<PlayList>) {
        loadFragment(PlayerFragment.newInstance(getString(R.string.gaanap_radio),"",position,list,false), PLAYER_FRAGMENT)
    }

    private fun performMoreOption(info: PlayList,position: Int) {
        val dialogView = layoutInflater.inflate(R.layout.bottomsheet_playlist_option,null)
        val dialog     = BottomSheetDialog(context ?: return)
        dialog.setContentView(dialogView)
        dialog.setCancelable(true)
        dialog.viewInfo.setOnClickListener {
            SongInfoDialog.newInstance(info).show(childFragmentManager, DIALOG)
            dialog.dismiss()
        }
        dialog.delete.visibility = View.GONE
        dialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

}