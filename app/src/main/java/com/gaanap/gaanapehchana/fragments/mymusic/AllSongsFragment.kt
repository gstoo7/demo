package com.gaanap.gaanapehchana.fragments.mymusic

import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.RadioAllSongsAdapter
import com.gaanap.gaanapehchana.fragments.BaseFragment
import com.gaanap.gaanapehchana.helper.PaginationScrollListener
import com.gaanap.gaanapehchana.interfaces.RadioView
import com.gaanap.gaanapehchana.models.event.PlayListEvent
import com.gaanap.gaanapehchana.models.response.Channel
import com.gaanap.gaanapehchana.models.response.ChannelsResponse
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.presenter.RadioPresenter
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.utility.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_all_songs.*

class AllSongsFragment : BaseFragment(),RadioView {

    private val presenter     = RadioPresenter(this)
    private val adapter       = RadioAllSongsAdapter(arrayListOf(),this::onMoreClickListener,this::onSongsClickListener)
    private val disposable    = CompositeDisposable()
    private val limit         = 10
    private var currentPage   = 1
    private var totalPages    = 0
    private var isPageLoading = false
    private var lastPage      = false

    private var currentView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseDisposable.add(RxBus.listen(PlayListEvent::class.java).subscribe ({
            adapter.updateItem(it.playListId,it.songId)
        },Throwable::printStackTrace))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if(currentView == null) inflater.inflate(R.layout.fragment_all_songs, container, false)
        else currentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(currentView == null) {
            currentView = view
            setupCategoryList()
            loadPage()
        }
    }

    private fun setupCategoryList() {
        ViewCompat.setNestedScrollingEnabled(categoryList,false)
        val linearLayoutManager    = LinearLayoutManager(activity)
        categoryList.layoutManager = linearLayoutManager
        categoryList.adapter       = adapter
        categoryList.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override val totalPageCount: Int
                get() = totalPages

            override val isLastPage: Boolean
                get() = lastPage

            override val isLoading: Boolean
                get() = isPageLoading

            override fun loadMoreItems() {
                isPageLoading = true
                currentPage += 1
                loadPage()
            }

        })
    }

    private fun loadPage() {
        disposable.add(presenter.getAllSongs(currentPage,limit))
    }

    override fun showProgress() {
        if(currentPage == 1)
            progressBar?.visibility = View.VISIBLE

        isPageLoading = true
    }

    override fun hideProgress() {
        if(currentPage > 1)
            adapter.removeLoadingFooter()

        isPageLoading = false
        progressBar?.visibility = View.GONE
    }

    override fun onResponse(response: ChannelsResponse) {
        hideProgress()
        totalPages  = response.pagination.last_page
        adapter.addAll(response.data.filter { it.songs?.isEmpty() == false })
        if (currentPage < totalPages) adapter.addLoadingFooter() else lastPage = true
    }

    override fun onNetworkError() {
        if(adapter.itemCount <= 0)
            connectionError?.visibility = View.VISIBLE
        else
            activity?.showToast(R.string.connection_error)
    }

    private fun onMoreClickListener(channel: Channel) {
        if(!isNetworkAvailable(activity ?: return)) {
            activity?.showToast(R.string.connection_error)
            return
        }
        EventUtils.logGroupClickEvent(MOST_SELECTED,CHANNEL,channel.radio_name)
        loadFragment(PlayListSongsFragment.newInstance(channel.id,channel.radio_name), PLAYLIST_FRAGMENT)
    }


    private fun onSongsClickListener(position: Int,channel: Channel,list: ArrayList<PlayList>) {
        if(!isNetworkAvailable(activity ?: return)) {
            activity?.showToast(R.string.connection_error)
            return
        }
        EventUtils.logGroupClickEvent(MOST_SELECTED,CHANNEL,channel.radio_name)
        loadFragment(PlayerFragment.newInstance(channel.radio_name,channel.id,position,list,false), PLAYER_FRAGMENT)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.clear()
    }


    companion object {
        @JvmStatic
        fun newInstance() = AllSongsFragment()
    }
}
