package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.interfaces.ForgotPasswordView
import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.models.response.ErrorResponse
import com.gaanap.gaanapehchana.presenter.ForgotPasswordPresenter
import com.gaanap.gaanapehchana.utility.getDeviceWidth
import com.gaanap.gaanapehchana.utility.showToast
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_reset_password.*

class ResetPasswordDialog : KeyBoardListenerFragment(), ForgotPasswordView {

    private val presenter  = ForgotPasswordPresenter(this)
    private val disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reset_password,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        resetPassword.setOnClickListener {
            val newPassword    = edtNewPassword.text.toString()
            val retypePassword = edtRetypePassword.text.toString()
            val code           = edtOTP.text.toString()

            if(verification(newPassword,retypePassword,code))
                disposable.add(presenter.resetPassword(newPassword,retypePassword,code))
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout((getDeviceWidth() * .9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }

    override fun showProgress() {
        resetPassword.visibility = View.GONE
        progressBar.visibility   = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility   = View.GONE
        resetPassword?.visibility = View.VISIBLE
    }

    override fun onResponse(response: BaseResponse) {
        activity?.showToast(R.string.password_reset_successfully)
        dismiss()
    }

    override fun onError(error: ErrorResponse?) {
        if(error?.status_code == 422)
            activity?.showToast(R.string.pls_enter_your_valid_code)
        else
            activity?.showToast(R.string.something_wrong_error)
    }

    private fun verification(newPassword: String,retypePassword: String,otp: String) : Boolean {

        return when {
            newPassword.isBlank() -> {
                activity?.showToast(R.string.enter_new_password)
                false
            }

            retypePassword.isBlank() -> {
                activity?.showToast(R.string.enter_retype_password)
                false
            }

            newPassword != retypePassword -> {
                activity?.showToast(R.string.retype_not_match)
                false
            }

            otp.isBlank() -> {
                activity?.showToast(R.string.enter_your_code)
                false
            }

            otp.length != 4 -> {
                activity?.showToast(R.string.pls_enter_your_valid_code)
                false
            }

            else -> true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}
