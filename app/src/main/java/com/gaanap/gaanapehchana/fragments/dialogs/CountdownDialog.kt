package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.utility.DIALOG
import com.gaanap.gaanapehchana.utility.getDeviceWidth
import kotlinx.android.synthetic.main.fragment_countdown.*

class CountdownDialog : BaseDialogFragment() {

    private var totalTime = 3
    private var pauseTime = 3

    private lateinit var listener:() -> Unit

    private var handler : Handler? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_countdown, container, false)
    }

    override fun onResume() {
        super.onResume()
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setCancelable(false)
        dialog?.window?.setLayout((getDeviceWidth() * .8).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        timer.text = totalTime.toString()
        handler    = Handler()
        handler?.postDelayed(runnable,1000)
    }

    private val runnable = object : Runnable {
        override fun run() {
            pauseTime  = --totalTime
            if(pauseTime > 0) {
                timer?.text = pauseTime.toString()
                handler?.postDelayed(this,1000)
            } else {
                listener.invoke()
                dismissAllowingStateLoss()
            }
        }

    }

    override fun onPause() {
        super.onPause()
        handler?.removeCallbacks(runnable)
        handler = null
    }

    companion object {
        fun show(context: AppCompatActivity,listener:() -> Unit) {
            val fragment = CountdownDialog()
            fragment.listener  = listener
            context.fragmentManager.beginTransaction().add(fragment,DIALOG).commitAllowingStateLoss()
        }
    }

}
