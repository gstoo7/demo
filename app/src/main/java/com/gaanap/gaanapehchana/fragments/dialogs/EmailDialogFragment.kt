package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.models.request.RegisterRequest
import com.gaanap.gaanapehchana.utility.ValidationUtils
import kotlinx.android.synthetic.main.fragment_email_dialog.*

class EmailDialogFragment : DialogFragment() {

    private var registerRequest: RegisterRequest? = null

    private lateinit var listener: (RegisterRequest) -> Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, STYLE_NORMAL)
        isCancelable = false
        registerRequest = arguments?.getParcelable(KEY_REQUEST)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_email_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        done.setOnClickListener {
            val email = edtEmailId.text.toString()
            if (ValidationUtils.isValidEmail(email)) {
                registerRequest?.email = email
                listener.invoke(registerRequest!!)
                dismiss()
            } else {
                edtEmailId.requestFocus()
                edtEmailId.error = getString(R.string.enter_valid_email_address)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }

    companion object {
        const val KEY_REQUEST = "request"

        fun newInstance(registerRequest: RegisterRequest, listener: (RegisterRequest) -> Unit): EmailDialogFragment {
            val fragment = EmailDialogFragment()
            val args = Bundle()
            args.putParcelable(KEY_REQUEST, registerRequest)
            fragment.arguments = args
            fragment.listener  = listener
            return fragment
        }
    }
}
