package com.gaanap.gaanapehchana.fragments.leaderboard

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.enums.ScoreType
import com.gaanap.gaanapehchana.fragments.BaseFragment
import com.gaanap.gaanapehchana.utility.GAME
import kotlinx.android.synthetic.main.fragment_master_leaderbord.*

class LeaderboardFragment : BaseFragment() {

    private var gameType  = GameType.NONE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gameType  = arguments?.getSerializable(GAME) as GameType
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_master_leaderbord, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewPager()
    }

    private fun setupViewPager() {
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = MyPagerAdapter(activity?.supportFragmentManager!!)
        tabs.setViewPager(viewPager)
    }


    inner class MyPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
        private val pageCount = 3
        private val tabTitles = arrayOf("7 Days", "Month", "All Time")
        private val scoreType = arrayOf(ScoreType.WEEKLY,ScoreType.MONTHLY,ScoreType.LIFE_TIME)

        override fun getCount() = pageCount
        override fun getPageTitle(position: Int) = tabTitles[position]
        override fun getItem(position: Int)      = ScoreFragment.newInstance(gameType,scoreType[position])
    }


    companion object {

        @JvmStatic
        fun newInstance(gameType: GameType) = LeaderboardFragment().apply {
            arguments = Bundle().apply {
                putSerializable(GAME,gameType)
            }
        }
    }

}
