package com.gaanap.gaanapehchana.fragments.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.enums.ScoreType
import com.gaanap.gaanapehchana.firebasehelper.model.GameInfo
import com.gaanap.gaanapehchana.firebasehelper.model.LeaderboardModel
import com.gaanap.gaanapehchana.utility.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.Query
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_gleaderboard_dialog.*


class GLeaderboardDialog : DialogFragment() {

    private var userId    = ""
    private var scoreType = ScoreType.WEEKLY
    private var gameType  = GameType.TMK

    private var leaderboard: LeaderboardModel? = null

    private var query: Query?  = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE,R.style.DialogTheme)
        arguments?.let {
            leaderboard = it.getParcelable(GAME_PLAYED_INFO)
            gameType    = it.getSerializable(GAME)  as GameType
            scoreType   = it.getSerializable(SCORE) as ScoreType
        }

        userId = leaderboard?.userId ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gleaderboard_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        totalScore.text = leaderboard?.gameInfo?.totalScore?.toString() ?: "0"
        profileIcon.loadUrl(leaderboard?.gameInfo?.profileIcon ?: "")
        buildQuery()
        closeIcon.setOnClickListener { dismiss() }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawableResource(R.drawable.rect_white_with_bottom_corners_20dp)
    }

    private fun buildQuery() {
        progressBar?.visibility = View.VISIBLE

        query = when (gameType) {
            GameType.TMK -> {

                title.text = getString(R.string.tmk_title)
                headerView.setBackgroundResource(R.drawable.rect_gradient_tmk)

                when(scoreType) {
                    ScoreType.WEEKLY    -> FirebaseNodeRef.getWeeklyTMKScoreRef(userId)
                    ScoreType.MONTHLY   -> FirebaseNodeRef.getMonthlyTMKScoreRef(userId)
                    ScoreType.LIFE_TIME -> FirebaseNodeRef.getLifetimeTMKScoreRef(userId)
                }
            }

            GameType.BBG -> {

                title.text = getString(R.string.bbg_title)
                headerView.setBackgroundResource(R.drawable.rect_gradient_bbg)

                when(scoreType) {
                    ScoreType.WEEKLY    -> FirebaseNodeRef.getWeeklyBBGScoreRef(userId)
                    ScoreType.MONTHLY   -> FirebaseNodeRef.getMonthlyBBGScoreRef(userId)
                    ScoreType.LIFE_TIME -> FirebaseNodeRef.getLifetimeBBGScoreRef(userId)
                }
            }

            else -> {

                title.text = getString(R.string.aursunao_title)
                headerView.setBackgroundResource(R.drawable.rect_gradient_asn)

                when(scoreType) {
                    ScoreType.WEEKLY    -> FirebaseNodeRef.getWeeklyASNScoreRef(userId)
                    ScoreType.MONTHLY   -> FirebaseNodeRef.getMonthlyASNScoreRef(userId)
                    ScoreType.LIFE_TIME -> FirebaseNodeRef.getLifetimeASNScoreRef(userId)
                }
            }
        }

        query?.keepSynced(true)
        query?.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                activity?.showToast(R.string.something_wrong_error)
                progressBar?.visibility = View.GONE
            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                try {
                    val scoreInfo = dataSnapshot?.getValue(GameInfo::class.java)
                    totalGames.text = scoreInfo?.totalGame?.toString() ?: "0"
                    score.text = scoreInfo?.totalScore?.toString() ?: "0"
                    progressBar?.visibility = View.GONE
                } catch (e:Exception) {
                    activity?.showToast(R.string.something_wrong_error)
                }
            }

        })

    }

    companion object {

        @JvmStatic
        fun newInstance(leaderboard: LeaderboardModel,gameType: GameType,scoreType: ScoreType) =
                GLeaderboardDialog().apply {
                    arguments = Bundle().apply {
                        putParcelable(GAME_PLAYED_INFO,leaderboard)
                        putSerializable(GAME,gameType)
                        putSerializable(SCORE,scoreType)
                    }
                }
    }
}
