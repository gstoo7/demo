package com.gaanap.gaanapehchana.fragments.dialogs

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.utility.getDeviceWidth
import kotlinx.android.synthetic.main.fragment_clips_loading_dialog.*

class ClipsLoadingDialog : BaseDialogFragment() {

    private var listener: (()-> Unit)? = null

    var isCanceledOnTouchOutside = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_clips_loading_dialog, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinner?.startAnimation()
    }

    override fun onResume() {
        super.onResume()
        dialog?.setCanceledOnTouchOutside(isCanceledOnTouchOutside)
        dialog?.setCancelable(isCanceledOnTouchOutside)
        dialog?.window?.setLayout((getDeviceWidth() * .8).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    fun changeProgress(progress:Int) {
        if(progress > 0)
            activity?.runOnUiThread { spinner?.clearAnimation() }

        progressbar?.progress = progress
    }

    fun showLoading(appCompatActivity: AppCompatActivity) {
        if(isAdded)
            return
        if(!isVisible)
            show(appCompatActivity.fragmentManager, null)
    }

    fun hideLoading(time: Long = 100) {
        Handler().postDelayed({
            try {
                if (activity == null || activity?.isFinishing == true || activity?.isDestroyed == true)
                    return@postDelayed

                if (isVisible) dismissAllowingStateLoss()
            } catch (e: Exception) {
                Log.e("error","$e")
            }
        },time)

    }

    fun setListener(listener: ()-> Unit) {
        this.listener = listener
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
        listener?.invoke()
    }
}
