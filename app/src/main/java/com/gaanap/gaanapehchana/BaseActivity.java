package com.gaanap.gaanapehchana;

import android.annotation.SuppressLint;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.TextView;

import com.gaanap.gaanapehchana.models.User;
import com.gaanap.gaanapehchana.utility.AppUtilsKt;

import io.reactivex.disposables.CompositeDisposable;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    private User mUser;

    protected CompositeDisposable baseDisposable = new CompositeDisposable();

    protected void setupToolbar() {
        setupToolbar(null);
    }

    protected void setupToolbar(@StringRes int title) {
        setupToolbar(getString(title));
    }

    protected void setupToolbar(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        if(!TextUtils.isEmpty(title)) {
            TextView text = toolbar.findViewById(R.id.toolbar_title);
            text.setText(title);
        }
        setSupportActionBar(toolbar);
    }

    public boolean isNetworkAvailable() {
        return AppUtilsKt.isNetworkAvailable(this);
    }

    protected User getCurrentUser() {
        if(mUser == null)
            mUser = SessionManager.getUser(this);
        return mUser;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        baseDisposable.clear();
    }
}
