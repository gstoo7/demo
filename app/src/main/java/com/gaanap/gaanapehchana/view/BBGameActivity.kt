package com.gaanap.gaanapehchana.view

import android.animation.*
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.TextView

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.enums.PlayerState
import com.gaanap.gaanapehchana.fragments.dialogs.CountdownDialog
import com.gaanap.gaanapehchana.models.common_response.ClipOptions
import com.gaanap.gaanapehchana.models.common_response.Scoring
import com.gaanap.gaanapehchana.models.response.BBGameResponse
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.content_bbgame.*
import kotlinx.android.synthetic.main.view_for_game_animation.view.*

class BBGameActivity : BaseGameActivity() {

    private var mResponse : BBGameResponse? = null
    private var isBonus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventUtils.logTimeEvent(MOST_TIME_SPENT)
        EventUtils.logTimeEvent(TIME_OF_GAME_IN_BETWEEN_LEFT)
        setContentView(R.layout.activity_bbgame)
        setupToolbar(getString(R.string.bbg_title))
        disposable.add(RxBus.listenClipsResponse(BBGameResponse::class.java).subscribe(this::onResponse,Throwable::printStackTrace))
    }

    override fun setupView() {
        super.setupView()
        gameType = GameType.BBG
        bbgOptionList.layoutManager     = LinearLayoutManager(this)
        bbgOptionList.adapter           = adapter
        bbgOptionList.setHasFixedSize(true)

    }

    private fun onResponse(response: BBGameResponse) {
        mResponse      = response
        totalClips     = mResponse?.clips?.size ?: 0
        totalTime      = (mResponse?.bbgLength!! * SECONDS * MILLI_SECONDS).toLong()
        lastPauseTime  = totalTime
        timer.text     = getDurationString(totalTime)
        gameScore.text = score.toString()
        setNewClip()
        CountdownDialog.show(this,this::onPlaySongs)
    }

    private fun setNewClip() {
        currentClips = mResponse?.clips?.get(clipIndex)
        val currentLevel = currentClips?.details?.clipLevel ?: ""
        level.setText(currentLevel)
        val currentTV = level.currentView as? TextView
        currentTV?.setTextColor(Color.parseColor(levelColors[currentLevel.toLowerCase()]))
        setClipsScore(mResponse?.scoring,currentLevel)
        adapter.updateGameList(currentClips?.options ?: return)

        if(mResponse?.scoring?.bonus == 1) {
            diamondIcon.setImageResource(R.drawable.ic_jewels)
            clipScore.setTextColor(ContextCompat.getColor(this,R.color.white))
        } else {
            diamondIcon.setImageResource(R.drawable.ic_star_24dp)
            clipScore.setTextColor(ContextCompat.getColor(this,R.color.purple))
        }
        bgView.clearAnimation()
        animateBackground(bgView)
    }

    private fun setClipsScore(score: Scoring?, clipName: String) {
        val addBonus: Int = if(mResponse?.scoring?.bonus == 1) 2 else 1
        clipScore.text = when(clipName.toLowerCase()) {
            "sa" -> {
                clipScore.tag = score?.sa
                (addBonus *(score?.sa?.toIntOrNull() ?: 0)).toString()
            }
            "re" -> {
                clipScore.tag = score?.re
                (addBonus *(score?.re?.toIntOrNull() ?: 0)).toString()
            }
            "ga" -> {
                clipScore.tag = score?.ga
                (addBonus *(score?.ga?.toIntOrNull() ?: 0)).toString()
            }
            "ma" -> {
                clipScore.tag = score?.ma
                (addBonus *(score?.ma?.toIntOrNull() ?: 0)).toString()
            }
            "pa" -> {
                clipScore.tag = score?.pa
                (addBonus *(score?.pa?.toIntOrNull() ?: 0)).toString()
            }
            else -> "0"
        }
    }

    override fun onCompletion() {
        startupNewData()
    }

    override fun updateProgressBar() {
        if(playerState == PlayerState.INIT)
            return
        progressbar.progress = player.currentPosition.toInt()
        isBonus = mResponse?.scoring?.bonus == 1 && player.currentPosition < player.duration/2

        if(isBonus) {
            diamondIcon.setImageResource(R.drawable.ic_jewels)
            clipScore.setTextColor(ContextCompat.getColor(this,R.color.white))
        } else {
            diamondIcon.setImageResource(R.drawable.ic_star_24dp)
            clipScore.setTextColor(ContextCompat.getColor(this,R.color.purple))
            clipScore.text = clipScore.tag?.toString() ?: ""
            bgView.clearAnimation()
        }

        handler.postDelayed(runnable, 100)
    }

    override fun onGameReset() {
        super.onGameReset()
        clipIndex = -1
        gameScore.text = score.toString()
        startupNewData()
    }

    override fun onSelectAnswer(isAnswerRight: Boolean,option: ClipOptions) {
        val currentClipScore  = clipScore.text.toString().toInt()
        addSongInfo(currentClips?.songDetails,currentClips?.details?.clipLevel,currentClipScore,isAnswerRight,isBonus)
        if(isAnswerRight) {
            score += currentClipScore
            gameScore.text = score.toString()

            if(isBonus)
                animateView()

            applyRotation(starIcon)
        }

        startupNewData()
    }

    private fun animateView() {
        val location = IntArray(2)
        diamondIcon.getLocationOnScreen(location)
        val view = layoutInflater.inflate(R.layout.view_for_game_animation,rootView,false)
        view.bonusIcon.setImageDrawable(diamondIcon.drawable)
        view.bonusScore.text = clipScore.text.toString()
        view.x = location[0].toFloat()
        view.y = (location[1] - diamondIcon.height).toFloat()
        rootView.addView(view)
        startAnimation(view)
    }

    private fun startAnimation(view: View) {
        view.animate()
                .translationX(gameScore.left.toFloat())
                .translationY(gameScore.top.toFloat())
                .setDuration(700)
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}
                    override fun onAnimationCancel(animation: Animator?) {}
                    override fun onAnimationStart(animation: Animator?) {}
                    override fun onAnimationEnd(animation: Animator?) {
                        rootView.removeView(view)
                    }
                })

    }

    private fun animateBackground(view: View) {
        val scale = ScaleAnimation(1f,1.2f,1f,1.2f,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        scale.duration    = 500
        scale.repeatCount = Animation.INFINITE
        scale.repeatMode  = Animation.REVERSE
        view.startAnimation(scale)
    }

    private fun startupNewData() {
        resetPlayer()
        clipIndex += 1

        if (clipIndex >= totalClips) {
            onGameOver()
        } else {
            setNewClip()
            onPlaySongs()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        BBGClipsDownloadService.stopService(this)
        EventUtils.logGroupClickEvent(MOST_TIME_SPENT,GAME, BBG)
        if(!isGameOver) {
            EventUtils.logGroupClickEvent(GAME_IN_BETWEEN_LEFT, GAME_CHOICE, BBG)
            EventUtils.logGroupClickEvent(TIME_OF_GAME_IN_BETWEEN_LEFT, GAME, BBG)
        }
    }
}
