package com.gaanap.gaanapehchana.view

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.fragments.leaderboard.LeaderboardFragment
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_leaderboard.*


class LeaderboardActivity : BaseActivity(),AdapterView.OnItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)
        setupToolbar()
        setUpMenu()
        getDataFromIntent()
        setListener()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedLabel.text = parent?.selectedItem as String
        when (position) {
           0 ->  replaceFragment(GameType.NONE , "MasterLeaderboard")
           1 ->  replaceFragment(GameType.TMK  , "TMKLeaderboard")
           2 ->  replaceFragment(GameType.BBG  , "BBGLeaderboard")
           3 ->  replaceFragment(GameType.ASN  , "ASNLeaderboard")
        }
    }

    private fun getDataFromIntent() {
        val currentGame = intent?.getSerializableExtra(GAME) as? GameType ?: GameType.NONE
        when(currentGame) {
            GameType.TMK -> spinner.setSelection(1)
            GameType.BBG -> spinner.setSelection(2)
            GameType.ASN -> spinner.setSelection(3)
            else         -> spinner.setSelection(0)
        }
    }

    private fun setUpMenu() {
        spinner.onItemSelectedListener = this
        spinner.adapter = ArrayAdapter<String>(this,R.layout.item_leaderboard_menu_adapter,resources.getStringArray(R.array.leaderboard))
    }

    private fun setListener() {
        closeIcon.setOnClickListener { onBackPressed() }
        shareIcon.setOnClickListener {
            if(PermissionUtils.checkExternalStoragePermission(this))
                shareScore(this)
            else
                PermissionUtils.requestExternalStoragePermission(this)
        }
    }

    private fun replaceFragment(gameType: GameType,tag: String) {
        replaceFragment(R.id.container, LeaderboardFragment.newInstance(gameType),tag)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            EXTERNAL_STORAGE_PC -> {
                if (PermissionUtils.verifyPermissions(grantResults))
                    shareScore(this)
                else
                    DialogUtils.openPermissionDenyDialog(this,getString(R.string.storage_permission_msg))
            }
        }
    }
}