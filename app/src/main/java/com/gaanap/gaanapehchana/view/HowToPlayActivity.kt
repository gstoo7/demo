package com.gaanap.gaanapehchana.view

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.ImageView
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_how_to_play.*

class HowToPlayActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_how_to_play)
        backIcon.setOnClickListener { finish() }
        val gameType = intent.getSerializableExtra(GAME) as GameType
        when(gameType) {
            GameType.TMK -> {
                setupToolbar(R.string.how_to_play_tmk)
                layoutInflater.inflate(R.layout.content_how_to_play_tmk,container,true)
                loadImage(R.drawable.how_to_play_tmk)
                EventUtils.logGroupClickEvent(HOW_TO_PLAY,GAME,getString(R.string.tmk_title))
            }

            GameType.BBG -> {
                setupToolbar(R.string.how_to_play_bbg)
                layoutInflater.inflate(R.layout.content_how_to_play_bbg,container,true)
                loadImage(R.drawable.how_to_play_bbg)
                EventUtils.logGroupClickEvent(HOW_TO_PLAY,GAME,getString(R.string.bbg_title))
            }

            else -> {
                setupToolbar(R.string.how_to_play_asn)
                layoutInflater.inflate(R.layout.content_how_to_play_asn,container,true)
                loadImage(R.drawable.how_to_play_asn)
                EventUtils.logGroupClickEvent(HOW_TO_PLAY,GAME,getString(R.string.aursunao_title))
            }
        }
    }

    private fun loadImage(resId: Int) {
        val imageView = findViewById<ImageView>(R.id.imageIcon)
        imageView.setImageBitmap(decodeSampledBitmapFromResource(resId, getDeviceWidth(), getDeviceHeight()))
    }

    private fun decodeSampledBitmapFromResource(resId: Int, reqWidth: Int, reqHeight: Int): Bitmap {
        // First decode with inJustDecodeBounds=true to check dimensions
        return BitmapFactory.Options().run {
            inJustDecodeBounds = true
            BitmapFactory.decodeResource(resources, resId, this)

            // Calculate inSampleSize
            inSampleSize = calculateInSampleSize(this, reqWidth, reqHeight)

            // Decode bitmap with inSampleSize set
            inJustDecodeBounds = false

            BitmapFactory.decodeResource(resources, resId, this)
        }
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {

        val (height: Int, width: Int) = options.run { outHeight to outWidth }
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val halfHeight: Int = height / 2
            val halfWidth: Int  = width / 2

            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }
}
