package com.gaanap.gaanapehchana.view

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.helper.DividerItemDecoration
import com.gaanap.gaanapehchana.interfaces.RadioView
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.models.response.RadioSearchResponse
import com.gaanap.gaanapehchana.presenter.RadioPresenter
import com.gaanap.gaanapehchana.utility.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_radio_search.*
import com.gaanap.gaanapehchana.adapter.PlayListSongsAdapter
import com.gaanap.gaanapehchana.fragments.dialogs.SongInfoDialog
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.bottomsheet_playlist_option.*

class RadioSearchActivity : BaseActivity(),RadioView,SearchView.OnQueryTextListener {

    private val presenter    = RadioPresenter(this)
    private val disposable   = CompositeDisposable()
    private val querySubject = PublishSubject.create<String>()

    private val adapter by lazy {
        PlayListSongsAdapter(this,arrayListOf(),this::onSongsClickListener,this::performMoreOption)
    }

    private var searchQuery = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_radio_search)
        setupToolbar()
        setRecycleView()
        val userId = SessionManager.getUser(this).user_id ?: return
        disposable.add(presenter.searchQuery(querySubject,userId))
    }

    private fun setRecycleView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter       = adapter
        recyclerView.addItemDecoration(DividerItemDecoration(this))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val item = menu.findItem(R.id.action_filter_search)
        val searchView = item.actionView as SearchView
        searchView.setIconifiedByDefault(false)
        searchView.setOnQueryTextListener(this)
        searchView.isIconified = false
        searchView.queryHint   = getString(R.string.search_song_movie)

        val searchAutoComplete = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as? SearchView.SearchAutoComplete
        searchAutoComplete?.setTextSize(TypedValue.COMPLEX_UNIT_SP,14f)

        item?.setOnActionExpandListener(object : MenuItem.OnActionExpandListener{
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                finish()
                return false
            }

        })

        item.expandActionView()

        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val item = menu.findItem(R.id.action_filter_search)
        item.isVisible = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        onQueryTextChange(query?: "")
        return true
    }

    override fun onQueryTextChange(newText: String): Boolean {
        searchQuery = newText
        if(newText.isBlank()) {
            adapter.clear()
        } else
            querySubject.onNext(newText)

        return true
    }

    override fun showProgress() {}
    override fun hideProgress() {}

    override fun onResponse(response: RadioSearchResponse) {
        hideProgress()
        when {
            searchQuery.isBlank() -> return
            response.error == 0   -> adapter.addAll(response.data)
            //else                  -> showToast(getString(R.string.no_records_found),Toast.LENGTH_SHORT)
        }

    }

    override fun onError(throwable: Throwable) {
        hideProgress()
        val error = ErrorUtils.parseErrorResponse(throwable)
        showToast(error?.message ?: getString(R.string.something_wrong_error))
    }

    private fun onSongsClickListener(position: Int,list: ArrayList<PlayList>) {
        startIntentActivity<HomeActivity>(Intent().apply {
            putExtra(POSITION,position)
            putExtra(SONG_LIST,list)
        })
    }

    private fun performMoreOption(info: PlayList,position: Int) {
        val dialogView = layoutInflater.inflate(R.layout.bottomsheet_playlist_option,null)
        val dialog     = BottomSheetDialog(this)
        dialog.setContentView(dialogView)
        dialog.setCancelable(true)
        dialog.viewInfo.setOnClickListener {
            SongInfoDialog.newInstance(info).show(supportFragmentManager, DIALOG)
            dialog.dismiss()
        }
        dialog.delete.visibility = View.GONE
        dialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

}