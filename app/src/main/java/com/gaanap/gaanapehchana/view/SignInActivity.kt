package com.gaanap.gaanapehchana.view

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.fragments.dialogs.ForgotPasswordDialog
import com.gaanap.gaanapehchana.interfaces.LoginView
import com.gaanap.gaanapehchana.models.response.ErrorResponse
import com.gaanap.gaanapehchana.models.response.LoginResponse
import com.gaanap.gaanapehchana.presenter.SignInPresenter
import com.gaanap.gaanapehchana.rx.RxDisposable
import com.gaanap.gaanapehchana.services.jobs.SendDeviceTokenJob
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : BaseActivity(),LoginView {

    private val signInPresenter = SignInPresenter(this,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        type.addFont("Futura-Bold.ttf")
        setListener()
    }

    private fun setListener() {
        login          .setOnClickListener { startLoginProcess() }
        forgotPassword .setOnClickListener { ForgotPasswordDialog().show(supportFragmentManager,DIALOG) }
        createAccount  .setOnClickListener {
            startActivity<SignUpActivity>()
            finish()
        }

        rootContainer.setOnTouchListener { touchView, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> hideKeyboard(this)
                MotionEvent.ACTION_UP   -> touchView.performClick()
            }
            false
        }
    }

    private fun startLoginProcess() {
        val email    = edtEmailId.text.toString()
        val password = edtPassword.text.toString()

        if(!ValidationUtils.isValidEmail(email)) {
            edtEmailId.requestFocus()
            edtEmailId.error = getString(R.string.enter_valid_email_address)
            return
        }

        if(password.isBlank()) {
            edtPassword.requestFocus()
            edtPassword.error = getString(R.string.enter_valid_password)
            return
        }

        if(isNetworkAvailable)
            RxDisposable.add(signInPresenter.login(email, password))
        else
            showToast(R.string.connection_error,Toast.LENGTH_LONG)
    }

    override fun showProgress() {
        login.visibility       = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        login.visibility       = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    override fun onResponse(loginResponse: LoginResponse) {
        hideProgress()
        EventUtils.logGroupClickEvent(SIGN_UP,SUCCESS,"Custom Email")
        SessionManager.saveToken(this,loginResponse.data?.token)
        SessionManager.saveUser(this,loginResponse.data?.user ?: return)
        RadioSongIntentService.startRadioSongIntentService(this)
        if(!SessionManager.isDeviceTokenSend(this))
            SendDeviceTokenJob.scheduleTaskForDeviceToken(this)
        startHomeActivity(this)
    }

    override fun onError(error: ErrorResponse?) {
        EventUtils.logGroupClickEvent(SIGN_UP,FAILURE,"Custom Email")
        hideProgress()
        if(error?.message == "invalid_credentials")
            showToast(getString(R.string.invalid_email_or_password),Toast.LENGTH_LONG)
        else
            showToast(getString(R.string.something_wrong_error),Toast.LENGTH_LONG)
    }

    override fun onNetworkError() {
        showToast(getString(R.string.connection_error),Toast.LENGTH_LONG)
    }

    override fun onDestroy() {
        RxDisposable.clear()
        super.onDestroy()
    }
}
