package com.gaanap.gaanapehchana.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.utility.addFont
import com.gaanap.gaanapehchana.utility.inflate
import com.gaanap.gaanapehchana.utility.startActivity
import kotlinx.android.synthetic.main.activity_intro.*
import kotlinx.android.synthetic.main.layout_intro.view.*

class IntroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        setupView()
    }

    private fun setupView() {
        val adapter = LoginIntroAdapter()
        mIntroViewPager.adapter = adapter
        mIntroIndicator.setViewPager(mIntroViewPager)
    }

    private inner class LoginIntroAdapter : PagerAdapter() {

        private val imageArray : IntArray      = intArrayOf(R.drawable.intro_1,R.drawable.intro_2,R.drawable.intro_3)
        private val introList  : Array<String> = resources.getStringArray(R.array.intro_txt)

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view: View = container.inflate(R.layout.layout_intro)
            view.introBgIcon.setImageResource(imageArray[position])
            view.introText.text = introList[position]
            view.introText.addFont("Futura-Medium.ttf")
            view.startPlaying.setOnClickListener{
                startActivity<LoginActivity>()
                finish()
            }
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return imageArray.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }

}
