package com.gaanap.gaanapehchana.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.facebook.CallbackManager
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.fragments.dialogs.EmailDialogFragment
import kotlinx.android.synthetic.main.activity_login.*
import com.gaanap.gaanapehchana.interfaces.LoginView
import com.gaanap.gaanapehchana.models.response.LoginResponse
import com.gaanap.gaanapehchana.models.request.RegisterRequest
import com.gaanap.gaanapehchana.presenter.LoginPresenter
import com.gaanap.gaanapehchana.services.jobs.SendDeviceTokenJob
import com.gaanap.gaanapehchana.utility.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient

class LoginActivity : BaseActivity(), LoginView {

    private var callbackManager : CallbackManager?   = null
    private var authClient      : TwitterAuthClient? = null

    private val loginPresenter:LoginPresenter = LoginPresenter(this,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setListener()
    }

    private fun setListener() {
        facebookLogin .setOnClickListener { loginWithFacebook() }
        twitterLogin  .setOnClickListener { loginWithTwitter()  }
        googleLogin   .setOnClickListener { loginWithGoogle()   }
        emailLogin    .setOnClickListener { startActivity<SignInActivity>() }
        createAccount .setOnClickListener { startActivity<SignUpActivity>() }
    }

    private fun loginWithFacebook() {
        if (isNetworkAvailable) {
            callbackManager = CallbackManager.Factory.create()
            loginPresenter.loginWithFacebook(callbackManager)
        } else
            showToast(R.string.connection_error)
    }

    private fun loginWithTwitter() {
        if (isNetworkAvailable) {
            authClient = TwitterAuthClient()
            loginPresenter.loginWithTwitter(authClient)
        } else
            showToast(R.string.connection_error)
    }

    private fun loginWithGoogle() {
        if(isNetworkAvailable)
            loginPresenter.loginWithGoogle()
        else
            showToast(R.string.connection_error)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        authClient?.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) loginPresenter.handleSignInResult(data)
    }

    override fun onEmailMissing(request: RegisterRequest) {
        try {
            val fragment = EmailDialogFragment.newInstance(request, loginPresenter::register)
            fragmentManager.beginTransaction().add(fragment, DIALOG).commitAllowingStateLoss()
        } catch (e: Exception) {
            Log.e("Login","Error: $e")
        }
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun onResponse(loginResponse: LoginResponse) {
        SessionManager.saveToken(this,loginResponse.data?.token)
        SessionManager.saveUser(this,loginResponse.data?.user)
        RadioSongIntentService.startRadioSongIntentService(this)
        if(!SessionManager.isDeviceTokenSend(this))
            SendDeviceTokenJob.scheduleTaskForDeviceToken(this)
        startHomeActivity(this)
        finish()
    }
}
