package com.gaanap.gaanapehchana.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.os.CountDownTimer
import android.os.Handler
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import com.devbrackets.android.exomedia.AudioPlayer
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.ClipOptionAdapter
import com.gaanap.gaanapehchana.ConstantsFlavor
import com.gaanap.gaanapehchana.enums.*
import com.gaanap.gaanapehchana.firebasehelper.model.GamePlayedInfo
import com.gaanap.gaanapehchana.fragments.dialogs.ClipsLoadingDialog
import com.gaanap.gaanapehchana.fragments.dialogs.PausedGameDialog
import com.gaanap.gaanapehchana.models.RightOptionDetails
import com.gaanap.gaanapehchana.models.common_response.ClipOptions
import com.gaanap.gaanapehchana.models.common_response.Clips
import com.gaanap.gaanapehchana.models.event.ClipsEvent
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.utility.*
import com.gaanap.gaanapehchana.widget.LoadingSpinner
import com.gaanap.gaanapehchana.widget.RingProgressBar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.view_toolbar_for_games.*
import java.io.File

@SuppressLint("Registered")
open class BaseGameActivity : BaseActivity() {

    protected var currentClips  : Clips?       = null
    private   var timeOut       : GameTimeOut? = null

    private   var timerState      = TimerState.INIT
    protected val handler         = Handler()
    protected var playerState     = PlayerState.INIT
    protected val selectedOption  = arrayListOf<RightOptionDetails>()
    protected val runnable        = this::updateProgressBar
    protected var isGameOver      = false
    protected var totalTime       = 0L
    protected var lastPauseTime   = 0L
    protected var clipIndex       = 0
    protected var score           = 0
    protected var totalClips      = 0
    protected val disposable      = CompositeDisposable()
    private   val clipsLoader     = ClipsLoadingDialog()
    private   val clipsStatusList = arrayListOf<ClipsEvent>()
    private   val playedInfo      = GamePlayedInfo()


    protected lateinit var dialog      : PausedGameDialog
    protected lateinit var player      : AudioPlayer
    protected lateinit var adapter     : ClipOptionAdapter
    protected lateinit var pauseIcon   : ImageButton
    protected lateinit var pause       : TextView
    protected lateinit var pauseView   : LinearLayout
    protected lateinit var resetView   : LinearLayout
    protected lateinit var loaderIcon  : LoadingSpinner
    protected lateinit var clipScore   : TextView
    protected lateinit var timer       : TextView
    protected lateinit var progressbar : RingProgressBar
    protected lateinit var gameType    : GameType

    protected open fun updateProgressBar() {
        handler.postDelayed(runnable, 100)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        MusicPlayer.stopMusicService()
        setupView()
        setListener()
        setupPlayer()
        disposable.add(RxBus.listenDownloadingStatus(ClipsEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::clipsDownloadingStatus,Throwable::printStackTrace))
    }

    override fun onResume() {
        super.onResume()
        if(playerState == PlayerState.AUTO_PAUSE)
            dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                backActionCode -> {
                    when(data?.getSerializableExtra(RESULT)) {
                        ActivityResult.NEW_GAME -> onNewGame()
                        ActivityResult.REPLAY   -> onGameReset()
                    }
                }
            }
        } else
            finish()
    }

    protected open fun setupView() {
        pauseIcon   = findViewById(R.id.pauseIcon)
        pause       = findViewById(R.id.pause)
        pauseView   = findViewById(R.id.pauseView)
        resetView   = findViewById(R.id.resetView)
        loaderIcon  = findViewById(R.id.loaderIcon)
        clipScore   = findViewById(R.id.clipScore)
        timer       = findViewById(R.id.timer)
        progressbar = findViewById(R.id.progressbar)
        adapter     = ClipOptionAdapter(arrayListOf(),this::onSelectAnswer)
        dialog      = PausedGameDialog(this)
    }

    protected open fun setListener() {
        pauseView.setOnClickListener { onGamePause()  }
        closeIcon.setOnClickListener { onCancelGame() }

        resetView.setOnClickListener {
            onPause()
            DialogUtils.dialogWithCancelListener(this,getString(R.string.are_you_sure_reset_game)) { dialog, which ->
                when(which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        dialog.dismiss()
                        onGameReset()
                    }

                    DialogInterface.BUTTON_NEGATIVE -> {
                        dialog.dismiss()
                        if(playerState == PlayerState.AUTO_PAUSE)
                            onGamePause()
                    }
                }

            }
        }

        dialog.setCallbacks(object : PausedGameDialog.Callbacks {
            override fun onClose() { finish() }
            override fun onReset() { onGameReset() }
            override fun onPlay()  { onGamePause() }

        })
    }

    private fun setupPlayer() {
        player = AudioPlayer(this)
        player.setAudioStreamType(AudioManager.STREAM_MUSIC)
        player.setOnPreparedListener(this::onPrepared)
        player.setOnErrorListener{onError()}
        player.setOnCompletionListener(this::onCompletion)
    }

    private fun clipsDownloadingStatus(clipsEvent: ClipsEvent) {
        if(clipsStatusList.getOrNull(clipsEvent.index) == null) {
            clipsStatusList.add(clipsEvent)
            if(clipsLoader.dialog?.isShowing == true) {
                clipsLoader.changeProgress((clipsStatusList.size*100)/totalClips)
            }
        }
        else
            clipsStatusList[clipsEvent.index] = clipsEvent

        if(clipsLoader.dialog?.isShowing == true && clipsStatusList.size >= totalClips && clipsEvent.status != DownloadingStatus.STARTING) {
            clipsLoader.hideLoading()
            onPlaySongs()
        }

    }

    protected open fun onPrepared() {
        player.start()
        playerState     = PlayerState.START
        progressbar.max = player.duration.toInt()
        pauseView.visibility = View.VISIBLE
        pause.text      = getString(R.string.pause)
        pauseIcon .setImageResource(if(gameType == GameType.BBG) R.drawable.ic_bbg_pause else R.drawable.ic_asn_pause)
        handler   .postDelayed(runnable,100)
    }

    protected open fun onError(): Boolean {
        return false
    }

    protected open fun onCompletion() {}

    protected open fun onSelectAnswer(isAnswerRight: Boolean,option: ClipOptions) {}

    protected open fun onGamePause() {
        when(playerState) {
            PlayerState.START -> {
                player.pause()
                playerState = PlayerState.PAUSE
                pauseIcon.setImageResource(if(gameType == GameType.BBG) R.drawable.ic_bbg_play else R.drawable.ic_asn_play)
                pause.text = getString(R.string.play)
                handler.removeCallbacks(runnable)
                stopGameOutTimer()
            }

            PlayerState.PAUSE -> {
                player.start()
                playerState = PlayerState.START
                pauseIcon.setImageResource(if(gameType == GameType.BBG) R.drawable.ic_bbg_pause else R.drawable.ic_asn_pause)
                pause.text = getString(R.string.pause)
                handler.post(runnable)
                startGameOutTimer(lastPauseTime)
            }

            PlayerState.AUTO_PAUSE -> {
                player.start()
                playerState = PlayerState.START
                handler.post(runnable)
                pauseIcon.setImageResource(if(gameType == GameType.BBG) R.drawable.ic_bbg_pause else R.drawable.ic_asn_pause)
                pause.text = getString(R.string.pause)
                startGameOutTimer(lastPauseTime)
            }
            else -> {}
        }
    }

    private fun onNewGame() {
        val intent = Intent()
        intent.putExtra(GAME,gameType)
        startIntentActivity<DecadesActivity>(intent)
        finish()
    }

    protected open fun onGameReset() {
        stopGameOutTimer()
        isGameOver    = false
        score         = 0
        lastPauseTime = totalTime
        timer.text    = getDurationString(totalTime)
        selectedOption.clear()
    }

    private fun onCancelGame() {
        onPause()
        DialogUtils.dialogWithCancelListener(this,getString(R.string.are_you_sure_close_game)) { dialog, which ->
            when(which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    val intent = Intent()
                    intent.putExtra(GAME,gameType)
                    startIntentActivity<DecadesActivity>(intent)
                    finish()
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.dismiss()
                    if(playerState == PlayerState.AUTO_PAUSE)
                        onGamePause()
                }
            }
        }
    }

    protected fun resetPlayer() {
        player.reset()
        playerState = PlayerState.INIT
        handler.removeCallbacks(runnable)
    }


    protected fun onPlaySongs() {
//        val downloadingStatus = clipsStatusList.getOrNull(clipIndex)
//        if(downloadingStatus == null || downloadingStatus.status == DownloadingStatus.STARTING) {
////            clipsLoader.showLoading(this)
////            stopGameOutTimer()
//            startSongs(false)
//            return
//        }
//
//        when(downloadingStatus.status) {
//            DownloadingStatus.DONE     -> startSongs(true)
//            DownloadingStatus.ERROR    -> {
//                clipsLoader.hideLoading()
//                onPause()
//                DialogUtils.openDialog(this,getString(R.string.connection_error)) { _, _ -> finish() }
//            }
//            else -> {}
//        }

        startSongs()
    }

    private fun startSongs() {
        val clipUrl = ConstantsFlavor.CLIPS_URL + currentClips?.details?.clipFileName
//        if(isInLocal) {
//            val file = File("$cacheDir/songs/${currentClips?.details?.clipFileName}")
//            if (file.exists() && file.length() > 0)
//                clipUrl = file.absolutePath
//        }

        player.setDataSource(Uri.parse(clipUrl.replace("\\s+".toRegex(), "%20")))
        player.prepareAsync()
        startGameOutTimer(lastPauseTime)
    }


    protected fun startGameOutTimer(remainingTime: Long) {
        if(timerState == TimerState.INIT) {
            timeOut = GameTimeOut(remainingTime,MILLI_SECONDS.toLong())
            timeOut?.start()
            timerState = TimerState.START
        }
    }

    private fun stopGameOutTimer() {
        if (timerState == TimerState.START) {
            timeOut?.cancel()
            timeOut = null
            timerState = TimerState.INIT
        }
    }

    protected fun addSongInfo(songDetails: Clips.SongDetails?,level :String?,currentClipScore :Int,isAnswerRight: Boolean,isBonus: Boolean) {
        val playList       = PlayList()
        playList.id        = songDetails?.id
        playList.song      = songDetails?.song
        playList.mp3File   = songDetails?.mp3_file
        playList.movieName = songDetails?.movieName
        playList.year      = songDetails?.year
        playList.thumbnail = songDetails?.thumbnail
        playList.singer1   = songDetails?.singer1
        playList.singer2   = songDetails?.singer2
        playList.singer3   = songDetails?.singer3
        playList.singer4   = songDetails?.singer4
        playList.singer5   = songDetails?.singer5
        playList.composer1 = songDetails?.composer1
        playList.composer2 = songDetails?.composer2
        playList.lyricist  = songDetails?.lyricist
        playList.lyrics    = songDetails?.lyrics
        playList.ratingAvg = songDetails?.ratingavg
        playList.comments1 = songDetails?.comments1
        selectedOption.add(RightOptionDetails(playList,currentClipScore,level ?:"",isAnswerRight,isBonus))
    }

    protected fun onGameOver() {
        isGameOver = true
        startIntentActivity<HomeActivity>(Intent().apply {
            putExtra(GAME,gameType)
            putExtra(SCORE,score)
            putExtra(GAME_PLAYED_INFO,playedInfo)
            putParcelableArrayListExtra(SELECTED_OPTION_LIST,selectedOption)
        })
    }

    inner class GameTimeOut(millisInFuture: Long, countDownInterval: Long) : CountDownTimer(millisInFuture, countDownInterval) {

        override fun onTick(millisUntilFinished: Long) {
            timer.text    = getDurationString(millisUntilFinished)
            lastPauseTime = millisUntilFinished
        }

        override fun onFinish() {
            if (!isGameOver)
                onGameOver()
        }
    }

    override fun onPause() {
        super.onPause()
        if(player.isPlaying) {
            player.pause()
            playerState = PlayerState.AUTO_PAUSE
            handler.removeCallbacks(runnable)
            pauseIcon.setImageResource(if(gameType == GameType.BBG) R.drawable.ic_bbg_play else R.drawable.ic_asn_play)
            pause.text = getString(R.string.play)
        }

        stopGameOutTimer()
    }

    override fun onStop() {
        super.onStop()
        if(isGameOver) {
            player.reset()
            player.stopPlayback()
            playerState = PlayerState.STOP
            handler.removeCallbacks(runnable)
            disposable.clear()
        }
    }

    override fun onBackPressed() {
        onCancelGame()
    }

    override fun onDestroy() {
        super.onDestroy()
        player.stopPlayback()
        player.release()
        playerState = PlayerState.INIT
        handler.removeCallbacks(runnable)
        disposable.clear()
    }
}