package com.gaanap.gaanapehchana.view

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.util.Log
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.ConstantsFlavor
import com.gaanap.gaanapehchana.GaanaPApp

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.fragments.*
import com.gaanap.gaanapehchana.fragments.mymusic.MyMusicFragment
import com.gaanap.gaanapehchana.helper.BottomNavigationViewHelper
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_web_view.*

class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventUtils.setCurrentUser(this)
        setContentView(R.layout.activity_home)
        BottomNavigationViewHelper.removeShiftMode(bottomNavView)
        setListener()
        loadFragment(HomeFragment(),HOME_FRAGMENT)
        EventUtils.logGroupClickEvent(MOST_SELECTED,TAB,"Game")
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if(intent?.hasExtra(GAME) == true) {
            loadFragment(GameWinFragment.newInstance(intent),GAME_WIN_FRAGMENT)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, null)
    }

    private fun setListener() {
        bottomNavView.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.actionGame -> {
                    EventUtils.logGroupClickEvent(MOST_SELECTED,TAB,"Game")
                    loadFragment(HomeFragment(),HOME_FRAGMENT)
                }

                R.id.actionMyMusic -> {
                    EventUtils.logGroupClickEvent(MOST_SELECTED,TAB,"MyMusic")
                    loadFragment(MyMusicFragment() ,MY_MUSIC_FRAGMENT)
                }

                R.id.actionRadio -> {
                    EventUtils.logGroupClickEvent(MOST_SELECTED,TAB,"Radio")
                    loadFragment(GPRadioFragment.newInstance(),GP_RADIO_FRAGMENT)
                }

                R.id.actionBlog    -> {
                    EventUtils.logGroupClickEvent(MOST_SELECTED,TAB,"Blog")
                    loadFragment(WebViewFragment.newInstance(ConstantsFlavor.BLOG_URL,getString(R.string.blogs),true), BLOG_FRAGMENT)
                }

                R.id.actionProfile -> {
                    EventUtils.logGroupClickEvent(MOST_SELECTED,TAB,"Profile")
                    loadFragment(ProfileFragment(), PROFILE_FRAGMENT)
                }
            }

            return@setOnNavigationItemSelectedListener true
        }
    }

    fun loadFragment(fragment: Fragment, tag: String, addToBackStack: Boolean = true) {
        val oldFragment = supportFragmentManager.findFragmentByTag(tag)
        if(oldFragment == null || !oldFragment.isVisible)
            replaceFragment(R.id.container,fragment,tag,addToBackStack)
    }

    private fun setChecked(position: Int) {
        bottomNavView?.menu?.getItem(position)?.isChecked = true
    }


    override fun onBackPressed() {
        try {
            var count = supportFragmentManager.backStackEntryCount
            if (count <= 1) {
                finish()
                return
            } else {
                val currentFragmentTag = supportFragmentManager.getBackStackEntryAt(count - 1).name
                when (currentFragmentTag) {
                    BLOG_FRAGMENT, ABOUT_US_FRAGMENT -> {
                        val fragment = supportFragmentManager.findFragmentByTag(currentFragmentTag) as? WebViewFragment
                        if (fragment?.isVisible == true && fragment.webView.canGoBack()) {
                            fragment.onBackNavigation()
                            return
                        }
                    }
                }

                supportFragmentManager.popBackStackImmediate()
            }

            count = supportFragmentManager.backStackEntryCount

            val currentFragmentTag = supportFragmentManager.getBackStackEntryAt(count - 1).name

            when (currentFragmentTag) {
                GP_RADIO_FRAGMENT -> setChecked(2)
                BLOG_FRAGMENT -> setChecked(3)
                HOME_FRAGMENT, TMK_LIST_FRAGMENT, GAME_WIN_FRAGMENT -> setChecked(0)
                MY_MUSIC_FRAGMENT, PLAYLIST_FRAGMENT, PLAYER_FRAGMENT, RADIO_SEARCH_FRAGMENT -> setChecked(1)
                PROFILE_FRAGMENT, ABOUT_US_FRAGMENT, SETTINGS_FRAGMENT, CONTACT_US_FRAGMENT -> setChecked(4)
            }
        } catch (e: Exception) {
            Log.e("error",": $e")
        }
    }

    override fun onDestroy() {
        GaanaPApp.sMixPanel.flush()
        super.onDestroy()
    }
}
