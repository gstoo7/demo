package com.gaanap.gaanapehchana.view

import android.content.ComponentName
import android.content.ServiceConnection
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.view.ViewPager
import android.util.TypedValue
import android.view.View
import android.widget.SeekBar
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.PlayerPagerAdapter
import com.gaanap.gaanapehchana.fragments.dialogs.AddToPlayListDialog
import com.gaanap.gaanapehchana.fragments.dialogs.SongInfoDialog
import com.gaanap.gaanapehchana.helper.ZoomOutPageTransformer
import com.gaanap.gaanapehchana.models.event.PlayListEvent
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.services.MusicService
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_player.*

class PlayerActivity : BaseActivity(), MusicService.PlayerServiceListener {

    private var serviceToken : MusicPlayer.ServiceToken? = null
    private var songsList    : ArrayList<PlayList>?      = null

    private lateinit var adapter : PlayerPagerAdapter

    private var isRepeat     : Boolean = false
    private var isShuffling  : Boolean = false
    private var playing      : Boolean = false
    private var paused       : Boolean = false
    private var isMyPlayList : Boolean = false

    private var position   = 0
    private var songsIndex = 0
//    private var mLastClick = 0L
//    private val clickGap   = 100L

    private var playListName = ""
    private var playListId   = ""
    private val myPlayListSongs = hashSetOf<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        getDataFromIntent()
        setupViewPager()
        setListener()
        setData()
    }

    private fun getDataFromIntent() {
        intent?.let {
            playListId   = it.getStringExtra(PLAY_LIST_ID)   ?: ""
            playListName = it.getStringExtra(PLAY_LIST_NAME) ?: getString(R.string.gaanap_radio)
            position     = it.getIntExtra(POSITION,0)
            isMyPlayList = it.getBooleanExtra(IS_MY_PLAY_LIST,false)
            songsList    = it.getParcelableArrayListExtra<PlayList>(SONG_LIST)
        }
    }

    private fun setupViewPager() {
        adapter = PlayerPagerAdapter(supportFragmentManager, arrayListOf())
        val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (40 * 2).toFloat(), resources.displayMetrics).toInt()
        viewPager.pageMargin         = -margin
        viewPager.clipToPadding      = false
        viewPager.offscreenPageLimit = 5
        viewPager.adapter            = adapter
        viewPager.setPageTransformer(true, ZoomOutPageTransformer(true))
    }

    private fun setListener() {
        backIcon      .setOnClickListener { onBackPressed()  }
        playPauseIcon .setOnClickListener { playPauseSongs() }
        repeatIcon    .setOnClickListener { repeatSongs()    }
        shuffleIcon   .setOnClickListener { shuffleSongs()   }

        nextIcon.setOnClickListener {
            ++songsIndex
            playNextSongs()
        }

        previousIcon.setOnClickListener {
            --songsIndex
            playNextSongs()
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                if(position != songsIndex) {
                    songsIndex = position
                    playNextSongs()
                }

                favoriteIcon.setImageResource(if(adapter.getSong(position)?.inMyPlaylist == true) R.drawable.ic_favorite_black_24dp else R.drawable.ic_favorite_border_black_24dp)
            }

        })

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if(fromUser) MusicPlayer.seekTo(progress)
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        favoriteIcon.setOnClickListener {
            val songId   = MusicPlayer.getCurrentAudio() ?.id
            val position = MusicPlayer.getSongPosition() ?: -1
            when {
                songId.isNullOrBlank()    -> showToast(R.string.something_wrong_error)
                position == -1            -> showToast(R.string.something_wrong_error)
                !isNetworkAvailable(this) -> showToast(R.string.connection_error)
                else -> {
                    AddToPlayListDialog.newInstance(songId!!) { _,_ ->
                        myPlayListSongs.add(position)
                        favoriteIcon.setImageResource(R.drawable.ic_favorite_black_24dp)
                        MusicPlayer.getPlayList()?.get(position)?.inMyPlaylist = true
                        RxBus.publish(PlayListEvent(playListId,songId))
                    }.show(supportFragmentManager, DIALOG)
                }
            }
        }

        infoIcon.setOnClickListener {
            val currentSong = MusicPlayer.getCurrentAudio() ?: return@setOnClickListener
            SongInfoDialog.newInstance(currentSong).show(supportFragmentManager, DIALOG)
        }
    }

    private fun setData() {
        radioTitle.text = playListName
        if(songsList != null) {
            adapter.addAll(songsList!!)
            favoriteIcon.setImageResource(if(songsList?.getOrNull(0)?.inMyPlaylist == true) R.drawable.ic_favorite_black_24dp else R.drawable.ic_favorite_border_black_24dp)
        }
        songsIndex            = position
        viewPager.currentItem = position
        setupPlayer()
    }

    private fun playNextSongs() {
        when {
            songsIndex < 0 -> {
                songsIndex = 0
                previousIcon.alpha     = 0.5f
                previousIcon.isEnabled = false
                return
            }
            songsIndex == 0 -> {
                previousIcon.alpha     = 0.5f
                previousIcon.isEnabled = false
            }

            songsIndex == adapter.count-1 -> {
                nextIcon.alpha     = 0.5f
                nextIcon.isEnabled = false
            }

            songsIndex >= adapter.count -> {
                songsIndex = adapter.count -1
                nextIcon.alpha     = 0.5f
                nextIcon.isEnabled = false
                return
            }
            else -> {
                previousIcon.alpha     = 1f
                previousIcon.isEnabled = true
                nextIcon.alpha         = 1f
                nextIcon.isEnabled     = true
            }
        }

        if(viewPager.currentItem != songsIndex)
            viewPager.currentItem = songsIndex

        MusicPlayer.stop()
        MusicPlayer.play(songsIndex)
    }

    private fun playPauseSongs() {
//        if (SystemClock.elapsedRealtime() - mLastClick < clickGap) {
//            return
//        }
//        mLastClick = SystemClock.elapsedRealtime()

        if (playing && !paused)
            MusicPlayer.pause()
        else
            MusicPlayer.play(MusicPlayer.getSongPosition() ?: return)
    }

    private fun repeatSongs() {
        isRepeat = !isRepeat

        if (isRepeat)
            repeatIcon.setColorFilter(Color.RED,PorterDuff.Mode.SRC_IN)
        else
            repeatIcon.clearColorFilter()

        MusicPlayer.repeatSongs(isRepeat)
    }

    private fun shuffleSongs() {
        isShuffling = !isShuffling

        if (isShuffling)
            shuffleIcon.setColorFilter(Color.RED,PorterDuff.Mode.SRC_IN)
        else
            shuffleIcon.clearColorFilter()

        MusicPlayer.shuffleSongs(isShuffling)
    }

    override fun onPreparedAudio(audioName: String?, duration: Long) {
        seekBar.progress = 0
        seekBar.max      = 100
        playPauseIcon.visibility = View.VISIBLE
        progressBar.visibility   = View.GONE
        adapter.currentFragment?.start()
    }

    override fun onCompletedAudio() {}

    override fun onPaused() {
        playing = false
        paused  = true
        playPauseIcon.setImageResource(R.drawable.ic_play)
        adapter.currentFragment?.pauseAnimation()
    }

    override fun onPlaying() {
        playing = true
        paused  = false
        playPauseIcon.setImageResource(R.drawable.ic_pause)
        adapter.currentFragment?.resumeAnimation()
    }

    override fun onTimeChanged(currentTime: Long, duration: Long) {
        runOnUiThread {
            if(duration > 0) {
                seekBar.progress   = ((currentTime * 100)/duration).toInt()
                timer.text         = getDurationString(currentTime)
                remainingTime.text = getDurationString(duration - currentTime)
            }
        }
    }

    override fun onPreparing(position: Int) {
        songsIndex = position
        viewPager.currentItem    = position
        playPauseIcon.visibility = View.GONE
        progressBar.visibility   = View.VISIBLE

        if(songsIndex == adapter.count-1) {
            nextIcon.alpha     = 0.5f
            nextIcon.isEnabled = false
        }
    }

    override fun onContinueAudio() {}

    private fun setupPlayer() {
        if (MusicPlayer.mService == null)
            serviceToken = MusicPlayer.bindToService(this,mConnection)
        else {
            MusicPlayer.registerServicePlayerListener(this)
            MusicPlayer.setPlayList(songsList)
            MusicPlayer.setPlayListName(playListName)
            MusicPlayer.setPlayListId(playListId)
            val currentSongs = MusicPlayer.getCurrentAudio()
            val details      = adapter.getSong(songsIndex)
            if(currentSongs?.id == details?.id)
                setPlayerState()
            else
                playNextSongs()
        }
    }

    private fun setPlayerState() {
        playing     = MusicPlayer.isPlaying()
        isShuffling = MusicPlayer.isShuffling()
        isRepeat    = MusicPlayer.isRepeating()
        paused      = !playing

        onTimeChanged(MusicPlayer.getCurrentPosition(),MusicPlayer.getDuration())

        playPauseIcon.setImageResource(if(playing) R.drawable.ic_pause else R.drawable.ic_play)

        if(isShuffling)
            shuffleIcon.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        else
            shuffleIcon.clearColorFilter()

        if(isRepeat)
            repeatIcon.setColorFilter(Color.RED,PorterDuff.Mode.SRC_IN)
        else
            repeatIcon.clearColorFilter()

        Handler().postDelayed({
            adapter.currentFragment?.start()
        },1500)

    }

    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, service: IBinder) {
            MusicPlayer.registerServicePlayerListener(this@PlayerActivity)
            MusicPlayer.setPlayList(songsList)
            MusicPlayer.setPlayListName(playListName)
            MusicPlayer.setPlayListId(playListId)
            playNextSongs()
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            MusicPlayer.mService = null
            playing = false
            paused  = true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (serviceToken != null) {
            MusicPlayer.unregisterServicePlayerListener(this)
            MusicPlayer.unbindFromService(serviceToken)
            serviceToken = null
        }
    }
}
