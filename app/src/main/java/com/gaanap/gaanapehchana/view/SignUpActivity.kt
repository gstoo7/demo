package com.gaanap.gaanapehchana.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.gaanap.gaanapehchana.BaseActivity

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.interfaces.LoginView
import com.gaanap.gaanapehchana.models.response.LoginResponse
import com.gaanap.gaanapehchana.models.request.RegisterRequest
import com.gaanap.gaanapehchana.presenter.LoginPresenter
import com.gaanap.gaanapehchana.services.jobs.SendDeviceTokenJob
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.json.JSONObject

class SignUpActivity : BaseActivity(),LoginView {

    private val loginPresenter: LoginPresenter = LoginPresenter(this,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        type.addFont("Futura-Bold.ttf")
        setListener()
    }

    private fun setListener() {
        signIn.setOnClickListener {
            val intent   = Intent()
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startIntentActivity<LoginActivity>(intent)
            finish()
        }

        signUp.setOnClickListener {
            val fName = firstName.text.toString()
            val lName = lastName.text.toString()
            val email = emailId.text.toString()
            val pass  = password.text.toString()
            if(checkValidation(fName,lName, email, pass)) {
                showProgress()
                loginPresenter.register(RegisterRequest("$fName $lName", email, pass, "M", null, null))
            }
        }

        rootContainer.setOnTouchListener { touchView, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> hideKeyboard(this)
                MotionEvent.ACTION_UP -> touchView.performClick()
            }
            false
        }
    }

    private fun checkValidation(fName: String,lName: String,email: String,pass: String) : Boolean {

        if(!ValidationUtils.isValidName(fName)) {
            firstName.requestFocus()
            firstName.error = getString(R.string.enter_valid_first_name)
            return false
        }

        if(!ValidationUtils.isValidName(lName)) {
            lastName.requestFocus()
            lastName.error = getString(R.string.enter_valid_last_name)
            return false
        }

        if(!ValidationUtils.isValidEmail(email)) {
            emailId.requestFocus()
            emailId.error = getString(R.string.enter_valid_email_address)
            return false
        }

        if(pass.isBlank()) {
            password.requestFocus()
            password.error = getString(R.string.enter_valid_password)
            return false
        }

        if(pass.length < 6) {
            password.requestFocus()
            password.error = getString(R.string.password_minimum_error)
            return false
        }

        return true
    }

    override fun showProgress() {
        signUp.visibility      = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
        signUp.visibility      = View.VISIBLE
    }

    override fun onResponse(loginResponse: LoginResponse) {
        EventUtils.logGroupClickEvent(SIGN_UP,SUCCESS,"Custom Email")
        SessionManager.saveToken(this,loginResponse.data?.token)
        SessionManager.saveUser(this,loginResponse.data?.user)
        RadioSongIntentService.startRadioSongIntentService(this)
        if(!SessionManager.isDeviceTokenSend(this))
            SendDeviceTokenJob.scheduleTaskForDeviceToken(this)
        startHomeActivity(this)
    }

    override fun onError(throwable: Throwable) {
        EventUtils.logGroupClickEvent(SIGN_UP,FAILURE,"Custom Email")
        val error = ErrorUtils.parseErrorResponse(throwable)
        var msg: String? = null
        if(error?.status_code == 400 && !error.message.isNullOrBlank()) {
            try {
                val json = JSONObject(error.message)
                msg      = json.optString("email",null)
                if(!msg.isNullOrBlank())
                    msg = msg.substring(2,msg.length-2)
            } catch (e: Exception) {
               Log.d("error",":$e")
            }
        }
        showToast(msg ?: getString(R.string.something_wrong_error))
    }
}
