package com.gaanap.gaanapehchana.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.gaanap.gaanapehchana.BaseActivity

import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.enums.LoadingStatus
import com.gaanap.gaanapehchana.fragments.dialogs.ClipsLoadingDialog
import com.gaanap.gaanapehchana.fragments.dialogs.DecadesBottomDialog
import com.gaanap.gaanapehchana.fragments.dialogs.LevelsBottomDialog
import com.gaanap.gaanapehchana.models.event.RxASNEvent
import com.gaanap.gaanapehchana.models.event.RxBBGEvent
import com.gaanap.gaanapehchana.models.response.ASNGameResponse
import com.gaanap.gaanapehchana.models.response.BBGameResponse
import com.gaanap.gaanapehchana.presenter.PreferencePresenter
import com.gaanap.gaanapehchana.rx.RxBus
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_decades.*
import com.gaanap.gaanapehchana.utility.*

class DecadesActivity : BaseActivity() {

    private var gameType          = GameType.BBG
    private var selectedDecadesBg = R.drawable.rect_gradient_radius_20dp_tmk
    private val loadingFragment   = ClipsLoadingDialog()
    private val disposable        = CompositeDisposable()
    private val realm             = Realm.getDefaultInstance()
    private val presenter         = PreferencePresenter()

    private var response    : BBGameResponse?  = null
    private var asnResponse : ASNGameResponse? = null

    private var isCompleted = false
    private var isPaused    = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_decades)
        setupToolbar()
        getInfoFromIntent()
        setData()
        setListener()
        loadingFragment.isCanceledOnTouchOutside = true
    }

    override fun onResume() {
        super.onResume()
        isPaused = false
        if(isCompleted)
            hideProgress()
    }

    private fun getInfoFromIntent() {
        gameType = intent.getSerializableExtra(GAME) as GameType

        when(gameType) {
            GameType.BBG -> {
                setupToolbar(R.string.bbg_title)
                mainView.setBackgroundResource(R.drawable.rect_gradient_bbg_bg)
                selectedDecadesBg = R.drawable.rect_gradient_radius_20dp_bbg
                letsPlay.setOnClickListener { loadData() }
            }

            else -> {
                setupToolbar(R.string.aursunao_title)
                aursunaoInfo.visibility = View.VISIBLE
                mainView.setBackgroundResource(R.drawable.rect_gradient_asn_bg)
                selectedDecadesBg = R.drawable.rect_gradient_radius_20dp_aursunao
                letsPlay.setOnClickListener { loadData() }
            }
        }
    }


    private fun setData() {
        val decadesBuilder = StringBuilder()
        val levelsBuilder  = StringBuilder()
        val selDecades     = presenter.getSelectedDecades()
        val selLevels      = presenter.getSelectedDifficulty()
        selDecades .forEach { decadesBuilder.append(it.labelName).append(", ") }
        selLevels  .forEach { levelsBuilder .append(it.labelName).append(", ") }

        selectedDecades.text = if(decadesBuilder.isNotEmpty()) decadesBuilder.substring(0,decadesBuilder.length -2).toString() else ""
        selectedLabel.text   = if(levelsBuilder.isNotEmpty())  levelsBuilder.substring(0,levelsBuilder.length -2).toString() else ""
    }

    private fun setListener() {
        closeIcon.setOnClickListener{ onBackPressed() }

        decades.setOnClickListener {
            DecadesBottomDialog.newInstance(selectedDecadesBg) {
                setData()
            }.show(supportFragmentManager, "Bottom Sheet")
        }

        levels.setOnClickListener {
            LevelsBottomDialog.newInstance(selectedDecadesBg) {
                setData()
            }.show(supportFragmentManager, "Bottom Sheet")
        }

        loadingFragment.setListener {
            disposable.clear()
            when(gameType) {
                GameType.BBG -> BBGClipsDownloadService.stopService(this)
                else         -> ASNClipsDownloadService.stopService(this)
            }
        }
    }

    private fun showProgress() {
        loadingFragment.showLoading(this)
    }

    private fun hideProgress() {
        isCompleted = true
        loadingFragment.hideLoading()
        if(isPaused)
            return

        if(gameType == GameType.BBG) {
            if(response != null) {
                RxBus.publishClipsResponse(response!!)
                startActivity<BBGameActivity>()
            }
        }

        if(gameType == GameType.ASN) {
            if(asnResponse != null) {
                RxBus.publishClipsResponse(asnResponse!!)
                startActivity<ASNGameActivity>()
            }
        }

        finish()
    }

    private fun bindBBGEvent() {
        disposable.add(RxBus.listen(RxBBGEvent::class.java)
                .subscribe ({
                    when(it.status) {
                        LoadingStatus.SHOW_PROGRESS -> showProgress()
                        LoadingStatus.HIDE_PROGRESS -> hideProgress()
                        LoadingStatus.HALF_PROGRESS -> hideProgress()
                        LoadingStatus.PROGRESS      -> loadingFragment.changeProgress(it.progress)
                        LoadingStatus.DATA          -> response = it.response
                        else -> {
                            runOnUiThread {
                                loadingFragment.hideLoading(0)
                                showToast(R.string.something_wrong_error)
                            }
                        }
                    }
                },Throwable::printStackTrace)
        )

    }

    private fun bindASNEvent() {
        disposable.add(RxBus.listen(RxASNEvent::class.java)
                .subscribe ({
                    when(it.status) {
                        LoadingStatus.SHOW_PROGRESS -> showProgress()
                        LoadingStatus.HIDE_PROGRESS -> hideProgress()
                        LoadingStatus.HALF_PROGRESS -> hideProgress()
                        LoadingStatus.PROGRESS      -> loadingFragment.changeProgress(it.progress)
                        LoadingStatus.DATA          -> asnResponse = it.response
                        else -> {
                            runOnUiThread {
                                loadingFragment.hideLoading(0)
                                showToast(R.string.something_wrong_error)
                            }
                        }
                    }
                },Throwable::printStackTrace)
        )

    }

    private fun checkValidation() : Boolean {
//        if(decades.none { it.isSelected }) {
//            showToast(R.string.pls_select_one_decade,Toast.LENGTH_SHORT)
//            return true
//        }
//
//        if(diffList.none { it.isSelected }) {
//            showToast(R.string.pls_select_one_difficulty,Toast.LENGTH_SHORT)
//            return true
//        }

        return false
    }

    private fun loadData() {
        if(checkValidation())
            return
        if (isNetworkAvailable) {
            disposable.clear()
            when(gameType) {
                GameType.BBG -> {
                    EventUtils.logGroupClickEvent(MOST_SELECTED, GAME_CHOICE,BBG)
                    bindBBGEvent()
                    BBGClipsDownloadService.startService(this)
                }
                else -> {
                    EventUtils.logGroupClickEvent(MOST_SELECTED, GAME_CHOICE, ASN)
                    bindASNEvent()
                    ASNClipsDownloadService.startService(this)
                }
            }
        } else
            showToast(R.string.connection_error, Toast.LENGTH_SHORT)
    }

    override fun onPause() {
        super.onPause()
        isPaused = true
    }

    override fun onDestroy() {
        super.onDestroy()
        if(realm?.isClosed == false)
            realm.close()
        disposable.clear()
    }
}
