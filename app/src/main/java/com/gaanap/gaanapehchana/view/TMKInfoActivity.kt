package com.gaanap.gaanapehchana.view

import android.os.Bundle
import android.widget.Toast
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.rx.RxEvent
import com.gaanap.gaanapehchana.utility.*
import io.reactivex.subjects.ReplaySubject
import kotlinx.android.synthetic.main.activity_tmkinfo.*
import kotlinx.android.synthetic.main.view_toolbar_for_games.*

class TMKInfoActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tmkinfo)
        setupToolbar(getString(R.string.tmk_title))
        setListener()
        setScore()
        RxEvent.source = null
        if(isNetworkAvailable) {
            RxEvent.source = ReplaySubject.create()
            TMKClipsDownloadService.startService(this, intent.getStringExtra(GAME_ID))
        } else
            showToast(R.string.connection_error,Toast.LENGTH_SHORT)
    }

    private fun setListener() {
        closeIcon.setOnClickListener { onBackPressed() }

        letsPlay.setOnClickListener {
            if(isNetworkAvailable) {
                EventUtils.logGroupClickEvent(MOST_SELECTED,GAME_CHOICE,TMK)
                if(RxEvent.source == null) {
                    RxEvent.source = ReplaySubject.create()
                    TMKClipsDownloadService.startService(this, intent.getStringExtra(GAME_ID))
                }
                intent.setClass(this, TMKGameActivity::class.java)
                startActivity(intent)
                finish()
            } else
                showToast(R.string.connection_error,Toast.LENGTH_SHORT)
        }
    }

    private fun setScore() {
        sa1.setScore("1")
        sa2.setScore("0")
        sa3.setScore("0")
        re1.setScore("2")
        re2.setScore("1")
        re3.setScore("0")
        ga1.setScore("4")
        ga2.setScore("2")
        ga3.setScore("1")
        ma1.setScore("8")
        ma2.setScore("4")
        ma3.setScore("2")
        pa1.setScore("10")
        pa2.setScore("8")
        pa3.setScore("4")
    }

    override fun onBackPressed() {
        TMKClipsDownloadService.stopService(this)
        super.onBackPressed()
    }

}
