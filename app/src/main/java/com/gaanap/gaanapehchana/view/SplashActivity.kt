package com.gaanap.gaanapehchana.view

import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.interfaces.ForceUpdateView
import com.gaanap.gaanapehchana.models.response.ErrorResponse
import com.gaanap.gaanapehchana.models.response.ForceUpdateResponse
import com.gaanap.gaanapehchana.presenter.ForceUpdatePresenter
import com.gaanap.gaanapehchana.rx.RxDisposable
import com.gaanap.gaanapehchana.services.jobs.SendDeviceTokenJob
import com.gaanap.gaanapehchana.utility.*
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity(), ForceUpdateView {

    private val mTimeOut  = 3000L
    private var startTime = 0L
    private var endTime   = 0L
    private val presenter = ForceUpdatePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        if (!isTaskRoot && intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intent.action != null && intent.action == Intent.ACTION_MAIN) {
            finish()
            return
        }

        RxDisposable.add(presenter.checkForForceUpdate())

    }

    private fun startSplashTime(timeOut: Long) {
        RxDisposable.add(Observable.timer(timeOut, TimeUnit.MILLISECONDS)
                .subscribe({openActivity()},Throwable::printStackTrace))
    }

    private fun openActivity() {
        if(SessionManager.getUser(this) == null)
            startActivity<IntroActivity>()
        else {
            if(!SessionManager.isDeviceTokenSend(this))
                SendDeviceTokenJob.scheduleTaskForDeviceToken(this)
            startActivity<HomeActivity>()
        }
        finish()
    }

    override fun showProgress() {
        startTime = System.currentTimeMillis()
    }

    override fun hideProgress() {
        endTime = System.currentTimeMillis()
    }

    override fun onResponse(response: ForceUpdateResponse) {
        try {
            val serverVersion = response.data.version
            val appVersion = getAppVersion(this)
            if (appVersion < serverVersion) {
                checkForUpdate(response.data.forceupdate)
            } else {
                val callingTime = endTime - startTime
                if (mTimeOut <= callingTime)
                    openActivity()
                else
                    startSplashTime(mTimeOut - callingTime)
            }
        } catch (e: Exception) {
            DialogUtils.openDialog(this,getString(R.string.something_wrong_error)) { _, _ ->
                finish()
            }
        }
    }

    override fun onError(error: ErrorResponse?) {
        DialogUtils.openDialog(this,getString(R.string.something_wrong_error)) { _, _ -> finish()}
    }

    override fun onNetworkError() {
        connectionError.visibility = View.VISIBLE
    }

    private fun checkForUpdate(isForceUpdate: Int) {
        DialogUtils.openUpgradeDialog(this,isForceUpdate) { _, which ->
            when(which) {
                DialogInterface.BUTTON_POSITIVE -> downloadApp()
                DialogInterface.BUTTON_NEGATIVE -> openActivity()
            }
        }
    }

    private fun downloadApp() {
        try {
            val goToMarket = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName"))
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
            startActivity(goToMarket)
            finish()
        } catch (exception: ActivityNotFoundException) {
            Log.d("Error","App not found")
        }
    }

    override fun onDestroy() {
        RxDisposable.clear()
        super.onDestroy()
    }
}
