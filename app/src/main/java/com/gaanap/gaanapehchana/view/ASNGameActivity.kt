package com.gaanap.gaanapehchana.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.animation.OvershootInterpolator
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.enums.GameType
import com.gaanap.gaanapehchana.enums.PlayerState
import com.gaanap.gaanapehchana.fragments.dialogs.CountdownDialog
import com.gaanap.gaanapehchana.models.common_response.ClipOptions
import com.gaanap.gaanapehchana.models.common_response.Scoring
import com.gaanap.gaanapehchana.models.response.ASNGameResponse
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.utility.*
import kotlinx.android.synthetic.main.content_asngame.*
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.Color
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.TextView

class ASNGameActivity : BaseGameActivity() {

    private var mResponse    : ASNGameResponse? = null
    private var intervalTime : Long = 0
    private var pauseTime    : Long = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventUtils.logTimeEvent(MOST_TIME_SPENT)
        EventUtils.logTimeEvent(TIME_OF_GAME_IN_BETWEEN_LEFT)
        setContentView(R.layout.activity_asngame)
        setupToolbar(getString(R.string.aursunao_title))
        disposable.add(RxBus.listenClipsResponse(ASNGameResponse::class.java).subscribe(this::onResponse,Throwable::printStackTrace))
    }

    override fun onResume() {
        super.onResume()
        if(playerState == PlayerState.PAUSE && aurSunaoIcon.visibility == View.VISIBLE)
            dialog.show()
    }

    override fun setupView() {
        super.setupView()
        gameType = GameType.ASN
        asnOptionList.layoutManager     = LinearLayoutManager(this)
        asnOptionList.adapter           = adapter
        asnOptionList.setHasFixedSize(true)
    }

    override fun setListener() {
        super.setListener()
        aurSunaoIcon.setOnClickListener{ onAurSunaoClip() }
    }

    override fun onGameReset() {
        super.onGameReset()
        clipIndex      = -1
        gameScore.text = score.toString()
        startupNewData()
    }

    private fun onAurSunaoClip() {
        var currentClipScore = clipScore.text.toString().toInt()
        if(currentClipScore != 0)
            clipScore.text = (--currentClipScore).toString()
        pauseTime += intervalTime
        player.start()
        playerState = PlayerState.START
        handler.post(runnable)
        changeViewStates(false)
        pauseView.visibility = View.VISIBLE
        //isBonus = false
        startGameOutTimer(lastPauseTime)
    }

    override fun onGamePause() {
        if(aurSunaoIcon.visibility != View.VISIBLE)
            super.onGamePause()
        else
            startGameOutTimer(lastPauseTime)
    }

    private fun onResponse(response: ASNGameResponse) {
        mResponse      = response
        totalClips     = mResponse?.clips?.size ?: 0
        totalTime      = (mResponse?.length!! * SECONDS * MILLI_SECONDS).toLong()
        lastPauseTime  = totalTime
        intervalTime   = (mResponse?.interval!! * MILLI_SECONDS).toLong()
        pauseTime      = intervalTime
        timer.text     = getDurationString(totalTime)
        gameScore.text = score.toString()
        setNewClip()
        CountdownDialog.show(this,this::onPlaySongs)
    }

    private fun setNewClip() {
        currentClips     = mResponse?.clips?.get(clipIndex)
        val currentLevel = currentClips?.details?.clipLevel ?: ""
        level.setText(currentClips?.details?.clipLevel)
        val currentTV = level.currentView as? TextView
        currentTV?.setTextColor(Color.parseColor(levelColors[currentLevel.toLowerCase()]))
        setClipsScore(mResponse?.scoring,currentLevel)
        adapter.updateGameList(currentClips?.options ?: return)
    }

    private fun setClipsScore(score: Scoring?, clipName: String) {
        when(clipName.toLowerCase()) {
            "sa" -> clipScore.text = score?.sa.toString()
            "re" -> clipScore.text = score?.re.toString()
            "ga" -> clipScore.text = score?.ga.toString()
            "ma" -> clipScore.text = score?.ma.toString()
            "pa" -> clipScore.text = score?.pa.toString()
        }
    }

    override fun onCompletion() {
        startupNewData()
    }

    override fun updateProgressBar() {
        if(playerState != PlayerState.START)
            return

        val currentTime      = player.currentPosition.toInt()
        progressbar.progress = currentTime

        if (currentTime != 0 && currentTime >= pauseTime) {
            player.pause()
            playerState = PlayerState.PAUSE
            handler.removeCallbacks(runnable)
            changeViewStates(true)
            pauseView.visibility = View.INVISIBLE
            return
        }

        super.updateProgressBar()
    }

    override fun onSelectAnswer(isAnswerRight: Boolean,option: ClipOptions) {
        val currentClipScore  = clipScore.text.toString().toInt()
        addSongInfo(currentClips?.songDetails,currentClips?.details?.clipLevel,currentClipScore,isAnswerRight,false)

        if(isAnswerRight) {
            score += currentClipScore
            gameScore.text = score.toString()
            applyRotation(starIcon)
        }
        startupNewData()
    }

    private fun startupNewData() {
        resetPlayer()
        changeViewStates(false)
        clipIndex += 1
        pauseTime = intervalTime
        if (clipIndex >= totalClips) {
            onGameOver()
        } else {
            setNewClip()
            onPlaySongs()
        }
    }

    private fun changeViewStates(isHide: Boolean) {
        if(isHide) {
            aurSunaoIcon .visibility = View.VISIBLE
            playInfo     .visibility = View.VISIBLE
            diamondIcon  .visibility = View.INVISIBLE
            clipScore    .visibility = View.INVISIBLE
            startBounceAnimation()
            startZoomInAnimation()
        } else {
            diamondIcon  .visibility = View.VISIBLE
            clipScore    .visibility = View.VISIBLE
            aurSunaoIcon .visibility = View.GONE
            playInfo     .visibility = View.GONE
            aurSunaoIcon .clearAnimation()
            playInfo     .clearAnimation()
        }
    }

    private fun startBounceAnimation() {
        val animator = ObjectAnimator.ofFloat(aurSunaoIcon, "translationY",dp2px(5).toFloat(),-dp2px(5).toFloat())
        animator.interpolator = OvershootInterpolator()
        animator.startDelay   = 100
        animator.duration     = 500
        animator.repeatMode   = ValueAnimator.REVERSE
        animator.repeatCount  = ValueAnimator.INFINITE
        animator.start()
    }

    private fun startZoomInAnimation() {
        val scaleAnimation = ScaleAnimation(1f, 1.2f,1f, 1.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        scaleAnimation.duration    = 500
        scaleAnimation.startOffset = 100
        scaleAnimation.repeatMode  = ValueAnimator.REVERSE
        scaleAnimation.repeatCount = ValueAnimator.INFINITE
        playInfo.startAnimation(scaleAnimation)
    }

    override fun onDestroy() {
        super.onDestroy()
        ASNClipsDownloadService.stopService(this)
        EventUtils.logGroupClickEvent(MOST_TIME_SPENT,GAME, ASN)
        if(!isGameOver) {
            EventUtils.logGroupClickEvent(GAME_IN_BETWEEN_LEFT, GAME_CHOICE, ASN)
            EventUtils.logGroupClickEvent(TIME_OF_GAME_IN_BETWEEN_LEFT, GAME, ASN)
        }
    }
}
