package com.gaanap.gaanapehchana.view

import android.animation.Animator
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.devbrackets.android.exomedia.AudioPlayer
import com.gaanap.gaanapehchana.BaseActivity
import com.gaanap.gaanapehchana.R
import com.gaanap.gaanapehchana.adapter.ClipOptionAdapter
import com.gaanap.gaanapehchana.ConstantsFlavor
import com.gaanap.gaanapehchana.fragments.dialogs.ClipsLoadingDialog
import com.gaanap.gaanapehchana.fragments.dialogs.CountdownDialog
import com.gaanap.gaanapehchana.interfaces.TMKGameView
import com.gaanap.gaanapehchana.models.response.TMKGameResponse
import com.gaanap.gaanapehchana.rx.RxDisposable
import com.gaanap.gaanapehchana.rx.RxEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.gaanap.gaanapehchana.enums.*
import com.gaanap.gaanapehchana.models.RightOptionDetails
import com.gaanap.gaanapehchana.models.common_response.ClipOptions
import com.gaanap.gaanapehchana.models.event.ClipsEvent
import com.gaanap.gaanapehchana.models.response.PlayList
import com.gaanap.gaanapehchana.rx.RxBus
import com.gaanap.gaanapehchana.utility.*
import com.gaanap.gaanapehchana.widget.TMKGameBoardView
import kotlinx.android.synthetic.main.activity_tmkgame.*
import kotlinx.android.synthetic.main.content_tmkgame.*
import kotlinx.android.synthetic.main.layout_for_tmk_game_board.*
import kotlinx.android.synthetic.main.view_for_game_animation.view.*

class TMKGameActivity : BaseActivity(), TMKGameView {

    private var totalChance   = 3
    private var currentChance = 1
    private var clipPlay      = 0
    private var level         = 1
    private var score         = 0
    private var previousScore = 0
    private var totalClips    = 15
    private var playerStatus  = 1
    private var isFirstTime   = true
    private var isGameOver    = false
    private var isbonus       = false

    private var isPausedFromSheet = false

    private lateinit var player: AudioPlayer

    private var mResponse     : TMKGameResponse?           = null
    private var currentClips  : TMKGameResponse.Clips?     = null

    private val loadingFragment = ClipsLoadingDialog()
    private val handler         = Handler()
    private val adapter         = ClipOptionAdapter(arrayListOf(),this::onSelectAnswer)
    private var playerState     = PlayerState.INIT
    private val selectedOption  = arrayListOf<RightOptionDetails>()
    private val clipsStatusList = arrayListOf<ClipsEvent>()

    private var dialogStatus = DialogStatus.NONE

    private var gameId = ""
    private var theme  = ""

    private enum class DialogStatus {
        NONE,SHOW,HIDE
    }

    private lateinit var sheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventUtils.logTimeEvent(MOST_TIME_SPENT)
        EventUtils.logTimeEvent(TIME_OF_GAME_IN_BETWEEN_LEFT)
        setContentView(R.layout.activity_tmkgame)
        gameId        = intent?.getStringExtra(GAME_ID) ?: ""
        theme         = intent?.getStringExtra(THEME)   ?: ""
        previousScore = intent?.getStringExtra(PREVIOUS_SCORE)?.toIntOrNull() ?: -1
        MusicPlayer.stopMusicService()
        setupToolbar(theme)
        setUpView()
        setListener()
        setupPlayer()
        setInitValue()
        setRxSubject()
    }

    override fun onResume() {
        super.onResume()
        if(playerState == PlayerState.AUTO_PAUSE) {
            player.start()
            playerState = PlayerState.START
            handler.post(runnable)
            pauseIcon.setImageResource(R.drawable.ic_tmk_pause)
            pause.text = getString(R.string.pause)
        }
    }

    private fun setUpView() {
        loadingFragment.isCanceledOnTouchOutside = true
        tmkOptionList.layoutManager = LinearLayoutManager(this)
        tmkOptionList.adapter       = adapter
        tmkOptionList.setHasFixedSize(true)
        sheetBehavior = BottomSheetBehavior.from(gameBoardSheet)
    }

    private fun setListener() {
        loadingFragment.setListener  { onGoBack()     }
        pauseView.setOnClickListener { onGamePause()  }
        closeIcon.setOnClickListener { onCancelGame() }
        resetView.setOnClickListener {
            onPause()
            DialogUtils.dialogWithCancelListener(this,getString(R.string.are_you_sure_reset_game)) { dialog, which ->
                when(which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        dialog.dismiss()
                        onGameReset()
                    }

                    DialogInterface.BUTTON_NEGATIVE -> {
                        dialog.dismiss()
                        onResume()
                    }
                }

            }
        }

        gameBoard.setOnClickListener {
            if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                EventUtils.logClickEvent(TMKGameBoard)
            } else
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
        }

        overlayView.setOnTouchListener { _, motionEvent ->
            when(motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    if(sheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                        sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                        return@setOnTouchListener true
                    }
                }
            }

            return@setOnTouchListener false
        }

        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback(){
            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when(newState) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        if(playerState != PlayerState.PAUSE) {
                            isPausedFromSheet = true
                            pauseView.performClick()
                        }
                    }

                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        if(playerState == PlayerState.PAUSE && isPausedFromSheet) {
                            isPausedFromSheet = false
                            pauseView.performClick()
                        }
                    }
                }

            }

        })
    }

    private fun setInitValue() {
        totalChance   = 3
        currentChance = 1
        level         = 1
        clipPlay      = 0
        score         = 0
        playerStatus  = 1
        isGameOver    = false
        isbonus       = false

        levelValue    .setCurrentText(LAVEL[clipPlay])
        favoriteValue .setCurrentText(totalChance.toString())
        gameScore.text     = score.toString()
        playerState        = PlayerState.INIT
        player.reset()
        selectedOption.clear()
    }

    private fun setRxSubject() {
        if(RxEvent.source == null)
            return
        RxDisposable.add(RxEvent.source
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    when(it.status) {
                        LoadingStatus.SHOW_PROGRESS -> showProgress()
                        LoadingStatus.DATA          -> onResponse(it.response)
                        LoadingStatus.ERROR         -> onError(Throwable("wrong"))
                        LoadingStatus.PROGRESS      -> loadingFragment.changeProgress(it.progress)
                        LoadingStatus.HALF_PROGRESS -> hideProgress()
                        LoadingStatus.HIDE_PROGRESS -> if(dialogStatus == DialogStatus.SHOW || isFirstTime) hideProgress()
                    }
                },this::onError))

        RxDisposable.add(RxBus.listenDownloadingStatus(ClipsEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::clipsDownloadingStatus,Throwable::printStackTrace))
    }

    private fun clipsDownloadingStatus(clipsEvent: ClipsEvent) {
        if(clipsStatusList.getOrNull(clipsEvent.index) == null) {
            clipsStatusList.add(clipsEvent)
            if(loadingFragment.dialog?.isShowing == true) {
                loadingFragment.changeProgress((clipsStatusList.size*100)/totalClips)
            }
        }
        else
            clipsStatusList[clipsEvent.index] = clipsEvent

        if(!isFirstTime && dialogStatus == DialogStatus.SHOW && clipsStatusList.size >= totalClips && clipsEvent.status != DownloadingStatus.STARTING) {
            loadingFragment.hideLoading()
            dialogStatus = DialogStatus.HIDE
            onPlaySongs()
        }

    }

    private fun setupPlayer() {
        player = AudioPlayer(this)
        player.setAudioStreamType(AudioManager.STREAM_MUSIC)
        player.setOnPreparedListener(this::onPrepared)
        player.setOnErrorListener{onError()}
        player.setOnCompletionListener(this::onCompletion)
    }

    private fun onGamePause() {
        when(playerState) {
            PlayerState.START -> {
                playerState = PlayerState.PAUSE
                player.pause()
                pauseIcon.setImageResource(R.drawable.ic_tmk_play)
                pause.text = getString(R.string.play)
            }

            PlayerState.PAUSE -> {
                playerState = PlayerState.START
                player.start()
                pauseIcon.setImageResource(R.drawable.ic_tmk_pause)
                pause.text = getString(R.string.pause)
            }
            else -> {}
        }
    }

    private fun onCancelGame() {
        onPause()
        DialogUtils.dialogWithCancelListener(this,getString(R.string.are_you_sure_close_game)) { dialog, which ->
            when(which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    onGoBack()
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.dismiss()
                    onResume()
                }
            }
        }
    }

    private fun onGameReset() {
        setInitValue()
        setGameBoardScore()
        startupNewData()

    }

    override fun showProgress() {
        dialogStatus = DialogStatus.SHOW
        loadingFragment.showLoading(this)
    }

    override fun hideProgress() {
        loadingFragment.hideLoading()
        loadingFragment.isCancelable = false
        dialogStatus = DialogStatus.HIDE
        if(isFirstTime) {
            isFirstTime = false
            CountdownDialog.show(this) {
                onPlaySongs()
                animateBackground(bgView1)
            }
        }
    }

    override fun onResponse(response: TMKGameResponse) {
        mResponse  = response
        totalClips = mResponse?.lists?.clips?.size ?: 15
        setGameBoardScore()
        setNewClipData()
    }

    override fun onError(throwable: Throwable) {
        Log.d("Error","$throwable")
        loadingFragment.hideLoading()
        dialogStatus = DialogStatus.HIDE
        DialogUtils.openDialog(this,getString(R.string.connection_error)) { _, _ -> finish() }
    }

    private fun findClips(clip: String,level: Int) : TMKGameResponse.Clips? {
        return mResponse?.lists?.clips?.find {it.clipLevel.equals(clip,true) and (it.chance == level)}
    }

    private fun onPlaySongs() {
//        val index = clipPlay*3 + (level-1)
//        val downloadingStatus = clipsStatusList.getOrNull(index)
//        if(downloadingStatus == null || downloadingStatus.status == DownloadingStatus.STARTING) {
////            loadingFragment.showLoading(this)
////            dialogStatus = DialogStatus.SHOW
//            startSongs(false)
//            return
//        }
//
//        when(downloadingStatus.status) {
//            DownloadingStatus.DONE     -> startSongs(true)
//            DownloadingStatus.ERROR    -> {
//                loadingFragment.hideLoading()
//                dialogStatus = DialogStatus.HIDE
//                onPause()
//                DialogUtils.openDialog(this,getString(R.string.connection_error)) { _, _ -> finish() }
//            }
//            else -> {}
//        }

        startSongs()
    }

    private fun startSongs() {
        val clipUrl = ConstantsFlavor.CLIPS_URL + currentClips?.clipFileName
//        if(isInLocal) {
//            val file = File("$cacheDir/songs/${currentClips?.clipFileName}")
//            if (file.exists() && file.length() > 0)
//                clipUrl = file.absolutePath
//        }

        player.setDataSource(Uri.parse(clipUrl.replace("\\s+".toRegex(), "%20")))
        player.prepareAsync()
    }

    private fun onPrepared() {
        player.start()
        playerState = PlayerState.START
        when(playerStatus) {
            1 -> progressbar1.max = player.duration.toInt()
            2 -> progressbar2.max = player.duration.toInt()
            3 -> progressbar3.max = player.duration.toInt()
        }
        handler.postDelayed(runnable,100)
        pauseView.visibility = View.VISIBLE
        pauseIcon.setImageResource(R.drawable.ic_tmk_pause)
        pause.text = getString(R.string.pause)
    }

    private fun onError(): Boolean {
        DialogUtils.openDialog(this,getString(R.string.connection_error)) { _, _ -> finish() }
        return true
    }

    private fun onCompletion() {
        when(playerStatus) {
            1 -> loaderIcon1.startAnimation()
            2 -> loaderIcon2.startAnimation()
            3 -> loaderIcon3.startAnimation()
        }
        playerState = PlayerState.STOP
        pauseView.visibility = View.INVISIBLE
    }

    private fun setGameBoardScore() {
        val score = mResponse?.lists?.score ?: return
        if("1" == mResponse?.lists?.bonus) {
            sa1.setScore(((score.sa1?.toIntOrNull() ?: 0) * 2).toString())
            re1.setScore(((score.re1?.toIntOrNull() ?: 0) * 2).toString())
            ga1.setScore(((score.ga1?.toIntOrNull() ?: 0) * 2).toString())
            ma1.setScore(((score.ma1?.toIntOrNull() ?: 0) * 2).toString())
            pa1.setScore(((score.pa1?.toIntOrNull() ?: 0) * 2).toString())
        } else {
            sa1.setScore(score.sa1)
            re1.setScore(score.re1)
            ga1.setScore(score.ga1)
            ma1.setScore(score.ma1)
            pa1.setScore(score.pa1)
        }

        if("1" == mResponse?.lists?.bonus_tmk2) {
            sa2.setScore(((score.sa2?.toIntOrNull() ?: 0) * 2).toString())
            re2.setScore(((score.re2?.toIntOrNull() ?: 0) * 2).toString())
            ga2.setScore(((score.ga2?.toIntOrNull() ?: 0) * 2).toString())
            ma2.setScore(((score.ma2?.toIntOrNull() ?: 0) * 2).toString())
            pa2.setScore(((score.pa2?.toIntOrNull() ?: 0) * 2).toString())
        } else {
            sa2.setScore(score.sa2)
            re2.setScore(score.re2)
            ga2.setScore(score.ga2)
            ma2.setScore(score.ma2)
            pa2.setScore(score.pa2)
        }

        if("1" == mResponse?.lists?.bonus_tmk3) {
            sa3.setScore(((score.sa3?.toIntOrNull() ?: 0) * 2).toString())
            re3.setScore(((score.re3?.toIntOrNull() ?: 0) * 2).toString())
            ga3.setScore(((score.ga3?.toIntOrNull() ?: 0) * 2).toString())
            ma3.setScore(((score.ma3?.toIntOrNull() ?: 0) * 2).toString())
            pa3.setScore(((score.pa3?.toIntOrNull() ?: 0) * 2).toString())
        } else {
            sa3.setScore(score.sa3)
            re3.setScore(score.re3)
            ga3.setScore(score.ga3)
            ma3.setScore(score.ma3)
            pa3.setScore(score.pa3)
        }

    }

    private fun setNewClipData() {
        isbonus      = false
        currentClips = findClips(LAVEL[clipPlay],level)
        setClipsScore(mResponse?.lists?.score,LAVEL[clipPlay])
        adapter.updateGameList(currentClips?.options?:return)
    }

    private fun setClipsScore(score: TMKGameResponse.Score?,clipName: String) {
        when(clipName) {
            "Sa" -> {
                clipscore1.text = if("1" == mResponse?.lists?.bonus)      ((score?.sa1?.toIntOrNull() ?: 0) * 2).toString() else score?.sa1
                clipscore2.text = if("1" == mResponse?.lists?.bonus_tmk2) ((score?.sa2?.toIntOrNull() ?: 0) * 2).toString() else score?.sa2
                clipscore3.text = if("1" == mResponse?.lists?.bonus_tmk3) ((score?.sa3?.toIntOrNull() ?: 0) * 2).toString() else score?.sa3

                clipscore1.tag = score?.sa1
                clipscore2.tag = score?.sa2
                clipscore3.tag = score?.sa3
            }

            "Re" -> {
                clipscore1.text = if("1" == mResponse?.lists?.bonus)      ((score?.re1?.toIntOrNull() ?: 0) * 2).toString() else score?.re1
                clipscore2.text = if("1" == mResponse?.lists?.bonus_tmk2) ((score?.re2?.toIntOrNull() ?: 0) * 2).toString() else score?.re2
                clipscore3.text = if("1" == mResponse?.lists?.bonus_tmk3) ((score?.re3?.toIntOrNull() ?: 0) * 2).toString() else score?.re3

                clipscore1.tag = score?.re1
                clipscore2.tag = score?.re2
                clipscore3.tag = score?.re3
            }

            "Ga" -> {
                clipscore1.text = if("1" == mResponse?.lists?.bonus)      ((score?.ga1?.toIntOrNull() ?: 0) * 2).toString() else score?.ga1
                clipscore2.text = if("1" == mResponse?.lists?.bonus_tmk2) ((score?.ga2?.toIntOrNull() ?: 0) * 2).toString() else score?.ga2
                clipscore3.text = if("1" == mResponse?.lists?.bonus_tmk3) ((score?.ga3?.toIntOrNull() ?: 0) * 2).toString() else score?.ga3

                clipscore1.tag = score?.ga1
                clipscore2.tag = score?.ga2
                clipscore3.tag = score?.ga3
            }

            "Ma" -> {
                clipscore1.text = if("1" == mResponse?.lists?.bonus)      ((score?.ma1?.toIntOrNull() ?: 0) * 2).toString() else score?.ma1
                clipscore2.text = if("1" == mResponse?.lists?.bonus_tmk2) ((score?.ma2?.toIntOrNull() ?: 0) * 2).toString() else score?.ma2
                clipscore3.text = if("1" == mResponse?.lists?.bonus_tmk3) ((score?.ma3?.toIntOrNull() ?: 0) * 2).toString() else score?.ma3

                clipscore1.tag = score?.ma1
                clipscore2.tag = score?.ma2
                clipscore3.tag = score?.ma3
            }

            "Pa" -> {
                clipscore1.text = if("1" == mResponse?.lists?.bonus)      ((score?.pa1?.toIntOrNull() ?: 0) * 2).toString() else score?.pa1
                clipscore2.text = if("1" == mResponse?.lists?.bonus_tmk2) ((score?.pa2?.toIntOrNull() ?: 0) * 2).toString() else score?.pa2
                clipscore3.text = if("1" == mResponse?.lists?.bonus_tmk3) ((score?.pa3?.toIntOrNull() ?: 0) * 2).toString() else score?.pa3

                clipscore1.tag = score?.pa1
                clipscore2.tag = score?.pa2
                clipscore3.tag = score?.pa3
            }
        }
    }

    private fun onSelectAnswer(isAnswerRight: Boolean,option: ClipOptions) {
        val currentClipScore: Int
        val currentBonusIcon = when(playerStatus) {
            1 -> {
                currentClipScore = clipscore1.text.toString().toInt()
                diamondIcon1
            }

            2 -> {
                currentClipScore = clipscore2.text.toString().toInt()
                diamondIcon2
            }

            3 -> {
                currentClipScore = clipscore3.text.toString().toInt()
                diamondIcon3
            } else -> {
                currentClipScore = 0
                null
            }
        }

        addSongInfo(currentClips,currentClipScore,isAnswerRight,isbonus)
        try {
            val txtId = resources.getIdentifier("${LAVEL[clipPlay].toLowerCase()}$level", "id", packageName)
            findViewById<TMKGameBoardView>(txtId).changeView(isAnswerRight)
            if(sheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        } catch (e: Exception){}

        if(isAnswerRight) {
            if(isbonus && currentBonusIcon != null)
                animateView(currentBonusIcon,currentClipScore)

            score    += currentClipScore
            clipPlay += 1

            if(clipPlay >= LAVEL.size)
                isGameOver = true
            else {
                levelValue.setText(LAVEL[clipPlay])
                val currentTV = levelValue.currentView as? TextView
                currentTV?.setTextColor(Color.parseColor(levelColors[LAVEL[clipPlay].toLowerCase()]))
            }

            gameScore.text = score.toString()
            playerStatus   = 1
            level          = 1

            applyRotation(starIcon)

        } else {
            favoriteValue.setText((totalChance - currentChance).toString())
            addBounceEffect(favoriteIcon)
            currentChance++
            level++
            playerStatus++
        }
        startupNewData()

    }

    private fun startupNewData() {
        if (isGameOver || totalChance < currentChance) {
            onGameOver()
        } else {
            player.reset()
            playerState = PlayerState.INIT
            handler.removeCallbacks(runnable)
            setNewClipData()
            setPlayerState()
            onPlaySongs()
        }
    }

    private fun onGameOver() {
        isGameOver = true
        val intent = Intent().apply {
            putExtra(GAME,GameType.TMK)
            putExtra(GAME_ID,mResponse?.lists?.id)
            putExtra(SCORE,score)
            putExtra(PREVIOUS_SCORE,previousScore)
            putParcelableArrayListExtra(SELECTED_OPTION_LIST,selectedOption)
            putExtra(IS_WIN, totalChance >= currentChance)
        }
        startIntentActivity<HomeActivity>(intent)
        finish()
    }


    private fun setPlayerState() {
        resetAllPlayer()
        when(playerStatus) {
            1 -> {
                setLayoutParams(gameProgress1,180,180)
                setLayoutParams(bgView1,80,80)
                diamondIcon1.visibility    = View.VISIBLE
                diamondIcon1.setImageResource(R.drawable.ic_jewels)
                bgView1.setBackgroundResource(R.drawable.circle_white_with_yellow_boader)
                clipscore1.setTextColor(ContextCompat.getColor(this,R.color.white))
                animateBackground(bgView1)
            }

            2 -> {
                clipscore1.text = "X"
                setLayoutParams(gameProgress2,180,180)
                setLayoutParams(bgView2,80,80)
                diamondIcon2.visibility    = View.VISIBLE
                diamondIcon2.setImageResource(R.drawable.ic_jewels)
                bgView2.setBackgroundResource(R.drawable.circle_white_with_yellow_boader)
                clipscore2.setTextColor(ContextCompat.getColor(this,R.color.white))
                animateBackground(bgView2)
            }

            3 -> {
                clipscore1.text = "X"
                clipscore2.text = "X"
                setLayoutParams(gameProgress3,180,180)
                setLayoutParams(bgView3,80,80)
                diamondIcon3.visibility    = View.VISIBLE
                diamondIcon3.setImageResource(R.drawable.ic_jewels)
                bgView3.setBackgroundResource(R.drawable.circle_white_with_yellow_boader)
                clipscore3.setTextColor(ContextCompat.getColor(this,R.color.white))
                animateBackground(bgView3)
            }
        }
    }

    private fun resetAllPlayer() {
        loaderIcon1.clearAnimation()
        loaderIcon2.clearAnimation()
        loaderIcon3.clearAnimation()
        bgView1.clearAnimation()
        bgView2.clearAnimation()
        bgView3.clearAnimation()
        setLayoutParams(gameProgress1,70,70)
        setLayoutParams(gameProgress2,70,70)
        setLayoutParams(gameProgress3,70,70)
        setLayoutParams(bgView1,30,30)
        setLayoutParams(bgView2,30,30)
        setLayoutParams(bgView3,30,30)
        diamondIcon1.visibility = View.GONE
        diamondIcon2.visibility = View.GONE
        diamondIcon3.visibility = View.GONE
        bgView1.setBackgroundResource(R.drawable.circle_yellow)
        bgView2.setBackgroundResource(R.drawable.circle_yellow)
        bgView3.setBackgroundResource(R.drawable.circle_yellow)
        clipscore1.setTextColor(ContextCompat.getColor(this,R.color.redOrange))
        clipscore2.setTextColor(ContextCompat.getColor(this,R.color.redOrange))
        clipscore3.setTextColor(ContextCompat.getColor(this,R.color.redOrange))
        progressbar1.progress = 0
        progressbar2.progress = 0
        progressbar3.progress = 0
    }

    private fun setLayoutParams(view: View, width: Int, height: Int) {
        val layoutParams    = view.layoutParams
        layoutParams.width  = dp2px(width)
        layoutParams.height = dp2px(height)
        view.requestLayout()
        view.invalidate()
    }

    private fun animateView(view: ImageView,score : Int) {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        val newView = layoutInflater.inflate(R.layout.view_for_game_animation,rootView,false)
        newView.bonusIcon.setImageDrawable(view.drawable)
        newView.bonusScore.text = score.toString()
        newView.x = location[0].toFloat()
        newView.y = (location[1] - view.height).toFloat()
        rootView.addView(newView)
        startAnimation(newView)
    }

    private fun startAnimation(view: View) {
        view.animate()
                .translationX(gameScore.left.toFloat())
                .translationY(gameScore.top.toFloat())
                .setDuration(700)
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}
                    override fun onAnimationCancel(animation: Animator?) {}
                    override fun onAnimationStart(animation: Animator?) {}
                    override fun onAnimationEnd(animation: Animator?) {
                        rootView.removeView(view)
                    }
                })

    }

    private fun animateBackground(view: View) {
        val scale = ScaleAnimation(1f,1.2f,1f,1.2f,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        scale.duration    = 500
        scale.repeatCount = Animation.INFINITE
        scale.repeatMode  = Animation.REVERSE
        view.startAnimation(scale)
    }

    private val runnable = this::updateProgressBar

    private fun updateProgressBar() {
        if(playerState == PlayerState.INIT)
            return

        isbonus = when (playerStatus) {
            1 -> "1" == mResponse?.lists?.bonus      && player.currentPosition < player.duration/2
            2 -> "1" == mResponse?.lists?.bonus_tmk2 && player.currentPosition < player.duration/2
            3 -> "1" == mResponse?.lists?.bonus_tmk3 && player.currentPosition < player.duration/2
            else -> false
        }


        when(playerStatus) {
            1 -> {
                progressbar1.progress = player.currentPosition.toInt()

                if(isbonus) {
                    diamondIcon1.setImageResource(R.drawable.ic_jewels)
                    clipscore1.setTextColor(ContextCompat.getColor(this,R.color.white))
                } else {
                    diamondIcon1.setImageResource(R.drawable.ic_star_24dp)
                    clipscore1.setTextColor(ContextCompat.getColor(this,R.color.blue_start_color))
                    clipscore1.text = clipscore1.tag.toString()
                    bgView1.clearAnimation()
                    changeGameBoardScore(clipscore1.tag.toString())
                }
            }

            2 -> {
                progressbar2.progress = player.currentPosition.toInt()
                if(isbonus) {
                    diamondIcon2.setImageResource(R.drawable.ic_jewels)
                    clipscore2.setTextColor(ContextCompat.getColor(this,R.color.white))
                } else {
                    diamondIcon2.setImageResource(R.drawable.ic_star_24dp)
                    clipscore2.setTextColor(ContextCompat.getColor(this,R.color.blue_start_color))
                    clipscore2.text = clipscore2.tag.toString()
                    bgView2.clearAnimation()
                    changeGameBoardScore(clipscore2.tag.toString())
                }
            }

            3 -> {
                progressbar3.progress = player.currentPosition.toInt()
                if(isbonus) {
                    diamondIcon3.setImageResource(R.drawable.ic_jewels)
                    clipscore3.setTextColor(ContextCompat.getColor(this,R.color.white))
                } else {
                    diamondIcon3.setImageResource(R.drawable.ic_star_24dp)
                    clipscore3.setTextColor(ContextCompat.getColor(this,R.color.blue_start_color))
                    clipscore3.text = clipscore3.tag.toString()
                    bgView3.clearAnimation()
                    changeGameBoardScore(clipscore3.tag.toString())
                }
            }
        }

        handler.postDelayed(runnable, 100)
    }

    private fun changeGameBoardScore(score: String) {
        try {
            val txtId = resources.getIdentifier("${LAVEL[clipPlay].toLowerCase()}$level", "id", packageName)
            findViewById<TMKGameBoardView>(txtId).setScore(score)
        } catch (e: Exception){}
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                backActionCode -> parseIntent(data)
            }
        } else
            finish()
    }

    private fun parseIntent(data: Intent?) {
        when(data?.getSerializableExtra(RESULT)) {
            ActivityResult.REPLAY-> onGameReset()
            ActivityResult.NEW_GAME -> { finish() }
        }
    }

    private fun addSongInfo(songDetails: TMKGameResponse.Clips?,currentClipScore :Int,isAnswerRight: Boolean,isBonus: Boolean) {
        val playList       = PlayList()
        playList.id        = songDetails?.songid
        playList.song      = songDetails?.songname
        playList.mp3File   = songDetails?.songMp3Fle
        playList.movieName = songDetails?.songMovie
        playList.year      = songDetails?.songYear
        playList.thumbnail = songDetails?.thumbnail
        playList.singer1   = songDetails?.songSinger
        playList.composer1 = songDetails?.songComposer
        playList.lyricist  = songDetails?.songLyricist
        playList.ratingAvg = songDetails?.rating
        playList.comments1 = songDetails?.songComment
        selectedOption.add(RightOptionDetails(playList,currentClipScore,currentClips?.clipLevel?:"",isAnswerRight,isBonus))
    }

    private fun onGoBack() {
        TMKClipsDownloadService.stopService(this)
        startIntentActivity<TMKInfoActivity>(Intent().apply {
            putExtra(GAME_ID,gameId)
            putExtra(THEME,theme)
            putExtra(PREVIOUS_SCORE,previousScore)
        })
        finish()
    }

    override fun onPause() {
        super.onPause()
        if(player.isPlaying) {
            player.pause()
            playerState = PlayerState.AUTO_PAUSE
            handler.removeCallbacks(runnable)
        }
    }

    override fun onStop() {
        super.onStop()
        if(isGameOver) {
            player.stopPlayback()
            playerState = PlayerState.STOP
            handler.removeCallbacks(runnable)
        }
    }

    override fun onBackPressed() {
        onCancelGame()
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
        player.stopPlayback()
        player.release()
        RxDisposable.clear()
        TMKClipsDownloadService.stopService(this)
        RxEvent.source = null

        EventUtils.logGroupClickEvent(MOST_TIME_SPENT,GAME,TMK)

        if(!isGameOver) {
            EventUtils.logGroupClickEvent(GAME_IN_BETWEEN_LEFT, GAME_CHOICE, TMK)
            EventUtils.logGroupClickEvent(TIME_OF_GAME_IN_BETWEEN_LEFT, GAME, TMK)
        }
    }
}