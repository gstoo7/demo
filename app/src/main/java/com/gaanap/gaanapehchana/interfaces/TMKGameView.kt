package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.TMKGameResponse
import com.gaanap.gaanapehchana.models.response.TMKListResponse


interface TMKGameView : BaseView {
    override fun showProgress() {}
    override fun hideProgress() {}
    fun onResponse(response: TMKListResponse) {}
    fun onResponse(response: TMKGameResponse) {}
}