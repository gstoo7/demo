package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.BBGameResponse

interface BBGameView : BaseView {
    fun onResponse(response: BBGameResponse)
}