package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.LoginResponse
import com.gaanap.gaanapehchana.models.request.RegisterRequest

interface LoginView : BaseView {
    fun onResponse(loginResponse: LoginResponse)
    fun onEmailMissing(request: RegisterRequest) {}
}