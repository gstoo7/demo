package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.NumberVerifyResponse
import com.gaanap.gaanapehchana.models.response.VerifyOTPResponse

interface NumberVerifyView: BaseView {
    fun onResponse(response: NumberVerifyResponse){}
    fun onResponse(response: VerifyOTPResponse){}
}