package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.*

interface RadioView : BaseView {
    fun onResponse(response: BaseResponse){}
    fun onResponse(response: ChannelsResponse){}
    fun onResponse(response: ArrayList<PublicPlaylist>){}
    fun onResponse(response: List<PlayList>){}
    fun onResponse(response: RadioSearchResponse) {}
}