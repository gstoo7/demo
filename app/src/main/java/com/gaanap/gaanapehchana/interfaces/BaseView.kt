package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.ErrorResponse
import com.gaanap.gaanapehchana.utility.ErrorUtils
import java.io.IOException

interface BaseView {
    fun showProgress()
    fun hideProgress()
    fun onNetworkError() {}
    fun onError(error : ErrorResponse?) {}
    fun onError(throwable: Throwable) {
        if(throwable is IOException)
            onNetworkError()
        else
            onError(ErrorUtils.parseErrorResponse(throwable))
    }
}