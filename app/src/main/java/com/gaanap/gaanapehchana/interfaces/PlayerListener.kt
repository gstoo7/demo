package com.gaanap.gaanapehchana.interfaces


interface PlayerListener {

    fun onSongsPlay(songsFile: String)

    fun onSongsPause()

    fun onSongsStop()
}