package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.BaseResponse

interface ForgotPasswordView : BaseView {
    fun onResponse(response: BaseResponse)
}