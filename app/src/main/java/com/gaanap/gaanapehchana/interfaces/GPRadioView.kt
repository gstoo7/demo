package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.PlayList

interface GPRadioView : BaseView {
    fun onResponse(response: ArrayList<PlayList>)
}