package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.BaseResponse
import com.gaanap.gaanapehchana.models.response.UpdateProfileResponse

interface ProfileView : BaseView {
    fun onResponse(){}
    fun onResponse(response: UpdateProfileResponse)
    fun onResponse(response: BaseResponse)
}