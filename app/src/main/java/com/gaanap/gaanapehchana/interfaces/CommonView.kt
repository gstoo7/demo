package com.gaanap.gaanapehchana.interfaces

import com.gaanap.gaanapehchana.models.response.BaseResponse


interface CommonView : BaseView {
    fun onResponse(response: BaseResponse)
}