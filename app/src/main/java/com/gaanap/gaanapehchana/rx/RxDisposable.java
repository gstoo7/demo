package com.gaanap.gaanapehchana.rx;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class RxDisposable {

    private static CompositeDisposable mDisposable = new CompositeDisposable();

    private RxDisposable() {}


    public static void add(Disposable disposable) {
        mDisposable.add(disposable);
    }

    public static void clear() {
        mDisposable.clear();
    }
}
