package com.gaanap.gaanapehchana.rx

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject

// Use object so we have a singleton instance
object RxBus {

    private val processSubject     = PublishSubject.create<Any>()
    private val responseSubject    = BehaviorSubject.create<Any>()
    private var downloadingSubject = ReplaySubject.create<Any>()

    fun updateDownloadingSubject() {
        downloadingSubject = ReplaySubject.create<Any>()
    }

    fun publish(event: Any) {
        processSubject.onNext(event)
    }

    fun publishClipsResponse(event: Any) {
        responseSubject.onNext(event)
    }

    fun publishDownloadingStatus(event: Any) {
        downloadingSubject.onNext(event)
    }

    // Listen should return an Observable and not the publisher
    // Using ofType we filter only events that match that class type
    fun <T> listen(eventType: Class<T>): Observable<T> = processSubject.ofType(eventType)

    fun <T> listenClipsResponse(eventType: Class<T>)     : Observable<T> = responseSubject.ofType(eventType)
    fun <T> listenDownloadingStatus(eventType: Class<T>) : Observable<T> = downloadingSubject.ofType(eventType)

}