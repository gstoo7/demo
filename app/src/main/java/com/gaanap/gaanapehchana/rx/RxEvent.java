package com.gaanap.gaanapehchana.rx;

import android.util.Log;

import com.gaanap.gaanapehchana.models.RxTMKData;
import com.gaanap.gaanapehchana.models.response.TMKGame;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import io.realm.Realm;

public class RxEvent {

    public static ReplaySubject<RxTMKData> source = null;

    public static Completable updateOnLocalDB(int score,String gameId) {
        return Completable.create(source -> {
            try(Realm realm = Realm.getDefaultInstance()) {
                realm.executeTransaction(r-> {
//                    TMKGame game = realm.where(TMKGame.class)
//                            .equalTo("gameid",gameId).and().equalTo("my_score", "--").findFirst();
//                    if(game != null)
//                        game.setMy_score(String.valueOf(score));
                    source.onComplete();
                });
            } catch (Exception e) {
                Log.d("error","OnRealmError:"+e);
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(Schedulers.newThread());
    }
}
