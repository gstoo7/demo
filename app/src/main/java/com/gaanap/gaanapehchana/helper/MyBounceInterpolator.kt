package com.gaanap.gaanapehchana.helper

import android.view.animation.Interpolator

internal class MyBounceInterpolator(var amplitude: Double = 1.0, var frequency: Double = 10.0) : Interpolator {

    override fun getInterpolation(time: Float): Float {
        return (-1.0 * Math.pow(Math.E, -time / amplitude) * Math.cos(frequency * time) + 1).toFloat()
    }
}