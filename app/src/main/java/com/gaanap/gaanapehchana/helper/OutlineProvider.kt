package com.gaanap.gaanapehchana.helper

import android.graphics.Outline
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.View
import android.view.ViewOutlineProvider

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
class OutlineProvider(val radius: Float) : ViewOutlineProvider() {

    override fun getOutline(view: View, outline: Outline) {
        outline.setRoundRect(0, 0, view.width, view.height, radius)
    }
}